<?php 
    
    // $bbt = array(0,5,3,3,1);
    $bbt = array(0,$nitrogen,$posfor,$kalium,$tekstur);

    // $arr_ccd = array(array(),array(),array(),array(),array(),array(),array(),array(),array(),array(),array(),array(),array(),array(),array(),array(),array());
    // $arr_ccd = array(array(),array(),array(),array(),array(),array(),array(),array(),array(),array(),array(),array(),array(),array(),array(),array(),array());
    $rekomtakaran = $this->Rekomedasi_model->get_rekomtakaran()->result();
    foreach ($rekomtakaran as $row) {
        $arr_N[$row->id] = $row->nitrogen2;
        $arr_P[$row->id] = $row->posfor2;
        $arr_K[$row->id] = $row->kalium2;
        $arr_T[$row->id] = $row->tekstur2;
    }

 ?>
<section id="contact" class="color_section">
  <div class="container">
    
          
    <div class="row">
      <div class="col-md-12">
        <div class="portlet light bordered">
          <div class="portlet-body">
            <ul class="nav nav-tabs">
              <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> ELECTRE </a>
              </li>
              <li>
                <a href="#tab_1_2" data-toggle="tab"> TOPSIS </a>
              </li>
              <li>
                <a href="#tab_1_3" data-toggle="tab"> PERBANDINGAN </a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane fade active in" id="tab_1_1">
                <!-- /////////// -->
                <div class="row">
                  <div class="col-sm-12">
                    <center>
                      <h2 class="block-header">Hasil Rekomendasi Menggunakan Metode ELECTRE</h2> 
                    </center>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6">
                      <br>
                      <!-- <h3 class="block-header">SQRT</h3>  -->
                     <!--  <table class="table table-bordered">
                        <tr> -->
                            <td><?php  $arr_SQRT[1]=number_format(SQRT(($arr_N[1]*$arr_N[1])+ ($arr_N[2]*$arr_N[2]) + ($arr_N[3]*$arr_N[3]) + ($arr_N[4]*$arr_N[4]) + ($arr_N[5]*$arr_N[5]) + ($arr_N[6]*$arr_N[6]) + ($arr_N[7]*$arr_N[7]) + ($arr_N[8]*$arr_N[8]) + ($arr_N[9]*$arr_N[9]) + ($arr_N[10]*$arr_N[10]) + ($arr_N[11]*$arr_N[11]) + ($arr_N[12]*$arr_N[12]) + ($arr_N[13]*$arr_N[13]) + ($arr_N[14]*$arr_N[14]) + ($arr_N[15]*$arr_N[15]) + ($arr_N[16]*$arr_N[16]) + ($arr_N[17]*$arr_N[17])),2); ?></td>

                            <td><?php  $arr_SQRT[2]=number_format(SQRT(($arr_P[1]*$arr_P[1])+ ($arr_P[2]*$arr_P[2]) + ($arr_P[3]*$arr_P[3]) + ($arr_P[4]*$arr_P[4]) + ($arr_P[5]*$arr_P[5]) + ($arr_P[6]*$arr_P[6]) + ($arr_P[7]*$arr_P[7]) + ($arr_P[8]*$arr_P[8]) + ($arr_P[9]*$arr_P[9]) + ($arr_P[10]*$arr_P[10]) + ($arr_P[11]*$arr_P[11]) + ($arr_P[12]*$arr_P[12]) + ($arr_P[13]*$arr_P[13]) + ($arr_P[14]*$arr_P[14]) + ($arr_P[15]*$arr_P[15]) + ($arr_P[16]*$arr_P[16]) + ($arr_P[17]*$arr_P[17])),2); ?></td>

                            <td><?php  $arr_SQRT[3]=number_format(SQRT(($arr_K[1]*$arr_K[1])+ ($arr_K[2]*$arr_K[2]) + ($arr_K[3]*$arr_K[3]) + ($arr_K[4]*$arr_K[4]) + ($arr_K[5]*$arr_K[5]) + ($arr_K[6]*$arr_K[6]) + ($arr_K[7]*$arr_K[7]) + ($arr_K[8]*$arr_K[8]) + ($arr_K[9]*$arr_K[9]) + ($arr_K[10]*$arr_K[10]) + ($arr_K[11]*$arr_K[11]) + ($arr_K[12]*$arr_K[12]) + ($arr_K[13]*$arr_K[13]) + ($arr_K[14]*$arr_K[14]) + ($arr_K[15]*$arr_K[15]) + ($arr_K[16]*$arr_K[16]) + ($arr_K[17]*$arr_K[17])),2); ?></td>

                            <td><?php  $arr_SQRT[4]=number_format(SQRT(($arr_T[1]*$arr_T[1])+ ($arr_T[2]*$arr_T[2]) + ($arr_T[3]*$arr_T[3]) + ($arr_T[4]*$arr_T[4]) + ($arr_T[5]*$arr_T[5]) + ($arr_T[6]*$arr_T[6]) + ($arr_T[7]*$arr_T[7]) + ($arr_T[8]*$arr_T[8]) + ($arr_T[9]*$arr_T[9]) + ($arr_T[10]*$arr_T[10]) + ($arr_T[11]*$arr_T[11]) + ($arr_T[12]*$arr_T[12]) + ($arr_T[13]*$arr_T[13]) + ($arr_T[14]*$arr_T[14]) + ($arr_T[15]*$arr_T[15]) + ($arr_T[16]*$arr_T[16]) + ($arr_T[17]*$arr_T[17])),2); ?></td>
                     <!--    </tr>                      
                    </table> -->
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6">
                      <br>
                      <h3 class="block-header">Normalisasi Matriks (R)</h3> 
                      <table class="table table-bordered">
                        <tr>
                            <th>Rekomendasi</th>
                            <th>Nitrogen</th>
                            <th>Posfor</th>
                            <th>Kalium</th>
                            <th>Tekstur</th>
                        </tr>
                        <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                        <tr>                
                            <td><?php echo $row->kd_formula; ?></td>
                            <td><?php echo $arr_nor[1][$i]=number_format(($row->nitrogen2 / $arr_SQRT[1]),3) ; ?></td>
                            <td><?php echo $arr_nor[2][$i]=number_format(($row->posfor2 / $arr_SQRT[2]),3) ; ?></td>
                            <td><?php echo $arr_nor[3][$i]=number_format(($row->kalium2 / $arr_SQRT[3]),3) ; ?></td>
                            <td><?php echo $arr_nor[4][$i]=number_format(($row->tekstur2 / $arr_SQRT[4]),3) ; ?></td>
                        </tr>                      
                        <?php $i++; } ?>
                    </table>
                  </div>

                  <div class="col-sm-6">
                      <br>
                      <h3 class="block-header">Pembobotan Matriks (V: normalisasi * bobot)/ (R*W)</h3> 
                      <table class="table table-bordered">
                        <tr>
                            <th>Rekomendasi</th>
                            <th>Nitrogen</th>
                            <th>Posfor</th>
                            <th>Kalium</th>
                            <th>Tekstur</th>
                        </tr>
                        <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                        <tr>                
                            <td><?php echo $row->kd_formula; ?></td>
                            <td><?php echo $arr_bbt[1][$i]=number_format(($arr_nor[1][$i] * $bbt[1]),3) ; ?></td>
                            <td><?php echo $arr_bbt[2][$i]=number_format(($arr_nor[2][$i] * $bbt[2]),3) ; ?></td>
                            <td><?php echo $arr_bbt[3][$i]=number_format(($arr_nor[3][$i] * $bbt[3]),3) ; ?></td>
                            <td><?php echo $arr_bbt[4][$i]=number_format(($arr_nor[4][$i] * $bbt[4]),3) ; ?></td>
                        </tr>                      
                        <?php  $i++; } ?>
                    </table>
                  </div>
                </div>

                <!-- Corcordance -->
                <?php
                for ($h=1; $h <= 17; $h++) { 
                 for ($i=1; $i <= 17; $i++) { 
                   for ($j=1; $j <= 4; $j++) { 
                     if ($arr_nor[$j][$h] >= $arr_nor[$j][$i]) {
                       // $arr_ccd[$i][$h]= $arr_ccd[$i][$h].' - '.$j.','.$h.'='.$arr_nor[$j][$h].'>='.$j.','.($i).'='.$arr_nor[$j][($i)] ;//@$arr_ccd[$i][$h] . $j. ",";
                       $arr_ccd[$i][$h]= @$arr_ccd[$i][$h] .'1'. ",";
                       $arr_XMccd[$i][$h]= @$arr_XMccd[$i][$h] . $bbt[$j]. ",";
                       // $arr_ccd[$i][$h]= @$arr_ccd[$i][$h] . $j. ",";
                     } else {
                      $arr_ccd[$i][$h]= @$arr_ccd[$i][$h] .'0'. ",";
                      $arr_XMccd[$i][$h]= @$arr_XMccd[$i][$h] . 0 . ",";
                     }
                   }
                 }
                }
                for ($i=0; $i <=17 ; $i++) { 
                   $arr_ccd[$i][$i]='-';
                } 
                ?>
                <!-- N Corcordance -->

                <div class="row">
                  <div class="col-sm-12">
                      <br>
                      <h3 class="block-header">Corcordance (Ckl : Vkj >= Vij)</h3> 
                      <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                              <th><?php echo $row->kd_formula; ?></th>
                            <?php $i++; } ?>
                        </tr>
                        <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                        <tr>                
                            <td><b><?php echo $row->kd_formula; ?></b></td>
                            <td><?php echo $arr_ccd[1][$i]; ?></td>
                            <td><?php echo $arr_ccd[2][$i]; ?></td>
                            <td><?php echo $arr_ccd[3][$i]; ?></td>
                            <td><?php echo $arr_ccd[4][$i]; ?></td>
                            <td><?php echo $arr_ccd[5][$i]; ?></td>
                            <td><?php echo $arr_ccd[6][$i]; ?></td>
                            <td><?php echo $arr_ccd[7][$i]; ?></td>
                            <td><?php echo $arr_ccd[8][$i]; ?></td>
                            <td><?php echo $arr_ccd[9][$i]; ?></td>
                            <td><?php echo $arr_ccd[10][$i]; ?></td>
                            <td><?php echo $arr_ccd[11][$i]; ?></td>
                            <td><?php echo $arr_ccd[12][$i]; ?></td>
                            <td><?php echo $arr_ccd[13][$i]; ?></td>
                            <td><?php echo $arr_ccd[14][$i]; ?></td>
                            <td><?php echo $arr_ccd[15][$i]; ?></td>
                            <td><?php echo $arr_ccd[16][$i]; ?></td>
                            <td><?php echo $arr_ccd[17][$i]; ?></td>
                            
                        </tr>                      
                        <?php $i++; } ?>
                    </table>
                  </div>
                </div>

                <!-- Discordance -->
                <?php 
                for ($h=1; $h <= 17; $h++) { 
                 for ($i=1; $i <= 17; $i++) { 
                   for ($j=1; $j <= 4; $j++) { 
                     if ($arr_nor[$j][$h] < $arr_nor[$j][$i]) {
                       // $arr_ccd[$i][$h]= $arr_ccd[$i][$h].' - '.$j.','.$h.'='.$arr_nor[$j][$h].'>='.$j.','.($i).'='.$arr_nor[$j][($i)] ;//@$arr_ccd[$i][$h] . $j. ",";
                       $arr_dcd[$i][$h]= @$arr_dcd[$i][$h] .'1'. ",";
                       $arr_XMdcd[$i][$h]= @$arr_XMdcd[$i][$h] . $bbt[$j]. ",";
                     } else {
                      $arr_dcd[$i][$h]= @$arr_dcd[$i][$h] .'0'. ",";
                      $arr_XMdcd[$i][$h]= @$arr_XMdcd[$i][$h] . 0 . ",";          
                    }
                   }
                 }
                }
                for ($i=0; $i <=17 ; $i++) { 
                   $arr_dcd[$i][$i]='-';
                } 
                ?>
                <!-- N Discordance -->

                <div class="row">
                  <div class="col-sm-12">
                      <br>
                      <h3 class="block-header">Discordance (Dkl : Vkj < Vij)</h3> 
                      <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                              <th><?php echo $row->kd_formula; ?></th>
                            <?php $i++; } ?>
                        </tr>
                        <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                        <tr>                
                            <td><b><?php echo $row->kd_formula; ?></b></td>
                            <td><?php echo $arr_dcd[1][$i]; ?></td>
                            <td><?php echo $arr_dcd[2][$i]; ?></td>
                            <td><?php echo $arr_dcd[3][$i]; ?></td>
                            <td><?php echo $arr_dcd[4][$i]; ?></td>
                            <td><?php echo $arr_dcd[5][$i]; ?></td>
                            <td><?php echo $arr_dcd[6][$i]; ?></td>
                            <td><?php echo $arr_dcd[7][$i]; ?></td>
                            <td><?php echo $arr_dcd[8][$i]; ?></td>
                            <td><?php echo $arr_dcd[9][$i]; ?></td>
                            <td><?php echo $arr_dcd[10][$i]; ?></td>
                            <td><?php echo $arr_dcd[11][$i]; ?></td>
                            <td><?php echo $arr_dcd[12][$i]; ?></td>
                            <td><?php echo $arr_dcd[13][$i]; ?></td>
                            <td><?php echo $arr_dcd[14][$i]; ?></td>
                            <td><?php echo $arr_dcd[15][$i]; ?></td>
                            <td><?php echo $arr_dcd[16][$i]; ?></td>
                            <td><?php echo $arr_dcd[17][$i]; ?></td>
                            
                        </tr>                      
                        <?php $i++; } ?>
                    </table>
                  </div>
                </div>

                <!-- Matrix Corcordance -->
                <div class="row">
                  <div class="col-sm-12">
                      <br>
                      <h3 class="block-header">Matrix Corcordance (ckl)</h3> 
                      <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                              <th><?php echo $row->kd_formula; ?></th>
                            <?php $i++; } ?>
                        </tr>
                        <?php 
                            $thresholdCon=0; 
                            for ($h=1; $h <= 17; $h++) { 
                               for ($i=1; $i <= 17; $i++) { 
                                   $arr_Mccd[$i][$h] = array_sum($arr_int = json_decode('[' . $Mccd1=substr($arr_XMccd[$i][$h], 0, -1) . ']', true));                       
                               }
                            }

                            for ($h=1; $h <=17 ; $h++) { 
                              $arr_Mccd[$h][$h]='-';
                              for ($i=1; $i <= 17; $i++) { 
                                $thresholdCon= $arr_Mccd[$i][$h] + @$thresholdCon;                              
                              }
                            }
                         ?>
                        <?php $i=1; foreach ($rekomtakaran as $row) { 

                          // $Mccd1=substr($arr_Mccd[1][$i], 0, -1);
                          // $arr_int = json_decode('[' . $Mccd1=substr($arr_Mccd[1][$i], 0, -1) . ']', true);
                          // $x= array_sum($arr_int = json_decode('[' . $Mccd1=substr($arr_Mccd[1][$i], 0, -1) . ']', true));
                          // var_dump($arx);
                          ?>
                        <tr>                
                            <td><b><?php echo $row->kd_formula; ?></b></td>
                            <td><?php echo $arr_Mccd[1][$i]; ?></td> 
                            <td><?php echo $arr_Mccd[2][$i]; ?></td> 
                            <td><?php echo $arr_Mccd[3][$i]; ?></td> 
                            <td><?php echo $arr_Mccd[4][$i]; ?></td> 
                            <td><?php echo $arr_Mccd[5][$i]; ?></td> 
                            <td><?php echo $arr_Mccd[6][$i]; ?></td> 
                            <td><?php echo $arr_Mccd[7][$i]; ?></td> 
                            <td><?php echo $arr_Mccd[8][$i]; ?></td> 
                            <td><?php echo $arr_Mccd[9][$i]; ?></td> 
                            <td><?php echo $arr_Mccd[10][$i]; ?></td> 
                            <td><?php echo $arr_Mccd[11][$i]; ?></td> 
                            <td><?php echo $arr_Mccd[12][$i]; ?></td> 
                            <td><?php echo $arr_Mccd[13][$i]; ?></td> 
                            <td><?php echo $arr_Mccd[14][$i]; ?></td> 
                            <td><?php echo $arr_Mccd[15][$i]; ?></td> 
                            <td><?php echo $arr_Mccd[16][$i]; ?></td> 
                            <td><?php echo $arr_Mccd[17][$i]; ?></td> 
                        </tr>                      
                        <?php $i++; } 
                        ?>
                    </table>
                  </div>
                </div>
                <!-- N Matrix Corcordance -->

                <!-- Matrix Discordance -->
                <div class="row">
                  <div class="col-sm-12">
                      <br>
                      <h3 class="block-header">Matrix Discordance (dkl)</h3> 
                      <table class="table table-bordered">
                      <?php 
                            $thresholdDis=0;
                            $x[1]=0;
                            $y[1]=0;
                            for ($h=1; $h <= 17; $h++) { 
                               for ($i=1; $i <= 17; $i++) { 
                                  for ($j=1; $j <= 4; $j++) { 
                                    if ($h !== $i) $x[$j] = @$$x[$j] . ABS($arr_bbt[$j][$h] - $arr_bbt[$j][$i]);     //MAX(ABS($arr_bbt[$h][$i]))
                                     //array_sum($arr_int = json_decode('[' . $Mccd1=substr($arr_XMdcd[$i][$h], 0, -1) . ']', true));  
                                  } 

                                  for ($k=1; $k <= 1; $k++) { 
                                    if ($h !== $i) $y[$k] = @$$y[$k] . ABS($arr_bbt[$k][$h] - $arr_bbt[$k][$i]);     //MAX(ABS($arr_bbt[$h][$i]))
                                     //array_sum($arr_int = json_decode('[' . $Mccd1=substr($arr_XMdcd[$i][$h], 0, -1) . ']', true));  
                                  } 

                                  if (MAX($y)==0 or MAX($x)==0)
                                    $arr_Mdcd[$i][$h] = 0; else $arr_Mdcd[$i][$h] =number_format((MAX($y)/MAX($x)),3);
                                  
                                  

                               }
                            }

                            for ($h=1; $h <=17 ; $h++) { 
                              $arr_Mdcd[$h][$h]='-';
                                for ($i=1; $i <= 17; $i++) { 
                                  $thresholdDis=$arr_Mdcd[$i][$h] + $thresholdDis;
                                }
                            }
                         ?>

                        <tr>
                            <th></th>
                            <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                              <th><?php echo $row->kd_formula; ?></th>
                            <?php $i++; } ?>
                        </tr>
                        <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                        <tr>                
                            <td><b><?php echo $row->kd_formula; ?></b></td>
                            <td><?php echo $arr_Mdcd[1][$i]; ?></td>
                            <td><?php echo $arr_Mdcd[2][$i]; ?></td>
                            <td><?php echo $arr_Mdcd[3][$i]; ?></td>
                            <td><?php echo $arr_Mdcd[4][$i]; ?></td>
                            <td><?php echo $arr_Mdcd[5][$i]; ?></td>
                            <td><?php echo $arr_Mdcd[6][$i]; ?></td>
                            <td><?php echo $arr_Mdcd[7][$i]; ?></td>
                            <td><?php echo $arr_Mdcd[8][$i]; ?></td>
                            <td><?php echo $arr_Mdcd[9][$i]; ?></td>
                            <td><?php echo $arr_Mdcd[10][$i]; ?></td>
                            <td><?php echo $arr_Mdcd[11][$i]; ?></td>
                            <td><?php echo $arr_Mdcd[12][$i]; ?></td>
                            <td><?php echo $arr_Mdcd[13][$i]; ?></td>
                            <td><?php echo $arr_Mdcd[14][$i]; ?></td>
                            <td><?php echo $arr_Mdcd[15][$i]; ?></td>
                            <td><?php echo $arr_Mdcd[16][$i]; ?></td>
                            <td><?php echo $arr_Mdcd[17][$i]; ?></td>
                            
                        </tr>                      
                        <?php $i++; } ?>
                    </table>
                  </div>
                </div>
                <!-- N Matrix Discordance -->

                <div class="row">
                  <div class="col-sm-6">
                      <h3 class="block-header">Nilai Threshold Dominan Matriks Corcordance (c): <?php echo $thresholdConFix=number_format(($thresholdCon/(17*17)-1),3); ?></h3> 
                  </div>

                 <div class="col-sm-6">
                      <h3 class="block-header">Nilai Threshold Dominan Matriks Discordance (d) : <?php echo $thresholdDisFix=number_format(($thresholdDis/(17*(17-1))),3); ?></h3> 
                  </div>
                </div>

                <!-- Matrix Dominan Corcordance -->
                <div class="row">
                  <div class="col-sm-12">
                      <br>
                      <h3 class="block-header">Matrix Dominan Corcordance (F)</h3> 
                      <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                              <th><?php echo $row->kd_formula; ?></th>
                            <?php $i++; } ?>
                        </tr>
                        <?php 
                            for ($h=1; $h <= 17; $h++) { 
                               for ($i=1; $i <= 17; $i++) { 
                                    if ($arr_Mccd[$i][$h] >= $thresholdConFix) {
                                      $arr_MDccd[$i][$h] = 1;                       
                                    } else {
                                      $arr_MDccd[$i][$h] = 0;     
                                    }                      
                               }
                            }

                            for ($h=1; $h <=17 ; $h++) { 
                              $arr_MDccd[$h][$h]='-';
                            }
                         ?>
                        <?php $i=1; foreach ($rekomtakaran as $row) { 
                          ?>
                        <tr>                
                            <td><b><?php echo $row->kd_formula; ?></b></td>
                            <td><?php echo $arr_MDccd[1][$i]; ?></td> 
                            <td><?php echo $arr_MDccd[2][$i]; ?></td> 
                            <td><?php echo $arr_MDccd[3][$i]; ?></td> 
                            <td><?php echo $arr_MDccd[4][$i]; ?></td> 
                            <td><?php echo $arr_MDccd[5][$i]; ?></td> 
                            <td><?php echo $arr_MDccd[6][$i]; ?></td> 
                            <td><?php echo $arr_MDccd[7][$i]; ?></td> 
                            <td><?php echo $arr_MDccd[8][$i]; ?></td> 
                            <td><?php echo $arr_MDccd[9][$i]; ?></td> 
                            <td><?php echo $arr_MDccd[10][$i]; ?></td> 
                            <td><?php echo $arr_MDccd[11][$i]; ?></td> 
                            <td><?php echo $arr_MDccd[12][$i]; ?></td> 
                            <td><?php echo $arr_MDccd[13][$i]; ?></td> 
                            <td><?php echo $arr_MDccd[14][$i]; ?></td> 
                            <td><?php echo $arr_MDccd[15][$i]; ?></td> 
                            <td><?php echo $arr_MDccd[16][$i]; ?></td> 
                            <td><?php echo $arr_MDccd[17][$i]; ?></td> 
                        </tr>                      
                        <?php $i++; } 
                        ?>
                    </table>
                  </div>
                </div>
                <!-- N Matrix Dominan Corcordance -->

                <!-- Matrix Dominan Discordance -->
                <div class="row">
                  <div class="col-sm-12">
                      <br>
                      <h3 class="block-header">Matrix Dominan Discordance (G)</h3> 
                      <table class="table table-bordered">
                      <?php 
                            $thresholdDis=0;
                            for ($h=1; $h <= 17; $h++) { 
                               for ($i=1; $i <= 17; $i++) { 
                                    if ($arr_Mdcd[$i][$h] >= $thresholdDisFix) {
                                      $arr_MDdcd[$i][$h] = 1;                       
                                    } else {
                                      $arr_MDdcd[$i][$h] = 0;     
                                    }                      
                               }
                            }

                            for ($h=1; $h <=17 ; $h++) { 
                              $arr_MDdcd[$h][$h]='-';
                            }
                         ?>

                        <tr>
                            <th></th>
                            <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                              <th><?php echo $row->kd_formula; ?></th>
                            <?php $i++; } ?>
                        </tr>
                        <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                        <tr>                
                            <td><b><?php echo $row->kd_formula; ?></b></td>
                            <td><?php echo $arr_MDdcd[1][$i]; ?></td>
                            <td><?php echo $arr_MDdcd[2][$i]; ?></td>
                            <td><?php echo $arr_MDdcd[3][$i]; ?></td>
                            <td><?php echo $arr_MDdcd[4][$i]; ?></td>
                            <td><?php echo $arr_MDdcd[5][$i]; ?></td>
                            <td><?php echo $arr_MDdcd[6][$i]; ?></td>
                            <td><?php echo $arr_MDdcd[7][$i]; ?></td>
                            <td><?php echo $arr_MDdcd[8][$i]; ?></td>
                            <td><?php echo $arr_MDdcd[9][$i]; ?></td>
                            <td><?php echo $arr_MDdcd[10][$i]; ?></td>
                            <td><?php echo $arr_MDdcd[11][$i]; ?></td>
                            <td><?php echo $arr_MDdcd[12][$i]; ?></td>
                            <td><?php echo $arr_MDdcd[13][$i]; ?></td>
                            <td><?php echo $arr_MDdcd[14][$i]; ?></td>
                            <td><?php echo $arr_MDdcd[15][$i]; ?></td>
                            <td><?php echo $arr_MDdcd[16][$i]; ?></td>
                            <td><?php echo $arr_MDdcd[17][$i]; ?></td>
                            
                        </tr>                      
                        <?php $i++; } ?>
                    </table>
                  </div>
                </div>
                <!-- N Matrix Dominan Discordance -->

                <!-- Agregate Dominan Matriks E -->
                <div class="row">
                  <div class="col-sm-12">
                      <br>
                      <h3 class="block-header">Agregate Dominan Matriks (E)</h3> 
                      <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                              <th><?php echo $row->kd_formula; ?></th>
                            <?php $i++; } ?>
                            <th>SUM</th>
                        </tr>

                        <?php 
                            for ($h=1; $h <= 17; $h++) { 
                               for ($i=1; $i <= 17; $i++) { 
                                    $arr_DME[$i][$h] = $arr_MDccd[$i][$h] * $arr_MDdcd[$i][$h];                                       
                                    $arr_DME[18][$h]= @$arr_DME[18][$h] + $arr_DME[$i][$h];
                               }
                            }

                            for ($h=1; $h <=17 ; $h++) { 
                              $arr_DME[$h][$h]='-';
                            }
                         ?>

                        <?php $i=1; foreach ($rekomtakaran as $row) { 
                          ?>
                        <tr>                
                            <td><b><?php echo $row->kd_formula; ?></b></td>
                            <td><?php echo $arr_DME[1][$i]; ?></td> 
                            <td><?php echo $arr_DME[2][$i]; ?></td> 
                            <td><?php echo $arr_DME[3][$i]; ?></td> 
                            <td><?php echo $arr_DME[4][$i]; ?></td> 
                            <td><?php echo $arr_DME[5][$i]; ?></td> 
                            <td><?php echo $arr_DME[6][$i]; ?></td> 
                            <td><?php echo $arr_DME[7][$i]; ?></td> 
                            <td><?php echo $arr_DME[8][$i]; ?></td> 
                            <td><?php echo $arr_DME[9][$i]; ?></td> 
                            <td><?php echo $arr_DME[10][$i]; ?></td> 
                            <td><?php echo $arr_DME[11][$i]; ?></td> 
                            <td><?php echo $arr_DME[12][$i]; ?></td> 
                            <td><?php echo $arr_DME[13][$i]; ?></td> 
                            <td><?php echo $arr_DME[14][$i]; ?></td> 
                            <td><?php echo $arr_DME[15][$i]; ?></td> 
                            <td><?php echo $arr_DME[16][$i]; ?></td> 
                            <td><?php echo $arr_DME[17][$i]; ?></td> 
                            <td><?php echo $arr_DME[18][$i]; ?></td> 
                            <!-- <td><b><?php echo $arr_SumDME[$i]; ?></b></td>  -->
                        </tr>                      
                        <?php $i++; } 
                        ?>
                    </table>
                  </div>
                </div>
                <!-- N Agregate Dominan Matriks E -->

               <!-- // -->
             <!--    <div class="row">
                  <div class="col-sm-12">
                     <?php var_dump($arr_DME) ?> 
                  </div>
                </div>
              </div> -->
              <?php          
              $i=1; foreach ($rekomtakaran as $row) { 
                $data_poin_electre = array(                          
                        'id_analisa' => $ujitanah_DSC->id,
                        'kd_formula' => $row->kd_formula,
                        'poin' => $arr_DME[18][$i],                            
                        );
                $insert = $this->Rekomedasi_model->add_poin_electre($data_poin_electre);
              $i++;
              }

              ?>

              <div class="row">
                  <div class="col-sm-12">
                      <br>
                      <h3 class="block-header">Hasil Rekomendasi Menggunakan Metode ELECTRE</h3> 
                      <table class="table table-bordered">
                        <tr>
                            <th>No</th>               
                            <th>Formula</th>               
                            <th>Poin</th>               
                            <th>Ranking</th>               
                        </tr>

                        <?php 
                        $poin=$this->Rekomedasi_model->get_poin($ujitanah_DSC->id);
                        $i=1; foreach ($poin as $row) { 
                        ?>
                        <tr>                
                            <td><?php echo $i; ?></td> 
                            <td><?php echo $row->formula; ?></td> 
                            <td><?php echo $row->poin; ?></td> 
                            <td><?php echo $i; ?></td>                 
                        </tr>                      
                        <?php $i++; } 
                        ?>
                    </table>
                  </div>
              </div>
                <!-- //////// -->
              </div>
              <div class="tab-pane fade" id="tab_1_2">
                <!-- /////////// -->
                <div class="row">
                  <div class="col-sm-12">
                    <center>
                      <h2 class="block-header">Hasil Rekomendasi Menggunakan Metode TOPSIS</h2> 
                    </center>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6">
                      <br>
                      <!-- <h3 class="block-header">SQRT</h3> 
                      <table class="table table-bordered">
                        <tr> -->
                            <td><?php  $arr_SQRT[1]=number_format(SQRT(($arr_N[1]*$arr_N[1])+ ($arr_N[2]*$arr_N[2]) + ($arr_N[3]*$arr_N[3]) + ($arr_N[4]*$arr_N[4]) + ($arr_N[5]*$arr_N[5]) + ($arr_N[6]*$arr_N[6]) + ($arr_N[7]*$arr_N[7]) + ($arr_N[8]*$arr_N[8]) + ($arr_N[9]*$arr_N[9]) + ($arr_N[10]*$arr_N[10]) + ($arr_N[11]*$arr_N[11]) + ($arr_N[12]*$arr_N[12]) + ($arr_N[13]*$arr_N[13]) + ($arr_N[14]*$arr_N[14]) + ($arr_N[15]*$arr_N[15]) + ($arr_N[16]*$arr_N[16]) + ($arr_N[17]*$arr_N[17])),2); ?></td>

                            <td><?php  $arr_SQRT[2]=number_format(SQRT(($arr_P[1]*$arr_P[1])+ ($arr_P[2]*$arr_P[2]) + ($arr_P[3]*$arr_P[3]) + ($arr_P[4]*$arr_P[4]) + ($arr_P[5]*$arr_P[5]) + ($arr_P[6]*$arr_P[6]) + ($arr_P[7]*$arr_P[7]) + ($arr_P[8]*$arr_P[8]) + ($arr_P[9]*$arr_P[9]) + ($arr_P[10]*$arr_P[10]) + ($arr_P[11]*$arr_P[11]) + ($arr_P[12]*$arr_P[12]) + ($arr_P[13]*$arr_P[13]) + ($arr_P[14]*$arr_P[14]) + ($arr_P[15]*$arr_P[15]) + ($arr_P[16]*$arr_P[16]) + ($arr_P[17]*$arr_P[17])),2); ?></td>

                            <td><?php  $arr_SQRT[3]=number_format(SQRT(($arr_K[1]*$arr_K[1])+ ($arr_K[2]*$arr_K[2]) + ($arr_K[3]*$arr_K[3]) + ($arr_K[4]*$arr_K[4]) + ($arr_K[5]*$arr_K[5]) + ($arr_K[6]*$arr_K[6]) + ($arr_K[7]*$arr_K[7]) + ($arr_K[8]*$arr_K[8]) + ($arr_K[9]*$arr_K[9]) + ($arr_K[10]*$arr_K[10]) + ($arr_K[11]*$arr_K[11]) + ($arr_K[12]*$arr_K[12]) + ($arr_K[13]*$arr_K[13]) + ($arr_K[14]*$arr_K[14]) + ($arr_K[15]*$arr_K[15]) + ($arr_K[16]*$arr_K[16]) + ($arr_K[17]*$arr_K[17])),2); ?></td>

                            <td><?php  $arr_SQRT[4]=number_format(SQRT(($arr_T[1]*$arr_T[1])+ ($arr_T[2]*$arr_T[2]) + ($arr_T[3]*$arr_T[3]) + ($arr_T[4]*$arr_T[4]) + ($arr_T[5]*$arr_T[5]) + ($arr_T[6]*$arr_T[6]) + ($arr_T[7]*$arr_T[7]) + ($arr_T[8]*$arr_T[8]) + ($arr_T[9]*$arr_T[9]) + ($arr_T[10]*$arr_T[10]) + ($arr_T[11]*$arr_T[11]) + ($arr_T[12]*$arr_T[12]) + ($arr_T[13]*$arr_T[13]) + ($arr_T[14]*$arr_T[14]) + ($arr_T[15]*$arr_T[15]) + ($arr_T[16]*$arr_T[16]) + ($arr_T[17]*$arr_T[17])),2); ?></td>
                       <!--  </tr>                      
                    </table> -->
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6">
                      <br>
                      <h3 class="block-header">Normalisasi Matriks (R)</h3> 
                      <table class="table table-bordered">
                        <tr>
                            <th>Rekomendasi</th>
                            <th>Nitrogen</th>
                            <th>Posfor</th>
                            <th>Kalium</th>
                            <th>Tekstur</th>
                        </tr>
                        <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                        <tr>                
                            <td><?php echo $row->kd_formula; ?></td>
                            <td><?php echo $arr_nor[1][$i]=number_format(($row->nitrogen2 / $arr_SQRT[1]),3) ; ?></td>
                            <td><?php echo $arr_nor[2][$i]=number_format(($row->posfor2 / $arr_SQRT[2]),3) ; ?></td>
                            <td><?php echo $arr_nor[3][$i]=number_format(($row->kalium2 / $arr_SQRT[3]),3) ; ?></td>
                            <td><?php echo $arr_nor[4][$i]=number_format(($row->tekstur2 / $arr_SQRT[4]),3) ; ?></td>
                        </tr>                      
                        <?php $i++; } ?>
                    </table>
                  </div>

                  <div class="col-sm-6">
                      <br>
                      <h3 class="block-header">Pembobotan Matriks (V: normalisasi * bobot)/ (R*W)</h3> 
                      <table class="table table-bordered">
                        <tr>
                            <th>Rekomendasi</th>
                            <th>Nitrogen</th>
                            <th>Posfor</th>
                            <th>Kalium</th>
                            <th>Tekstur</th>
                        </tr>
                        <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                        <tr>                
                            <td><?php echo $row->kd_formula; ?></td>
                            <td><?php echo $arr_bbt[1][$i]=number_format(($arr_nor[1][$i] * $bbt[1]),3) ; ?></td>
                            <td><?php echo $arr_bbt[2][$i]=number_format(($arr_nor[2][$i] * $bbt[2]),3) ; ?></td>
                            <td><?php echo $arr_bbt[3][$i]=number_format(($arr_nor[3][$i] * $bbt[3]),3) ; ?></td>
                            <td><?php echo $arr_bbt[4][$i]=number_format(($arr_nor[4][$i] * $bbt[4]),3) ; ?></td>
                        </tr>                      
                        <?php  $i++; } ?>
                    </table>
                  </div>
                </div>

                <!-- ///// A  ///// -->
                <div class="row">
                  <div class="col-sm-12">
                      <br>
                      <h3 class="block-header">Nilai A</h3> 
                      <table class="table table-bordered">
                        <tr>
                            <th></th>
                            <th>Nitrogen</th>
                            <th>Posfor</th>
                            <th>Kalium</th>
                            <th>Tekstur</th>
                        </tr>
                        <tr>                
                            <td><?php echo 'A+'; ?></td>
                            <td><?php echo $aPlus[1]=max($arr_bbt[1]) ; ?></td>
                            <td><?php echo $aPlus[2]=max($arr_bbt[2]) ; ?></td>
                            <td><?php echo $aPlus[3]=max($arr_bbt[3]) ; ?></td>
                            <td><?php echo $aPlus[4]=max($arr_bbt[4]) ; ?></td>
                            
                        </tr> 
                        <tr>                
                            <td><?php echo 'A-'; ?></td>
                            <td><?php echo $aMin[1]=min($arr_bbt[1]) ; ?></td>
                            <td><?php echo $aMin[2]=min($arr_bbt[2]) ; ?></td>
                            <td><?php echo $aMin[3]=min($arr_bbt[3]) ; ?></td>
                            <td><?php echo $aMin[4]=min($arr_bbt[4]) ; ?></td>
                        </tr>                      
                    </table>
                  </div>
                </div>
                <!-- <div class="row">
                  <div class="col-sm-12">
                    <?php  echo $x=max($arr_bbt[2]); ?>
                  </div>
                </div> -->
                <!-- ///// A  ///// -->
                <div class="row">
                  <div class="col-sm-6">
                      <br>
                      <h3 class="block-header">Nilai D</h3> 
                      <table class="table table-bordered">
                        <tr>
                            <th>Rekomendasi</th>
                            <th>D+</th>
                            <th>D-</th>
                        </tr>
                        <?php 
                        for ($h=1; $h <= 4 ; $h++) { 
                          for ($i=1; $i <= 17 ; $i++) { 
                            $arr_dPlus[1][$i]= pow(($arr_bbt[$h][$i] - $aPlus[$h]),2) + @$arr_dPlus[1][$i]; 
                            $arr_dMin[1][$i]= pow(($arr_bbt[$h][$i] - $aMin[$h]),2) + @$arr_dMin[1][$i]; 
                          }
                        }
                        ?>
                        <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                        <tr>                
                            <td><?php echo $row->kd_formula; ?></td>
                            <td><?php echo $arr_dPlus[1][$i]=number_format(SQRT($arr_dPlus[1][$i]),3); ?></td>
                            <td><?php echo $arr_dMin[1][$i]=number_format(SQRT($arr_dMin[1][$i]),3); ?></td>
                        </tr>                      
                        <?php $i++; } ?>
                    </table>
                  </div>

                  <div class="col-sm-6">
                      <br>
                      <h3 class="block-header">Nilai V</h3> 
                      <table class="table table-bordered">
                        <tr>
                            <th>Rekomendasi</th>
                            <th>V</th>
                            <!-- <th>Poin</th> -->
                        </tr>
                        <?php $i=1; foreach ($rekomtakaran as $row) { ?>
                        <tr>                
                            <td><?php echo $row->kd_formula; ?></td>
                            <td><?php echo $arr_v[1][$i]=number_format(($arr_dMin[1][$i] /($arr_dMin[1][$i]+$arr_dPlus[1][$i])),3) ; ?></td>
                            <!-- <td><?php echo $arr_v[2][$i]=number_format(($arr_nor[2][$i] * $P),3) ; ?></td> -->
                        </tr>                      
                        <?php  $i++; } ?>
                    </table>
                  </div>
                </div>

                <?php          
                $i=1; foreach ($rekomtakaran as $row) { 
                  $data_poin_topsis = array(                          
                          'id_analisa' => $ujitanah_DSC->id,
                          'kd_formula' => $row->kd_formula,
                          'poin' => $arr_v[1][$i],                            
                          );
                  $insert = $this->Rekomedasi_model->add_poin_topsis($data_poin_topsis);
                $i++;
                }

                ?>

                <div class="row">
                    <div class="col-sm-12">
                        <br>
                        <h3 class="block-header">Hasil Rekomendasi Menggunakan Metode TOPSIS</h3> 
                        <table class="table table-bordered">
                          <tr>
                              <th>No</th>               
                              <th>Formula</th>               
                              <th>Poin</th>               
                              <th>Ranking</th>               
                          </tr>

                          <?php 
                          $poin=$this->Rekomedasi_model->get_poin_topsis($ujitanah_DSC->id);
                          $i=1; foreach ($poin as $row) { 
                          ?>
                          <tr>                
                              <td><?php echo $i; ?></td> 
                              <td><?php echo $row->formula; ?></td> 
                              <td><?php echo $row->poin; ?></td> 
                              <td><?php echo $i; ?></td>                 
                          </tr>                      
                          <?php $i++; } 
                          ?>
                      </table>
                    </div>
                </div>
            
                <!-- //////// -->
              </div>

            <div class="tab-pane fade" id="tab_1_3">
                <!-- /////////// -->
                <div class="row">
                  <div class="col-sm-12">
                    <center>
                      <h2 class="block-header">PERBANDINGAN</h2> 
                    </center>
                  </div>
                </div>


                <div class="row">
                  <div class="col-sm-5">
                      <br>
                      <h3 class="block-header">Hasil Rekomendasi Menggunakan Metode ELECTRE</h3> 
                      <table class="table table-bordered">
                        <tr>
                            <th>No</th>               
                            <th>Formula</th>               
                            <th>Poin</th>               
                            <th>Ranking</th>               
                        </tr>

                        <?php 
                        $poin=$this->Rekomedasi_model->get_poin($ujitanah_DSC->id);
                        $i=1; foreach ($poin as $row) { 
                        ?>
                        <tr>                
                            <td><?php echo $i; ?></td> 
                            <td><?php echo $row->formula; ?></td> 
                            <td><?php echo $row->poin; ?></td> 
                            <td><?php echo $i; ?></td>                 
                        </tr>                      
                        <?php $i++; } 
                        ?>
                    </table>
                  </div>

                    <div class="col-sm-5">
                        <br>
                        <h3 class="block-header">Hasil Rekomendasi Menggunakan Metode TOPSIS</h3> 
                        <table class="table table-bordered">
                          <tr>
                              <th>No</th>               
                              <th>Formula</th>               
                              <th>Poin</th>               
                              <th>Ranking</th>               
                          </tr>

                          <?php 
                          $poin=$this->Rekomedasi_model->get_poin_topsis($ujitanah_DSC->id);
                          $i=1; foreach ($poin as $row) { 
                          ?>
                          <tr>                
                              <td><?php echo $i; ?></td> 
                              <td><?php echo $row->formula; ?></td> 
                              <td><?php echo $row->poin; ?></td> 
                              <td><?php echo $i; ?></td>                 
                          </tr>                      
                          <?php $i++; } 
                          ?>
                      </table>
                    </div>    
                    <div class="col-sm-2">
                      <p><u>Keterangan :</u>
                        <br><i>Rekomendasi Terbaik ditunjukkan dari ranking satu.</i></p>
                    </div>                
                  </div>
                </div>
                <!-- //////// -->
                <div class="row">
                  <div class="col-sm-11">
                    <center>
                      <?php 
                      $last = $this->Rekomedasi_model->get_dataujitanah_DSC();
                      // var_dump($last);
                      $rank_one = $this->Rekomedasi_model->get_rank_one($last->id);
                       ?>
                      <p>Berdasarkan Hasil Analisis diatas maka takaran kombinasi pupuk tunggal yang direkomendasikan untuk standard 5 ton/ha diperoleh sesuai hasil perangkingan kedua Metode Electre dan Topsis<br>  untuk pilihan rekomendasi terbaik yakni <?php echo $rank_one->kd_formula; ?> - <?php echo $rank_one->formula; ?>.</p> 
                    </center>
                  </div>
                </div>

            </div>

            <br>
           
            <div class="row">
              <div class="col-sm-5">
                <ul class="nav nav-tabs">
                  <li class="active">
                    <a href="#tab_1_1" data-toggle="tab"> ELECTRE </a>
                  </li>
                  <li>
                    <a href="#tab_1_2" data-toggle="tab"> TOPSIS </a>
                  </li>
                  <li>
                    <a href="#tab_1_3" data-toggle="tab"> PERBANDINGAN </a>
                  </li>
                </ul>                
              </div>
              <div class="col-sm-3">
                
              </div>
              <div class="col-sm-4">
                <a href="<?php echo base_url(); ?>rekomendasi"><button class="theme_btn">Kembali</button></a>
                <!-- <a href="<?php echo base_url(); ?>rekomendasi/cetak/<?php echo $ujitanah_DSC->id ?>"><button class="theme_btn">Cetak</button></a> -->
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
