<footer id="footer" class="darkgrey_section">
  <div class="container">
    <div class="row">

    


    <div class="block col-sm-6 subscribe">
      <h3>Kritik dan Saran</h3>
      <form id="signup" action="/" method="get" class="form-inline">
        <div class="form-group">
          <input name="email" id="mailchimp_email" type="email" class="form-control" placeholder="E-Mail">
        </div>
        <br>
        <div class="form-group">
          <textarea aria-required="true" rows="3" cols="22" name="message" id="message" class="form-control" placeholder="Pesan"></textarea>
          <!-- <input name="email" id="mailchimp_email" type="email" class="form-control" placeholder="your@email.com"> -->
        </div>
        <button type="submit" class="theme_btn">Kirim</button>
        <div id="response"></div>
      </form>
    </div>


    <div class="block col-sm-6">
      <h3>About</h3>
      <p>&copy; Copyright 2018. by <a href="http://Handayani.ac.id" target="_blank"> STMIK Handayani Makassar</a></p>

    </div>
    

    </div>
  </div>
</footer>





</div><!-- EOF #box_wrap -->

<div id="gallery_container"></div>

<!-- <div class="preloader">
  <div class="preloaderimg"></div>
</div> -->

  
        <script src="<?php echo base_url(); ?>assets/js/vendor/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/placeholdem.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.10.2.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/hoverIntent.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/superfish.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery.actual.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery.elastislide.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery.flexslider-min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery.prettyPhoto.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery.easing.1.3.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery.ui.totop.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery.isotope.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery.easypiechart.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jflickrfeed.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery.sticky.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/owl.carousel.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery.nicescroll.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery.fractionslider.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery.scrollTo-min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery.localscroll-min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vendor/jquery.parallax-1.1.3.js"></script>
        <script src="<?php echo base_url(); ?>assets/twitter/jquery.tweet.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
        

       
    </body>
</html>