<!-- ========================== Innaer Banner ========================= -->


	<section id="inner_banner">
		<div class="overlay">

			<br><br><br><br>
				<center><h3>Petunjuk Penggunaan Applikasi</h3></center>
		
		</div>
	</section> <!-- /inner_banner -->


<!-- ========================== /Innaer Banner ========================= -->


<!-- ====================== Conatct Information ========================= -->



    <section class="slider-reviews section-large slider-reviews_1-col bg bg_7 bg_transparent">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
        	   
        	   <br><br>
        	   <p><b>Pertama-tama dari tampilan web utama pilih menu MASUK, Maka akan tampil Form login. Jika Anda belum registrasi, silahkan klik tulisan "Klik untuk registrasi".</b></p>
        	   <br>
             <center><img src="<?php echo base_url(); ?>assets/images/petunjuk/1.PNG"></center>               
             <br><br>
        	   <p><b>Anda juga bisa langsung melakukan Registrasi di menu Beranda "Klik REGISTRASI".</b></p>
        	   <br>
             <img src="<?php echo base_url(); ?>assets/images/petunjuk/2.PNG">            <br><br>
        	   <p><b>Maka akan muncul form Registrasi, masukkan data Pengguna kemudian simpan.</b></p>
        	   <br>
             <center><img src="<?php echo base_url(); ?>assets/images/petunjuk/regis.PNG"></center>
             <br><br>
        	   <p><b>Kemudian Akan muncul form login silahkan masukkan Username dan password, lalu klik Masuk.</b></p>
        	   <br>
             <center><img src="<?php echo base_url(); ?>assets/images/petunjuk/1.PNG"></center>
             <br><br>
        	   <p><b>Untuk memulai Sistem Pendukung Keputusan (SPK) Rekomendasi Takaran, silahkan klik Menu REKOMENDASI, Kemudian input nilai unsur hara dan tekstur tanah lahan, lalu klik tombol KIRIM.</b></p>
        	   <br>
              <img src="<?php echo base_url(); ?>assets/images/petunjuk/3.PNG">
             <br><br>
             <p><b>Maka akan tampil hasil Rekomendasi ELECTRE dan TOPSIS.</b></p>
             <br>
             <img src="<?php echo base_url(); ?>assets/images/petunjuk/4.PNG">
             <br><br>
             <p><b>Untuk melihat histori SPK, silahkan klik Menu HASIL, Untuk mencetak / mendownload hasil Rekomendasi, silahkan klik tulisan Cetak.</b></p>
             <br>
        	   <img src="<?php echo base_url(); ?>assets/images/petunjuk/5.PNG"> 
             <br><br><br>

             <p><b>Demikian, petunjuk penggunaan applikasi SPKPUKDI Selamat mencoba.<br>Mohon memperhatikan data inputan hasil uji tanah sebelum mengklik tombol KIRIM pada form input data uji tanah.</b></p>
             <br><br><br><br><br><br><br><br><br><br>  
         
         
        </div>
      </div>
    </div>
  </section>




<!-- ====================== /Conatct Information ========================= -->