<section id="middle" class="color_section"><div class="container"><div class="row blog">
  <div class="col-sm-12">
    <div class="content-area" id="primary">
      <div role="main" class="site-content" id="content">

      <?php foreach ($list as $row) { ?>
        <article class="post format-standard">
          <header class="entry-header">
            <div class="entry-thumbnail">
              <img alt="" src="<?php echo base_url(); ?>upload/<?php echo $row->path ?>">
            </div>
            <h2 class="entry-title">
              <a style="color: #ffffff;" href="<?php echo base_url(); ?>informasi/read/<?php echo $row->url ?>" rel="bookmark"><?php echo $row->name ?></a>
            </h2>
            <div class="entry-meta">
              <!--By
               <span class="author vcard">
                <a rel="author" href="#" class="url fn n">Alan Smith</a>
              </span>
              <span class="categories-links">
                In
                <a rel="category" href="#">Graphic Design</a>, 
                <a rel="category" href="#">Photography</a>
              </span> -->
              <span class="date">
                Posted at 
                  <time datetime="2013-08-08T15:05:23+00:00" class="entry-date"><?php echo $row->create_at ?></time>
              </span>
              <!-- <span class="comments-link">
                <a href="#">5 comments</a>
              </span> -->
            </div>
            <!-- .entry-meta --> 
          </header>
          <!-- .entry-header -->
          
          <div class="entry-content">
            <?php if($this->uri->segment(2)==""){echo $row->dest2.'...';} else {echo $row->dest;} ?>
            
          </div>
          <!-- .entry-content -->
          <!-- <div class="entry-tags">
              <span class="tags-links">
                <a rel="tag" href="#">Graphic Design</a>, 
                <a rel="tag" href="#">Photography</a>
              </span>
            </div> -->
            <!-- .entry-tags -->
        </article>
        <!-- .post --> 
      <?php } ?>

        
         
        
      </div>
      <!-- #content --> 
    </div>
    <!-- <div class="text-center"> 
      <ul class="pagination">
        <li class="active"><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
      </ul>
    </div>  -->
  </div>






</div></div></section>