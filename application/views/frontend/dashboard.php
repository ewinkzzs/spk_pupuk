<section id="mainslider"><div class="flexslider">
  <ul class="slides">

    <li>
      <img class="fullwidthimage" alt="Full width image" src="<?php echo base_url(); ?>assets/images/tani.png">
      <div class="container">
        <div class="row fullwidthimage">
          <div class="col-sm-12">
            <div class="slide_description">
              <div class="imagetitle">Welcome!</div>
              <div class="imagedesc">Selamat Datang di Official Website SPKPUKDI BPTP.</div>
              <a href="#" class="theme_btn">Know More</a> </div>
          </div>
        </div>
      </div>
    </li>
    
    <li>
      <img class="fullwidthimage" alt="Full width image" src="<?php echo base_url(); ?>assets/images/padi2.jpg">
      <div class="container">
        <div class="row fullwidthimage">
          <div class="col-sm-12">
            <div class="slide_description">
              <div class="imagetitle">Hello!</div>
              <div class="imagedesc">Untuk mengetahui petunjuk penggunaan Applikasi SPKPUKDI BPTP.</div>
              <a href="#" class="theme_btn">Pilih Menu Petunjuk</a> </div>
          </div>
        </div>
      </div>
    </li>
    
    <li>
      <img class="fullwidthimage" alt="Full width image" src="<?php echo base_url(); ?>assets/images/padi1.jpg">
      <div class="container">
        <div class="row fullwidthimage">
          <div class="col-sm-12">
            <div class="slide_description">
              <div class="imagetitle">Got To Know!</div>
              <div class="imagedesc">Untuk mengetahui Rekomendasi pemupukan silahkan.</div>
              <a href="#" class="theme_btn">Pilih Menu Rekomendasi</a> </div>
          </div>
        </div>
      </div>
    </li>

  </ul>
</div>
</section>

<div id="box_wrap">

<section id="about" class="color_section title_section">
  <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <img src="<?php echo base_url(); ?>assets/example/teaser1.jpg" alt="" class="img-circle">
        </div>
        <div class="col-sm-8">
          <h3>Some Information About Us</h3>
          <p><b>SPKPUKDI</b> adalah sebuah applikasi dengan metode yang dapat membantu memberi keputusan rekomendasi pemupukan untuk pupuk tunggal (Urea, SP36, Kcl) tanaman pangan khususnya tanaman padi yang disebut sistem pendukung keputusan pemupukan tanaman padi (SPKPUKDI). Dengan adanya Sistem pendukung keputusan Penentuan rekomendasi takaran Pupuk anorganik tanaman pangan  akan meningkatkan akses informasi dan pengelolaan data terpadu para peneliti dan penyuluh untuk menjaga ekspektasi petani dalam pemberian takaran pupuk dan mendukung upaya peningkatan efisiensi pengelolaan pertanian sehingga pada suatu waktu petani membutuhkan rekomendasi takaran pemupukan sebelum jangka waktu peninjauan kembali peneliti dapat menggunakan applikasi ini untuk memberikan rekomendasi takaran pemupukan sesuai dengan analisanya untuk diolah oleh sistem untuk memperoleh keputusan rekomendasi takaran pemupukan terbaik atau petani atau masyarakat dapat menggunakan applikasi secara langsung.</p>

          <p>Metode Elimination And Choice Expressing Reality (ELECTRE) dan Metode Technique for Order Performance by Similarity to Idea Solution (TOPSIS) merupakan metode analisis pengambilan keputusan multikriteria dengan konsep outrangking dengan menggunakan perbandingan berpasangan dari alternatif-alternatif berdasarkan setiap kriteria yang sesuai (Ningsih, Rekyan, dan Agus 2016).</p>
          

          <?php if ($this->session->userdata('username') == '') { ?> 
          <p> <b>Silahkan Melakukan Proses Registrasi untuk menggunakan Applikasi ! </b> </p>
          <a onclick="add()" class="theme_btn">Registrasi</a>
          <?php  }?>
        </div>
      </div>
  </div>
</section>

<section id="features" class="color_section with-top-border with-bottom-border">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <blockquote>
          <p>Pemupukan berimbang adalah upaya pemenuhan kebutuhan hara tanaman agar dapat mencapai hasil optimal (tanpa kelebihan/kekurangan hara) melalui pemberian pupuk dengan mempertimbangkan jumlah hara yang telah tersedia di dalam tanah.</p>
          <h3 class="text-right">Makarim</h3>
        </blockquote>
      </div>
    </div>
  </div>
</section>

<section class="color_section">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h3>Get To Know</h3>

      <!-- Horizontal Slider --> 

        <div class="horizontal_slider_list_wrapper related-posts">
          <div id="horizontal_slider">
            <ul class="horizontal_slider_list">
              <li>
                <article class="post format-standard">
                      <header class="entry-header">
                        <div class="entry-thumbnail">
                          <img src="<?php echo base_url(); ?>assets/example/takaran.jpg" class="post-image" alt="">
                        </div>
                      </header>
                      <!-- .entry-header -->
                      
                      <div class="entry-content">
                        <h2 class="entry-title">
                          <a href="#" rel="bookmark">Post With Image</a>
                        </h2>
                        <p class="date">
                          <span class="entry-date">April 5, 2018</span>
                        </p>
                        <p>Untuk Mengetahui rekomendasi takaran pemupukan padi perlu melakukan penginputan nilai dari kondisi unsur hara dan tekstur tanah karena Struktur tanah dengan kondisi unsur hara yang berbeda-beda disetiap tempat.</p>
                        
                      </div>
                      <!-- .entry-content -->
                      
                    </article>

              </li>
             
              <li>
                <article class="post format-video">
                      <header class="entry-header">
                        <div class="entry-thumbnail">
                          <img src="<?php echo base_url(); ?>assets/example/intro_img2.jpg" class="post-image" alt="">
                        </div>
                      </header>
                      <!-- .entry-header -->
                      
                      <div class="entry-content">
                        <h2 class="entry-title">
                          <a href="#" rel="bookmark">Post With Image</a>
                        </h2>
                        <p class="date">
                          <span class="entry-date">July 15, 2018</span>
                        </p>
                        <p>Pengambilan sample tanah diperlukan untuk melakukan proses uji tanah untuk mengetahui status unsur hara dan tekstur tanah pada lahan. Sample tanah selanjutnya diuji pada Lab Tanah atau dengan Petak Omisi.</p>
                        
                      </div>
                      <!-- .entry-content -->
                      
                    </article>

              </li>
             
              <li>
                <article class="post format-gallery">
                      <header class="entry-header">
                        <div class="entry-thumbnail">
                          <img src="<?php echo base_url(); ?>assets/example/intro_img3.jpg" class="post-image" alt="">
                        </div>
                      </header>
                      <!-- .entry-header -->
                      
                      <div class="entry-content">
                        <h2 class="entry-title">
                          <a href="#" rel="bookmark">Post With Image</a>
                        </h2>
                        <p class="date">
                          <span class="entry-date">July 15, 2018</span>
                        </p>
                        <p>Teknik pemupukan tanaman padi sangat relatif, Struktur tanah dengan kondisi unsur hara yang berbeda-beda di tempat satu dengan yang lainnya, memerlukan cara-teknik yang berbeda dalam pemupukan padi.</p>
                        
                      </div>
                      <!-- .entry-content -->
                      
                    </article>

              </li>
             
              <li>
                <article class="post format-standard">
                      <header class="entry-header">
                        <div class="entry-thumbnail">
                          <img src="<?php echo base_url(); ?>assets/example/intro_img4.png" class="post-image" alt="">
                        </div>
                      </header>
                      <!-- .entry-header -->
                      
                      <div class="entry-content">
                        <h2 class="entry-title">
                          <a href="#" rel="bookmark">Post With Image</a>
                        </h2>
                        <p class="date">
                          <span class="entry-date">July 15, 2018</span>
                        </p>
                        <p>Pemupukan tanaman pangan dapat berupa pupuk tunggal dan atau majemuk, SPKPUKDI ini akan memberikan rekomendasi pupuk tunggal UREA, SP36 dan KCL sebagai jenis pupuk yang paling sering dibutuhkan.</p>
                        
                      </div>
                      <!-- .entry-content -->
                      
                    </article>

              </li>


            </ul>
        </div>
      </div>

    <!--EOF Horizontal Slider --> 
         
        </div>
      </div>
    
  </div>
</section>

<section class="color_section">
  <div class="container">
    <div class="row">
      

    <!--EOF Horizontal Slider --> 
         
        </div>
      </div>
    
  </div>
</section>

<script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;
 
     
    function add()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
 
    
 
    function save()
    {
      var url;

        var nama1 = document.frmOnline.txtnama; 

        var email1 = document.frmOnline.txtemail; 

        var user1 = document.frmOnline.txtuser;

        var pass1 = document.frmOnline.txtpass;

        if (nama1.value == "") {
            alert("Nama Tidak Boleh Kosong");
            txtnama.focus();
            return false;
        }

        if (email1.value == "") {
            alert("Email Tidak Boleh Kosong");
            txtemail.focus();
            return false;
        }
        if (user1.value == "") {
            alert("Username Tidak Boleh Kosong");
            txtuser.focus();
            return false;
        }
        if (pass1.value == "") {
            alert("Password Tidak Boleh Kosong");
            txtpass.focus();
            return false;
        }

      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('login/save')?>";
      }
      else
      {
        url = "<?php echo site_url('')?>";
      }
 
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               $('#modal_form').modal('hide');
              location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              alert('Data berhasil disimpan, silahkan masuk');
              $('#modal_form').modal('hide');
              location.reload();// for reload a page
            }
        });
    }
 
    
 
  </script>
 
  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <br><br>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <h4 class="modal-title">Silahkan Registrasi</h4>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal" name="frmOnline">
          <div class="row">
          <div class="col-lg-10 col-md-offset-1">  
            <!-- //nik,nama,jk,no_telp,pekerjaan,email,username,`passwornd` -->
          <input type="hidden" value="" name="id"/>

          <div class="form-group ">
                    <div class="col-xs-4">
                        <h5>Nama Lengkap</h5>
                    </div>
                    <div class="col-xs-8">
                    <input class="form-control" type="text" name="nama" placeholder="Nama Lengkap" id="txtnama">                  
                  </div>
              </div>  

              <div class="form-group ">
                    <div class="col-xs-4">
                        <h5>Jenis Kelamin</h5>
                    </div>
                    <div class="col-xs-8">
                    <select class="form-control" name="jk" id="txtjk">
                            <option value="-">-</option>
                            <option value="Laki-laki">Laki-laki</option>
                            <option value="Perempuan">Perempuan</option>                            
                    </select>  
                  </div>
              </div> 

              <div class="form-group ">
                    <div class="col-xs-4">
                        <h5>Tempat Lahir</h5>
                    </div>
                    <div class="col-xs-8">
                    <input class="form-control" type="text" placeholder="Tempat Lahir" name="tmp_lahir" id="txttmp_lahir">                 
                  </div>
              </div> 

              <div class="form-group ">
                    <div class="col-xs-4">
                        <h5>Tanggal Lahir</h5>
                    </div>
                    <div class="col-xs-8">
                    <input class="form-control" type="date" name="tgl_lahir" id="txttgl_lahir">                 
                  </div>
              </div> 

              <div class="form-group ">
                    <div class="col-xs-4">
                        <h5>No. Telepon</h5>
                    </div>
                    <div class="col-xs-8">
                    <input class="form-control" type="text" placeholder="No. Telepon" name="no_telp" id="txtno_telp">                 
                  </div>
              </div> 

              <div class="form-group ">
                    <div class="col-xs-4">
                        <h5>Pekerjaan</h5>
                    </div>
                    <div class="col-xs-8">
                    <input class="form-control" type="text" placeholder="Pekerjaan" name="pekerjaan" id="txtpekerjaan">                 
                  </div>
              </div> 

              
              <div class="form-group ">
                    <div class="col-xs-4">
                        <h5>E-Mail</h5>
                    </div>
                    <div class="col-xs-8">
                    <input class="form-control" type="email" name="email" placeholder="E-mail" id="txtemail">                 
                  </div>
              </div>       
                            
               <div class="form-group ">
                    <div class="col-xs-4">
                        <h5>User Name</h5>
                    </div>
                    <div class="col-xs-8">
                    <input class="form-control" type="text" placeholder="User Name" name="username" id="txtuser">                 
                  </div>
              </div>    
               
              <div class="form-group ">
                    <div class="col-xs-4">
                        <h5>Password</h5>
                    </div>
                    <div class="col-xs-8">
                    <input class="form-control" type="password" placeholder="Password" name="password" id="txtpass">
                  </div>
              </div>   
              <div class="modal-footer">              
              <center>
              <button type="button" id="btnSave" onclick="save()" class="btn btn-info waves-effect waves-light">Simpan</button> 
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
              </center>
            </div>
            </div>
            </div>
        </form>
          </div>
          
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->