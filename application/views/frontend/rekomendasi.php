<section id="contact" class="color_section">
  <div class="container">
    
    <div class="row">
      <div class="col-sm-4">
        <h2 class="block-header">Masukkan Hasil Uji Tanah</h2>
        <div class="contact-form">
          <?php echo validation_errors(); ?>
          <form class="form-origin" method="POST" action="<?php echo base_url(); ?>rekomendasi/proses">
              <div class="form-group">
                    <span class="required">Nama Sample</span>
                    <input type="text" class="form-control" name="nama_sample" id="nama_sample">          
              </div>

              <div class="form-group">
                    <span class="required">Lokasi Sample</span>
                    <select class="form-control" name="id_lokasisample">
                      <option value="0">Pilih</option>
                      <?php foreach ($lokasisample as $row) { ?>
                        <option value="<?php echo $row->id; ?>"><?php echo $row->kec; ?> <?php echo $row->kab_kota; ?></option>
                      <?php } ?>
                    </select>                
              </div>

              <div class="form-group">
                    <span class="required">Nitrogen (N)</span>
                    <select class="form-control" name="nitrogen">
                        <option value="0">Pilih</option>
                        <option value="< 0.20 (Rendah)">< 0.20 (Rendah)</option>
                        <option value="0.21-0.50 (Sedang)">0.21-0.50 (Sedang)</option>
                        <option value="> 0.50 (Tinggi)">> 0.50 (Tinggi)</option>
                    </select>                
              </div>

              <div class="form-group">
                    <span class="required">Posfor (P)</span>
                    <select class="form-control" name="posfor">
                        <option value="0">Pilih</option>
                        <option value="< 20 (Rendah)">< 20 (Rendah)</option>
                        <option value="20-40 (Sedang)">20-40 (Sedang)</option>
                        <option value="> 40 (Tinggi)">> 40 (Tinggi)</option>
                    </select>                
              </div>

              <div class="form-group">
                    <span class="required">Kalium (K)</span>
                    <select class="form-control" name="kalium">
                        <option value="0">Pilih</option>
                        <option value="< 10 (Rendah)">< 10 (Rendah)</option>
                        <option value="10-20 (Sedang)">10-20 (Sedang)</option>
                        <option value="> 20 (Tinggi)">> 20 (Tinggi)</option>
                    </select>                
              </div>

              <div class="form-group">
                    <span class="required">Tekstur Tanah</span>
                    <select class="form-control" name="tekstur">
                        <option value="0">Pilih</option>
                        <option value="Liat">Liat</option>
                        <option value="Sedang">Sedang</option>
                        <option value="Berpasir">Berpasir</option>
                    </select>                
              </div>
              <button type="submit" class="theme_btn">Kirim</button>
            
          </form>
        </div>
      </div>

    </div>
  </div>
</section>