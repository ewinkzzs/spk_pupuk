<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<!-- For Resposive Device -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>SPKPUKDI BPTP</title>
	<!-- Favicon -->
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/images/logo.png">
	<!-- Custom Css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animations.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
    <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
</head>
    <body>

<section id="header" class="dark_section">
    <div class="container"><div class="row">

      <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/example/logo2.png" alt=""></a>

      <div class="col-sm-12 mainmenu_wrap"><div class="main-menu-icon visible-xs"><span></span><span></span><span></span></div>
        <ul id="mainmenu" class="nav menu sf-menu responsive-menu superfish">

          <li class="<?php if($this->uri->segment(1)==""){echo "active";}?>">
            <a href="<?php echo base_url(); ?>">Beranda</a>
          </li>

          <!-- <li class="dropdown">
            <a href="./about.html">Pages</a>
            <ul class="nav dropdown-menu">
              <li class="dropdown">
                <a href="./blog.html">Blog</a>
                <ul class="nav dropdown-menu">
                  <li class="">
                    <a href="./blog.html">Blog</a>
                  </li>
                  <li class="">
                    <a href="./blog-single.html">Blog Post</a>
                  </li>
                </ul>
              </li>
              <li class="dropdown">
                <a>Gallery</a>
                <ul class="nav dropdown-menu">
                  <li class="">
                    <a href="<?php echo base_url(); ?>informasi">Gallery</a>
                  </li>
                  <li class="">
                    <a href="./gallery-single.html">Gallery Item</a>
                  </li>
                </ul>
              </li>
              <li class="">
                <a href="./about.html">About Us</a>
              </li>
              <li class="">
                <a href="./services.html">Services</a>
              </li>
              <li class="">
                <a href="./404.html">404</a>
              </li>
              <li class="">
                <a href="./icons.html">Icons</a>
              </li>

            </ul>
          </li>
          <li class="dropdown">
            <a href="./blog.html">Blog</a>
            <ul class="nav dropdown-menu">
              <li class="">
                <a href="./blog.html">Blog</a>
              </li>
              <li class="">
                <a href="./blog-single.html">Blog Post</a>
              </li>
            </ul>
          </li> -->

          <li class="<?php if($this->uri->segment(1)=="informasi"){echo "active";}?>" >
             <a href="<?php echo base_url(); ?>informasi">Informasi</a>
          </li>

          <?php if ($this->session->userdata('username') != '') { ?> 
          <li class="<?php if($this->uri->segment(1)=="rekomendasi"){echo "active";}?>" >
             <a href="<?php echo base_url(); ?>rekomendasi">Rekomendasi</a>
          </li>
          <li class="<?php if($this->uri->segment(1)=="hasil_rekomendasi"){echo "active";}?>" >
             <a href="<?php echo base_url(); ?>hasil_rekomendasi">Hasil</a>
          </li>
          <?php  }?>
          <!-- <li class="dropdown">
            <a href="">Gallery</a>
            <ul class="nav dropdown-menu">
              <li class="">
                <a href="<?php echo base_url(); ?>informasi">Gallery</a>
              </li>
              <li class="">
                <a href="./gallery-single.html">Gallery Item</a>
              </li>
            </ul>
          </li>    -->       
          <li class="<?php if($this->uri->segment(1)=="petunjuk"){echo "active";}?>">
             <a href="<?php echo base_url(); ?>petunjuk"">Petunjuk</a>
          </li>
          <?php if ($this->session->userdata('username') != '') { ?> 
                
                <!-- <a href="admin/logout"><strong>Keluar</strong></a> -->
                <li class="">
                   <a href="<?php echo base_url(); ?>admin/logout"> Keluar</a>
                </li>
                <?php } else { ?>
                  <li class="">
                     <a href="<?php echo base_url(); ?>login">Masuk</a>
                  </li>
                <?php  }?>
          

        </ul>

    </div></div>
</section>

<!-- ========================== /Header =================== -->
