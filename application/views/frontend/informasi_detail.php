<section id="middle" class="color_section"><div class="container"><div class="row blog-single">

  <div class="col-sm-9">
    <div class="content-area" id="primary">
      <div role="main" class="site-content" id="content">
        <article class="post type-post">
          <header class="entry-header">
            <h1 class="entry-title">Post with Large Image</h1>
            <div class="entry-thumbnail">
              <img alt="" src="example/blog-post.jpg">
            </div>
            <div class="entry-meta">
              By
              <span class="author vcard">
                <a rel="author" href="#" class="url fn n">Alan Smith</a>
              </span>
              <span class="categories-links">
                In
                <a rel="category" href="#">Graphic Design</a>, 
                <a rel="category" href="#">Photography</a>
              </span>
              <span class="date">
                Posted
                  <span class="entry-date">August 8, 2013</span>
              </span>
              <span class="comments-link">
                <a href="#">5 comments</a>
              </span>
            </div>
            <!-- .entry-meta --> 
          </header>
          <!-- .entry-header -->
          
          <div class="entry-content">
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut  wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.
            </p>
            <p>
              Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel <a href="#">illum dolore eu feugiat</a> nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. At vero eos et accusam et justo duo.
            </p>
            <blockquote>
              At vero eos et accusam et justo duo dolores et ea rebum. Lorem ipsum dolor sit amet.
            </blockquote>
            <p>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum.
            </p>
            <p>
              Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
            </p>
            <p>
              <img class="alignleft" alt="" src="example/blog-post-small.jpg">
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea et gubergren, kasd magna no rebum. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.
            </p>
            <p>
              Dolore magna aliquam erat volutpat. Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. <span class="highlight">Lorem ipsum dolor</span> sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores duo eirmod eos erat.
            </p>
            <p>
              At vero eos et accusam et justo duo dolores et ea rebum. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. At vero eos et accusam et justo duo dolores et ea rebum. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            </p>
          </div>
          <!-- .entry-content -->
          
          <footer class="bottom-entry-meta">
            <div class="entry-tags row">
              <div class="col-sm-6">
                <span class="tags-links">
                  <a rel="tag" href="#">Graphic Design</a>, 
                  <a rel="tag" href="#">Photography</a>
                </span>
              </div>
              <div class="col-sm-6">  
                <span class='st_facebook_hcount' displayText='Facebook'></span>
                <span class='st_twitter_hcount' displayText='Tweet'></span>
                <span class='st_googleplus_hcount' displayText='Google +'></span>
              </div>
            </div>
            <div class="author-meta row">
              <div class="col-sm-4 author-image">
                <img src="example/team1.jpg" alt="">
              </div>
              <div class="col-sm-8">
                  <h3>Jason Willis
                  <span class="author-social">
                    <a class="socialico-twitter" href="#" title="Twitter">#</a>
                    <a class="socialico-facebook" href="#" title="Facebook">#</a>
                    <a class="socialico-linkedin" href="#" title="LinkedIn">#</a>
                  </span>
                  </h3>
                  <p>
                    Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Stet clita kasd gubergren, no sea takimata sanctus est. Ut wisi enim ad minim veniam, quisostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip.
                  </p>
              </div>
            </div>
          </footer>  

            <div class="related-posts block">
              <h3>Related Posts</h3>
              
              <div class="row">
                <div class="col-sm-4">
                    <article class="post format-standard">
                      <div class="entry-thumbnail">
                        <img alt="" src="example/blog-secondary.jpg">
                      </div>
                      <h3><a href="#">Post With Image</a></h3>
                      <p class="date">
                        <span class="entry-date">Jan 8, 2014</span>
                      </p>
                      <p class="introtext">Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</p>
                      <p class="entry-share">
                          <a class="socialico-facebook" href="#" title="Facebook">#</a>
                          <a class="socialico-twitter" href="#" title="Twitter">#</a>
                          <a class="socialico-google" href="#" title="Google">#</a>
                      </p>
                     </article>
                  </div>

                  <div class="col-sm-4">  
                    <article class="post format-video">
                      <div class="entry-thumbnail">
                        <img src="example/blog-secondary.jpg" class="post-image" alt="">
                        <a class="video-view prettyPhoto" title="" rel="prettyPhoto" href="http://www.youtube.com/watch?v=Vpg9yizPP_g"></a>
                      </div>
                      <h3><a href="#">Post With Video</a></h3>
                      <p class="date">
                        <span class="entry-date">Jan 12, 2014</span>
                      </p>
                      <p class="introtext">Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</p>
                      <p class="entry-share">

                          <a class="socialico-facebook" href="#" title="Facebook">#</a>
                          <a class="socialico-twitter" href="#" title="Twitter">#</a>
                          <a class="socialico-google" href="#" title="Google">#</a>

                      </p>
                     </article>
                  </div>
                  
                  <div class="col-sm-4">
                    <article class="post format-gallery">
                      <div class="entry-thumbnail">
                        <div id="carousel-generic" class="carousel slide">
                          <!-- Indicators -->
                          <ol class="carousel-indicators">
                            <li data-target="#carousel-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-generic" data-slide-to="1" class=""></li>
                            <li data-target="#carousel-generic" data-slide-to="2" class=""></li>
                          </ol>

                          <!-- Wrapper for slides -->
                          <div class="carousel-inner">
                            <div class="item active">
                              <img src="example/blog-secondary.jpg" alt="image">
                            </div>
                            <div class="item">
                              <img src="example/blog-secondary.jpg" alt="image">
                            </div>
                            <div class="item">
                              <img src="example/blog-secondary.jpg" alt="image">
                            </div>
                          </div>

                          <!-- Controls -->
                          <a class="left carousel-control" href="#carousel-generic" data-slide="prev">
                            <span class="icon-prev"></span>
                          </a>
                          <a class="right carousel-control" href="#carousel-generic" data-slide="next">
                            <span class="icon-next"></span>
                          </a>
                        </div>
                      </div>
                      <h3><a href="#">Post With Gallery</a></h3>
                      <p class="date">
                        <span class="entry-date">Jan 18, 2014</span>
                      </p>
                      <p class="introtext">Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</p>
                      <p class="entry-share">
                        
                          <a class="socialico-facebook" href="#" title="Facebook">#</a>
                          <a class="socialico-twitter" href="#" title="Twitter">#</a>
                          <a class="socialico-google" href="#" title="Google">#</a>
                        
                      
                    </p>
                  </article>
                </div>

              </div>
            </div>
          
          <!-- .entry-meta --> 
        </article>
        <!-- #post -->
        
        <div class="comments-area" id="comments">
          <h2 class="comments-title">Showing 5 comments</h2>
          <ol class="comment-list">
            <li class="comment even thread-even depth-1 parent">
              <article class="comment-body">
                <footer class="comment-meta">
                  <div class="comment-author vcard"> <img class="avatar" src="example/team1.jpg" alt=""> <a class="author_url" rel="external nofollow" href="#">Alan Smith</a></div>
                  <!-- .comment-author -->
                  
                  <div class="comment-metadata">
                    <span>17.10.2013 10:30</span>
                  </div>
                    <div class="reply"> <a href="#" class="comment-reply-link">Reply</a> </div>
                  <!-- .reply --> 
                <!-- .comment-metadata --> 
                  
                </footer>
                <!-- .comment-meta -->
                
                <div class="comment-content">
                  <p>Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. At vero eos et accusam et justo duo dolores et ea rebum. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.</p>
                </div>
                <!-- .comment-content -->
                
              </article>
              <!-- .comment-body -->
              <ol class="children">
                <li class="comment byuser odd alt depth-2 parent">
                  <article class="comment-body">
                    <footer class="comment-meta">
                      <div class="comment-author vcard">
                        <img class="avatar" src="example/team2.jpg" alt="">
                        <a class="author_url" rel="external nofollow" href="#">Alan Smith</a>
                      </div>
                      <!-- .comment-author -->
                      
                      <div class="comment-metadata"> 
                          <span>17.10.2013 10:30</span>
                        </div>
                        <div class="reply"> <a href="#" class="comment-reply-link">Reply</a> </div>
                        <!-- .reply --> 
                      <!-- .comment-metadata --> 
                      
                    </footer>
                    <!-- .comment-meta -->
                    
                    <div class="comment-content">
                      <p>Odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. At vero eos et accusam et justo duo dolores rebum.</p>
                    </div>
                    <!-- .comment-content -->
                    
                  </article>
                  <!-- .comment-body -->
                  <ol class="children">
                    <li class="comment byuser even depth-3">
                      <article class="comment-body">
                        <footer class="comment-meta">
                          <div class="comment-author vcard"> <img class="avatar" src="example/team3.jpg" alt=""> <a class="author_url" rel="external nofollow" href="#">Alan Smith</a></div>
                          <!-- .comment-author -->
                          
                          <div class="comment-metadata">
                            <span>17.10.2013 10:30</span>
                          </div>
                            <div class="reply"> <a href="#" class="comment-reply-link">Reply</a> </div>
                            <!-- .reply --> 
                          <!-- .comment-metadata --> 
                          
                        </footer>
                        <!-- .comment-meta -->
                        
                        <div class="comment-content">
                          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.</p>
                        </div>
                        <!-- .comment-content -->
                        
                      </article>
                      <!-- .comment-body --> 
                    </li>
                    <!-- #comment-## -->
                  </ol>
                  <!-- .children --> 
                </li>
                <!-- #comment-## -->
              </ol>
              <!-- .children --> 
            </li>
            <!-- #comment-## -->
            <li class="comment byuser odd alt thread-odd thread-alt depth-1">
              <article class="comment-body">
                <footer class="comment-meta">
                  <div class="comment-author vcard">
                    <img class="avatar" src="example/team4.jpg" alt="">
                    
                      <a class="author_url" rel="external nofollow" href="#">Alan Smith</a>
                    
                  </div>
                  <!-- .comment-author -->
                  
                  <div class="comment-metadata">
                    <span>17.10.2013 10:30</span>
                  </div>
                    <div class="reply"> <a href="#" class="comment-reply-link">Reply</a> </div>
                    <!-- .reply --> 
                  <!-- .comment-metadata --> 
                  
                </footer>
                <!-- .comment-meta -->
                
                <div class="comment-content">
                  <p>Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. At vero eos et accusam et justo duo dolores et ea rebum. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.</p>
                </div>
                <!-- .comment-content -->
                
              </article>
              <!-- .comment-body --> 
            </li>
            <!-- #comment-## -->
            <li class="comment byuser even thread-even depth-1">
              <article class="comment-body">
                <footer class="comment-meta">
                  <div class="comment-author vcard">
                    <img class="avatar" src="example/team1.jpg" alt="">
                    
                      <a class="author_url" rel="external nofollow" href="#">Alan Smith</a>
                    
                  </div>
                  <!-- .comment-author -->
                  
                  <div class="comment-metadata">
                    <span>17.10.2013 10:30</span>
                  </div>
                    <div class="reply"> <a href="#" class="comment-reply-link">Reply</a> </div>
                    <!-- .reply --> 
                  <!-- .comment-metadata --> 
                  
                </footer>
                <!-- .comment-meta -->
                
                <div class="comment-content">
                  <p>Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. At vero eos et accusam et justo duo dolores et ea rebum. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan.</p>
                </div>
                <!-- .comment-content -->
                
              </article>
              <!-- .comment-body --> 
            </li>
            <!-- #comment-## -->
          </ol>
          <!-- .comment-list -->
          
          <div class="comment-respond" id="respond">
            <h3 class="comment-reply-title" id="reply-title">Leave a Comment</h3>
            <form class="comment-form" id="commentform" method="post" action="/">
              <p class="comment-form-author">
                <label for="author">Name <span class="required">*</span></label>
                <input type="text" aria-required="true" size="30" value="" name="author" id="author" class="form-control" placeholder="Name">
              </p>
              <p class="comment-form-email">
                <label for="email">Email <span class="required">*</span></label>
                <input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Enter email">
              </p>
              <p class="comment-form-comment">
                <label for="comment">Comment</label>
                <textarea aria-required="true" rows="8" cols="45" name="comment" id="comment" class="form-control" placeholder="Comment"></textarea>
              </p>
              <p class="form-submit">
                <input type="submit" value="Leave a Comment" id="submit" name="submit" class="theme_btn">
              </p>
            </form>
          </div>
          <!-- #respond --> 
          
        </div>
        <!-- #comments --> 
        
      </div>
      <!-- #content --> 
    </div>

  </div>



  <aside class="col-sm-3">

    <div class="block widget_categories">
      <h3>Post Categories</h3>
      <ul>
        <li class="cat-item"><a href="#">Graphic Design</a> (3)</li>
        <li class="cat-item"><a href="#">Icon Desgin</a> (7)</li>
        <li class="cat-item"><a href="#">Interactive Media</a> (1)</li>
        <li class="cat-item"><a href="#">Photography</a> (2)</li>
        <li class="cat-item"><a href="#">Web Design</a> (4)</li>
      </ul>
    </div>


    <div class="block widget_archive">
        <h3>Blog Archives</h3>
        <ul>
          <li><a href="#">July 2013</a></li>
          <li><a href="#">June 2013</a></li>
          <li><a href="#">May 2013</a></li>
          <li><a href="#">April 2013</a></li>
          <li><a href="#">March 2013</a></li>
        </ul>
      </div>



    <div class="block widget_tag_cloud">
      <h3>Tags</h3>
      <div class="tagcloud">
        <a href="#" class="tag-link">Graphic Design</a>
        <a href="#" class="tag-link">Photography</a>
        <a href="#" class="tag-link">Design</a>
        <a href="#" class="tag-link">Media</a>
        <a href="#" class="tag-link">Typography</a>
      </div>
    </div>
    
  </aside>


</div></div></section>