<!DOCTYPE html>
<html><head>
  
<title>Rekomendasi</title>
  
	<style>
	  
		table{
		      
		border-collapse: collapse;
		      
		/*width: 100%;*/
		      
		margin: 0 auto;
		  
		}
		  
		table th{
		      
		/*border:1px solid #000;*/
		      
		padding: 3px;
		      
		font-weight: bold;
		      
		text-align: center;
		  
		}
		  
		table td{
		      
		/*border:1px solid #000;*/
		      
		padding: 3px;
		      
		vertical-align: top;
		  
		}
	  
	</style>
</head><body>
<!-- kop -->
<table >
    <tr>
        <td style="border:0px solid #000;width: 50px;">
        <table>    
            <tr>           
                <td style="border:0px solid #000;width: 90px;"><img src="<?php echo base_url() ?>assets/images/logo.png" class="img-responsive" alt="" height="100" width="97"></td>
            </tr>

        </table>
        </td>

        <td>
        <table>    
            <tr>    
                <td style="border:0px solid #000;width: 700px;font-weight: bold;">
                	<center>
                		<br>
		                BADAN PENELITIAN DAN PENGEMBANGAN PERTANAMAN <br>               	
		                BALAI PENGKAJIAN TEKNOLOGI PERTANIAN SULAWESI SELATAN <br>
		                Jl. Perintis Kemerdekaan KM. 17,5 Makassar 90243 <br>
		                Telepon (0411)556449; Faksimili (0411)55452 <br>
		                Website : www.sulsellitbangpertanian.go.id, Email : bptp.sulsel@litbang.pertanian.go.id 
	                </center>
                </td>
            </tr>       
        </table>
        </td>

        <td>
        <table>    
            <tr>           
                <td style="border:0px solid #000;width: 90px;"><img src="<?php echo base_url() ?>assets/images/agro.jpg" class="img-responsive" alt="" height="100" width="97"></td>
            </tr>
        </table>
        </td>
    </tr>
</table>
<!-- <H3 style="text-align: center; font-weight: bold;">BADAN PENELITIAN DAN PENGEMBANGAN PERTANAMAN</H3> -->
<!-- <H3 style="text-align: center; font-weight: bold;">BALAI PENGKAJIAN TEKNOLOGI PERTANIAN SULAWESI SELATAN</H3> -->
<hr>
<!-- N Kop -->
<H3 style="text-align: center; font-weight: bold;">HASIL ANALISIS REKOMENDASI TAKARAN PUPUK ANORGANIK</H3>
<h5 style="text-align: center; font-weight: bold;color:red;">Target Hasil Standar 5 ton GKG/ha</h5>

<table>    
	<tr>
		<td>Tanggal</td>
        <td style="width:1px;">:</td>
		<td><?php echo $x=date('d-m-Y', strtotime($hasil->tgl_uji)); ?></td>
	</tr>
	<tr>
		<td>Nama Sample</td>
        <td style="width:1px;">:</td>
		<td><?php echo $hasil->nama_sample; ?></td>        
	</tr>  
	<tr>
		<td>Lokasi Sample</td>
        <td style="width:1px;">:</td>
		<td><?php echo 'Kec. '.$hasil->kec.' Kab.'.$hasil->kab_kota; ?></td>        
	</tr>      
	
</table>

<br>
<table>
	<tr>
		<td style="border:1px solid #000;width:370px;"><center>STATUS HARA DAN TEKSTUR TANAH</center></td>
	</tr>
</table>

<table>
	<tr>
		<td style="border:1px solid #000;width:600px;">
			<table>
				<tr>
					<td>
						<table>
							<td>
								<tr>
									<td>Status Nitrogen (N)</td>
							        <td style="width:1px;">:</td>
									<td><?php echo $hasil->nitrogen2; ?></td>
									<td style="width:20px;"></td>
								</tr>

								<tr>
									<td>Status Posfor (P)</td>
							        <td style="width:1px;">:</td>
									<td><?php echo $hasil->posfor2; ?></td>
									<td style="width:20px;"></td>
								</tr>   

								
							</td>
						</table>
					</td>
					<td>
						<table>
							<td>
								<tr>
									<td>Status Kalium (K)</td>
							        <td style="width:1px;">:</td>
									<td><?php echo $hasil->kalium2; ?></td>
								</tr>
								<tr>
									<td>Tekstur Tanah</td>
							        <td style="width:1px;">:</td>
									<td><?php echo $hasil->tekstur2; ?></td>
								</tr>   
								
							</td>
						</table>
					</td>
				</tr>
				
				
			</table>
		</td>
	</tr>
</table>

<br><br>
<table>
	<tr>
		<td style="border:1px solid #000;width:370px;">
			<center>REKOMENDASI TAKARAN PUPUK TUNGGAL
					UNTUK SATUAN GKG/Ha
			</center>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td style="border:1px solid #000;width:600px;">
			Hasil Rekomendasi Menggunakan Metode ELECTRE :	
			<ol>
				<?php foreach ($poin_electre as $r) { ?>
					<li><?php echo $r->formula; ?></li>
				<?php } ?>
			</ol>
	
			Hasil Rekomendasi Menggunakan Metode TOPSIS	:
			<ol>
				<?php foreach ($poin_topsis as $r) { ?>
					<li><?php echo $r->formula; ?></li>
				<?php } ?>
			</ol>
		</td>
	</tr>
</table><br><br>
<H5 style="text-align: center; font-weight: thin;">Berdasarkan Hasil Analisis diatas maka takaran kombinasi pupuk tunggal yang direkomendasikan untuk standard 5 ton/ha diperoleh sesuai hasil perangkingan kedua Metode Electre dan Topsis<br>  untuk pilihan rekomendasi terbaik yakni <?php echo $rank_one->kd_formula; ?> - <?php echo $rank_one->formula; ?>.</H5>
<br><br><br><br><br><br><br>
</body></html>