<section id="contact" class="color_section">
  <div class="container">
    
    <div class="row">
      <div class="col-sm-12">
        <center>
        <h2 class="block-header">Hasil Proses Rekomendasi</h2>
        </center>
      </div>
    </div>
    <div class="row">
                  <div class="col-sm-12">
                      <br>
                      <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Tgl. Uji</th>
                            <th>Nama Sample</th>
                            <th>Kec / Kab</th>
                            <th>Nitrogen</th>
                            <th>Posfor</th>
                            <th>Kalium</th>
                            <th>Tekstur</th>
                            <th>Aksi</th>
                        </tr>
                        <?php $no=1; foreach ($hasil as $r) { ?>
                        <tr>                
                            <td><?php echo $no; ?></td>
                            <td><?php echo $x=date('d-m-Y', strtotime($r->tgl_uji)); ?></td>
                            <td><?php echo $r->nama_sample; ?></td>
                            <td><?php echo 'Kec. '.$r->kec.' Kab.'.$r->kab_kota; ?></td>
                            <td><?php echo $r->nitrogen2; ?></td>
                            <td><?php echo $r->posfor2; ?></td>
                            <td><?php echo $r->kalium2; ?></td>
                            <td><?php echo $r->tekstur2; ?></td>
                            <td><a href="<?php echo base_url(); ?>rekomendasi/cetak/<?php echo $r->id?>"><button class="btn btn-default">Cetak</button></a></td>
                            
                        </tr>
                        <?php $no++; } ?>
                    </table>
                  </div>
                </div>
  </div>
</section>