  <br><br><br>
  <div class="container">
    <h3>Data Penanggulangan</h3>
    <button class="btn btn-success" onclick="add()"><i class="glyphicon glyphicon-plus"></i> Tambah Data</button>
    <br> <br>
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
								      <thead>
								        <tr>
										  <th>No</th>
										  <th>Nama Penyakit</th>
										  <th>Penanggulangan</th>                
								          <th style="width:125px;">Aksi
								          </p></th>
								          <!-- ,dosis,aturan_pakai,ket -->
								        </tr>
								      </thead>
								      <tbody>
										<?php $nomor=1; foreach($list as $list){?>
											<tr>												
												<td><?php echo $nomor;?></td>												
												<td><?php echo $list->nama_penyakit;?></td>
												<td><?php echo $list->penanggulangan;?></td>								
												<td>
                          							<button class="btn btn-success" onclick="edit_data(<?php echo $list->id;?>)"><i class="glyphicon glyphicon-pencil"></i></button>													
													<button class="btn btn-danger" onclick="delete_data(<?php echo $list->id;?>)"><i class="glyphicon glyphicon-remove"></i></button>
                          
												</td>												
											</tr>	
										<?php $nomor++;}?>					 
								      </tbody>
								 
								      <tfoot>
								        
								      </tfoot>
								    </table>
								 
								  </div>

</div>
        <!-- END wrapper -->


    
        <script>
            var resizefunc = [];
        </script>

        <script src="<?php echo base_url(); ?>assets/jquery/jquery-3.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/datatables/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/datatables/js/dataTables.bootstrap.js"></script>

                 <!-- //// modal -->
        <script type="text/javascript">
        
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;
 
     
    function add()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#con-close-modal').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }

    function edit_data(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('admin/penanggulangan_get/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
        	$('[name="id"]').val(data.id);
            $('[name="kdPenyakit"]').val(data.kdPenyakit);
            $('[name="penanggulangan"]').val(data.penanggulangan);

            $('#con-close-modal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Ubah Data Penyakit'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
  
    function save()
    {
      var url;
        // <!-- ,dosis,aturan_pakai,ket -->
        var kd_Penyakit1 = document.frmOnline.txtKdPenyakit; 

        var penanggulangan1 = document.frmOnline.txtpenanggulangan;


        if (kd_Penyakit1.value == "") {
            alert("Kode Penyakit Tidak Boleh Kosong");
            txtKdPenyakit.focus();
            return false;
        }
        if (penanggulangan1.value == "") {
            alert("Penanggulangan Tidak Boleh Kosong");
            txtpenanggulangan.focus();
            return false;
        }
      /////
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('admin/penanggulangan_add')?>";
      }
      else
      {
        url = "<?php echo site_url('admin/penanggulangan_update')?>";
      }
 
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               alert('Data berhasil disimpan');
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
              
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_data(id)
    {
      if(confirm('Anda Yakin ingin menghapus data ini ?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('admin/penanggulangan_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

      }
    }
 
  </script>

  <!-- modal -->
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 class="modal-title">Masukkan Data Penanggulangan</h4> 
            </div> 
          <form class="form-appointment ui-form" action="#" id="form" onsubmit="return Validation()" name="frmOnline">
          <div class="row">
            <div class="col-lg-10 col-md-offset-1">    
              <br>          
              <input type="hidden" value="" name="id"/>
              <div class="form-group ">
                   <!-- ,dosis,aturan_pakai,ket -->
                  <div class="col-xs-12">
                    <select class="form-control" name="kdPenyakit" id="txtKdPenyakit">
                            <option value="Pilih Penyakit">Pilih Penyakit</option>
                            <?php foreach($penyakit_all as $penyakit_all) {?>
                              <option value="<?php echo $penyakit_all->kd_penyakit;?>"><?php echo $penyakit_all->nama_penyakit;?></option>
                            <?php } ?>
                        </select>  
                    
                	</div>
              </div>  
              <br> <br>                  
              <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" name="penanggulangan" placeholder="Penanggulangan" id="txtpenanggulangan">
                	</div>
              </div>       
              <br> <br>                      
            </div>
          </div><!-- end row -->
            <div class="modal-footer">
              <center>
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
              </center>
          </div>
        </div> 
    </div>
</div>

<!-- end modal -->        

    </body>
</html>