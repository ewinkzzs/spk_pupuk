  <br><br><br>
  <div class="container">
    <h3>Data Penyakit</h3>
    <button class="btn btn-success" onclick="add()"><i class="glyphicon glyphicon-plus"></i> Tambah Data</button>
    <br> <br>
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
								      <thead>
								        <tr>
										  <th>No</th>
										  <th>Kode Penyakit</th>
                      <th>Nama Penyakit</th>                
										  <th>Keterangan</th>                
								          <th style="width:125px;">Aksi
								          </p></th>
								          <!-- ,dosis,aturan_pakai,ket -->
								        </tr>
								      </thead>
								      <tbody>
										<?php $nomor=1; foreach($tb_penyakit as $tb_penyakit){?>
											<tr>												
												<td><?php echo $nomor;?></td>												
												<td><?php echo $tb_penyakit->kd_penyakit;?></td>
                        <td><?php echo $tb_penyakit->nama_penyakit;?></td>                
												<td><?php echo $tb_penyakit->ket;?></td>								
												<td>
                          							<button class="btn btn-success" onclick="edit_penyakit(<?php echo $tb_penyakit->id_penyakit;?>)"><i class="glyphicon glyphicon-pencil"></i></button>													
													<button class="btn btn-danger" onclick="delete_penyakit(<?php echo $tb_penyakit->id_penyakit;?>)"><i class="glyphicon glyphicon-remove"></i></button>
                          
												</td>												
											</tr>	
										<?php $nomor++;}?>					 
								      </tbody>
								 
								      <tfoot>
								        
								      </tfoot>
								    </table>
								 
								  </div>

</div>
        <!-- END wrapper -->


    
        <script>
            var resizefunc = [];
        </script>

        <script src="<?php echo base_url(); ?>assets/jquery/jquery-3.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/datatables/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/datatables/js/dataTables.bootstrap.js"></script>

                 <!-- //// modal -->
        <script type="text/javascript">
        
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;
 
     
    function add()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#con-close-modal').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }

    function edit_penyakit(id_penyakit)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('admin/penyakit_edit/')?>/" + id_penyakit,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
        	  $('[name="id_penyakit"]').val(data.id_penyakit);
            $('[name="kd_penyakit"]').val(data.kd_penyakit);
            $('[name="nama_penyakit"]').val(data.nama_penyakit);
            $('[name="ket"]').val(data.ket);

            $('#con-close-modal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Ubah Data Penyakit'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
  
    function save()
    {
      var url;
        // <!-- ,dosis,aturan_pakai,ket -->
        var kd_penyakit1 = document.frmOnline.txtKdPenyakit; 
        var nama_penyakit1 = document.frmOnline.txtNmPenyakit;
        var ket1 = document.frmOnline.txtket;

        if (kd_penyakit1.value == "") {
            alert("Kode Penyakit Tidak Boleh Kosong");
            txtKdPenyakit.focus();
            return false;
        }
        if (nama_penyakit1.value == "") {
            alert("Nama Penyakit Tidak Boleh Kosong");
            txtNmPenyakit.focus();
            return false;
        }
        if (ket1.value == "") {
            alert("Keterangan Tidak Boleh Kosong");
            txtket.focus();
            return false;
        }
      /////
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('admin/penyakit_add')?>";
      }
      else
      {
        url = "<?php echo site_url('admin/penyakit_update')?>";
      }
 
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               alert('Data berhasil disimpan');
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
              
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_penyakit(id_penyakit)
    {
      if(confirm('Anda Yakin ingin menghapus data ini ?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('admin/penyakit_delete')?>/"+id_penyakit,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

      }
    }
 
  </script>

  <!-- modal -->
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 class="modal-title">Masukkan Data Obat</h4> 
            </div> 
          <form class="form-appointment ui-form" action="#" id="form" onsubmit="return Validation()" name="frmOnline">
          <div class="row">
            <div class="col-lg-10 col-md-offset-1">    
              <br>          
              <input type="hidden" value="" name="id_penyakit"/>
              <div class="form-group ">
                   <!-- ,dosis,aturan_pakai,ket -->
                  <div class="col-xs-12">
                    <input class="form-control" type="text" name="kd_penyakit" placeholder="Kode Penyakit" id="txtKdPenyakit">     
                	</div>
              </div>  
              <br> <br>                  
              <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" name="nama_penyakit" placeholder="Nama Penyakit" id="txtNmPenyakit">
                	</div>
              </div>       
              <br> <br>   
              <div class="form-group ">
                  <div class="col-xs-12">
                    <input class="form-control" type="text" name="ket" placeholder="Keterangan" id="txtket">
                  </div>
              </div>  
              <br><br>                   
            </div>
          </div><!-- end row -->
            <div class="modal-footer">
              <center>
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
              </center>
          </div>
        </div> 
    </div>
</div>

<!-- end modal -->        

    </body>
</html>