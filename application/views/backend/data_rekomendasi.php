  <br><br><br>
  <div class="container">
    <h3>Data Hasil Rekomendasi</h3>
    <!-- <button class="btn btn-success" onclick="add()"><i class="glyphicon glyphicon-plus"></i> Tambah Data</button> -->
    <br> <br>
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
								      <thead>
								        <tr>
      										  <th style="width:20px;">No</th>
                            <th>Nama</th>
      										  <th>Tgl. Uji</th>
                            <th>Nama Sample</th>
                            <th>Kec / Kab</th>
                            <th>Nitrogen</th>
                            <th>Posfor</th>
                            <th>Kalium</th>
                            <th>Tekstur</th>
      								      <th style="width:65px;">Aksi</th>
      								          <!-- ,`,dest,path,url -->
								        </tr>
								      </thead>
								      <tbody>
										<?php $nomor=1; foreach($list as $r){?>
											<tr>												
												    <td><?php echo $nomor;?></td>												
                            <td><?php echo $r->nama; ?></td>
                            <td><?php echo $x=date('d-m-Y', strtotime($r->tgl_uji)); ?></td>
                            <td><?php echo $r->nama_sample; ?></td>
                            <td><?php echo 'Kec. '.$r->kec.' Kab.'.$r->kab_kota; ?></td>
                            <td><?php echo $r->nitrogen2; ?></td>
                            <td><?php echo $r->posfor2; ?></td>
                            <td><?php echo $r->kalium2; ?></td>
                            <td><?php echo $r->tekstur2; ?></td>
                            <td><a href="<?php echo base_url(); ?>rekomendasi/cetak/<?php echo $r->id?>"><button class="btn btn-default">Cetak</button></a></td>												
											</tr>	
										<?php $nomor++;}?>					 
								      </tbody>
								 
								      <tfoot>
								        
								      </tfoot>
								    </table>
								 
								  </div>

</div>
        <!-- END wrapper -->


    
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url(); ?>assets/jquery/jquery-3.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/datatables/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/datatables/js/dataTables.bootstrap.js"></script>

                 <!-- //// modal -->
        <script type="text/javascript">
        
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;
 
     
    function add()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#con-close-modal').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }

    function edit_(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('admin/lokasi_sample_get/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
        	$('[name="id"]').val(data.id);
            $('[name="nama_sample"]').val(data.nama_sample);
            $('[name="kab_kota"]').val(data.kab_kota);
            $('[name="kec"]').val(data.kec);

            $('#con-close-modal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Ubah Data Lokasi Sampel'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
  
    function save()
    {
      var url;
        // <!-- ,dosis,aturan_pakai,ket -->
        // var nama_sample = document.frmOnline.nama_sample; 
        var kab_kota = document.frmOnline.kab_kota;
        var kec = document.frmOnline.kec;


        // if (nama_sample.value == "") {
        //     alert("Data Belum Lengkap");
        //     nama_sample.focus();
        //     return false;
        // }
        if (kab_kota.value == "") {
            alert("Data Belum Lengkap");
            kab_kota.focus();
            return false;
        }
        if (kec.value == "") {
            alert("Data Belum Lengkap");
            kec.focus();
            return false;
        }
      /////
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('admin/lokasi_sample_add')?>";
      }
      else
      {
        url = "<?php echo site_url('admin/lokasi_sample_update')?>";
      }
 
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               alert('Data berhasil disimpan');
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
              
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_(id)
    {
      if(confirm('Anda Yakin ingin menghapus data ini ?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('admin/lokasi_sample_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

      }
    }
 
  </script>

  <!-- modal -->
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 class="modal-title">Masukkan Data Lokasi Sampel</h4> 
            </div> 
          <form class="form-appointment ui-form" action="#" id="form" onsubmit="return Validation()" name="frmOnline">
          <div class="row">
            <div class="col-lg-10 col-md-offset-1">    
              <br>          
              <input type="hidden" value="" name="id"/>
              <!-- <div class="form-group ">
                  <div class="col-xs-12">
                    <input type="text" class="form-control" name="nama_sample" id="nama_sample" placeholder="Nama Sample"/>
                	</div>
              </div>  
              <br> <br> -->                  
              <div class="form-group ">
                  <div class="col-xs-12">
                    <input type="text" class="form-control" name="kab_kota" id="kab_kota" placeholder="Kab Kota" />
                	</div>
              </div>       
              <br> <br>  
              <div class="form-group ">
                  <div class="col-xs-12">
                    <input type="text" class="form-control" name="kec" id="kec" placeholder="Kec"/>
                  </div>
              </div>       
              <br> <br>                    
            </div>
          </div><!-- end row -->
            <div class="modal-footer">
          <center>
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
            </center>
          </div>
        </div> 
    </div>
</div>

<!-- end modal -->        

    </body>
</html>