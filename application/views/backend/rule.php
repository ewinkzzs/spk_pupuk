  <br><br><br>
  <div class="container">
    <h3>Data Aturan</h3>
    <button class="btn btn-success" onclick="add()"><i class="glyphicon glyphicon-plus"></i> Tambah Data</button>
    <br />
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
                     
                      <tr>
                      <td widtd="5%">No</td>
                      <td widtd="30%">Kode dan Nama Penyakit</td>
                      <td></td>   
                      <td></td>                                 
                          <!-- ,dosis,aturan_pakai,ket -->
                        </tr>
                   
                      <tbody>
                    <?php $nomor=1; foreach($penyakit as $penyakit){?>
                      <tr>                        
                        <td><?php echo $nomor;?></td>                       
                        <td><?php echo $penyakit->kd_penyakit;?> - <?php echo $penyakit->penyakit;?></td>
                                    <td></td>
                                    <td></td>
                                    </tr>
                                    
                          <!-- <table id="datatable" width='100%' border='0' cellpadding='4' cellspacing='1' bordercolor='#F0F0F0' bgcolor='#DBEAF5'> -->
                                          <tr>
                                            <td></td>
                                            <td class="text-primary" widtd="30%">Nama Gejala :</td>
                                            <td class="text-primary" widtd="70%">Bobot</td>       
                                            <td class="text-primary" widtd="30%">Aksi</td>
                                          </tr>
                                          <?php 
                                         
                                      $gejala = $this->Rule_model->get_gejala("$penyakit->kd_penyakit");
                                          foreach($gejala as $gejala) { ?> 
                                          <tr> 
                                            <td></td>
                                            <td class="text-primary"><?php echo $gejala->namagejala;?></td>
                                            <td class="text-primary"><?php echo $gejala->bobot;?></td>            
                                            <td>                       
                                            <button class="btn btn-danger"  onclick="delete_rule(<?php echo $gejala->id_rule;?>)"><i class="glyphicon glyphicon-remove"></i></button>
                                          </td>
                                          </tr> 
                                          <?php } ?>
                                       <!-- </table> -->
                                    
                    <?php $nomor++;}?>           
                      </tbody>
                    
                    </table>
 
  </div>
 
    <script src="<?php echo base_url(); ?>assets/jquery/jquery-3.1.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/datatables/js/dataTables.bootstrap.js"></script>
 
 
  <script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;
 
     
    function add()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
 
    function save()
    {
      var url;
        var txtkdpenyakit1 = document.frmOnline.txtkdpenyakit;
        var txtkdgejala1 = document.frmOnline.txtkdgejala; 
        var txtbobot1 = document.frmOnline.txtbobot;

        if (txtkdgejala1.value == "Pilih Gejala") {
            alert("Pilih Gejala");
            txtkdgejala.focus();
            return false;
        }
        if (txtbobot1.value == "Pilih Bobot") {
            alert("Pilih Bobot");
            txtbobot.focus();
            return false;
        }
        if (txtkdpenyakit1.value == "Pilih Penyakit") {
            alert("Pilih Penyakit");
            txtkdpenyakit.focus();
            return false;
        }

      if(save_method == 'add')
      {
          url = "<?php echo site_url('admin/rule_add')?>";
      }
      else
      {
        url = "<?php echo site_url('admin/rule_update')?>";
      }
 
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               alert('Data berhasil disimpan');
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
              
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_rule(id)
    {
      if(confirm('Anda Yakin ingin menghapus data ini ?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('admin/rule_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

      }
    }
 
  </script>
 
  </script>
 
  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header col-lg-10 col-md-offset-1">
        <h3 class="modal-title">Masukkan data Aturan</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal" name="frmOnline" onsubmit="return Validation()" name="frmOnline">
        <div class="row">
            <div class="col-lg-10 col-md-offset-1">    
              
              <input type="hidden" value="" name="id"/>      
              <div class="form-group ">
              <br>
              
                   <label class="control-label col-md-3">Pilih Penyakit</label>
               
                  <div class="col-md-9">
                    <select class="form-control" name="kd_penyakit" id="txtkdpenyakit">
                            <option value="Pilih Penyakit">Pilih Penyakit</option>
                            <?php foreach($penyakit_all as $penyakit_all) {?>
                              <option value="<?php echo $penyakit_all->kd_penyakit;?>"><?php echo $penyakit_all->nama_penyakit;?></option>
                            <?php } ?>
                        </select>  
                  </div>
              </div> 
              <div class="form-group ">
                   <label class="control-label col-md-3">Pilih Gejala</label>
                  <div class="col-md-9">

                      <select class="form-control" name="kd_gejala" id="txtkdgejala">
                            <option value="Pilih Gejala">Pilih Gejala</option>
                            <?php foreach($gejala_all as $gejala_all) {?>
                              <option value="<?php echo $gejala_all->id_gejala;?>"><?php echo $gejala_all->gejala;?></option>
                            <?php } ?>
                        </select>  
                       
                  </div> 
              </div>  
                               
              <div class="form-group ">
                   <label class="control-label col-md-3">Masukkan Bobot</label>

                    <!-- <div class="col-md-9">
                      <select class="form-control" type="text" name="bobot" id="txtbobot">
                            <option value="Pilih Bobot">Pilih Bobot</option>
                            <option value="6">6 | Gejala Parah</option>
                            <option value="4">4 | Gejala Dominan</option>
                            <option value="2">2 | Gejala Biasa</option>
                        </select>
                  </div> -->
                  <div class="col-md-9">
                    <input class="form-control" type="text" name="bobot" placeholder="Bobot" id="txtbobot">
                  </div>
              </div>       
                          

                      
            </div>
       
 
          </div>
        </form>
          </div>
          <div class="modal-footer">
          <center>
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
            </center>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->

  </body>
</html>