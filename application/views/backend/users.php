  <br><br><br>
  <div class="container">
    <h3>Data Pengguna</h3>
    
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
								      <thead>
								        <tr>
										  <th>No</th>
                      <th>Nama</th>
                      <th>Jenis Kelamin</th>
                      <th>No. Telepon</th>
										  <th>Pekerjaan</th>
										  <th>Email</th>
										  <th>Username</th>										  
								          <th style="width:125px;">Aksi
								          </th>
            
								          <!-- //nik,nama,jk,no_telp,pekerjaan,email,username,`password` -->
								        </tr>
								      </thead>
								      <tbody>
										<?php $nomor=1; foreach($users as $users){?>
											<tr>												
												<td><?php echo $nomor;?></td>
												<!-- <td><?php echo $users->id_user;?></td> -->
                        <td><?php echo $users->nama;?></td>
                        <td><?php echo $users->jk;?></td>
                        <td><?php echo $users->no_telp;?></td>
												<td><?php echo $users->pekerjaan;?></td>
												<td><?php echo $users->email;?></td>
												<td><?php echo $users->username;?></td>												
												<td>													
													<button class="btn btn-danger" onclick="delete_users(<?php echo $users->id;?>)"><i class="glyphicon glyphicon-remove"></i></button>
												</td>												
											</tr>	
										<?php $nomor++;}?>					 
								      </tbody>
								 
								      <tfoot>
								        
								      </tfoot>
								    </table>
								 
								  </div>

</div>
        <!-- END wrapper -->

        <script src="<?php echo base_url(); ?>assets/jquery/jquery-3.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/datatables/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/datatables/js/dataTables.bootstrap.js"></script>

                 <!-- //// modal -->
        <script type="text/javascript">
        
  $(document).ready( function () {
      $('#table_id').DataTable();
  } ); 

    function delete_users(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('admin/users_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

      }
    }
 
  </script>       

    </body>
</html>