 <!DOCTYPE html>
<html>
    <head> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Administrator</title>
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/images/logo.png">
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
    <!-- batas -->
    
    
  </head>
  <body>
<!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <img style="width: 190px; height: 47px;" src="<?php echo base_url(); ?>assets/example/logo.png" >
          &nbsp;&nbsp;&nbsp;
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="<?php if($this->uri->segment(2)==""){echo "active";}?>"><a href="<?php echo base_url(); ?>admin">  Dashboard</a></li>
            <li class="<?php if($this->uri->segment(2)=="lokasi_sample"){echo "active";}?>"><a href="<?php echo base_url(); ?>admin/lokasi_sample">  Lokasi Sample</a></li>          
            <li class="<?php if($this->uri->segment(2)=="rekomtakaran"){echo "active";}?>"><a href="<?php echo base_url(); ?>admin/rekomtakaran">  Takaran</a></li>          
            <li class="<?php if($this->uri->segment(2)=="data_rekomendasi"){echo "active";}?>"><a href="<?php echo base_url(); ?>admin/data_rekomendasi">  Hasil Rekomendasi</a></li>          
            <li class="<?php if($this->uri->segment(2)=="informasi"){echo "active";}?>"><a href="<?php echo base_url(); ?>admin/informasi">  Informasi</a></li>          
            <li class="<?php if($this->uri->segment(2)=="users"){echo "active";}?>"><a href="<?php echo base_url(); ?>admin/users">Pengguna</a></li>
            <!-- <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li> -->
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <!-- <li><a href="../navbar/">Default</a></li> -->
            <li><a href="<?php echo base_url(); ?>login/logout">Keluar</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>