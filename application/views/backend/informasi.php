  <br><br><br>
  <div class="container">
    <h3>Data Informasi</h3>
    <button class="btn btn-success" onclick="add()"><i class="glyphicon glyphicon-plus"></i> Tambah Data</button>
    <br> <br>
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
								      <thead>
								        <tr>
      										  <th>No</th>
      										  <th>Judul Informasi</th>
                            <th>Deskripsi</th>                
                            <th>url</th>                
      										  <th>Foto</th>                
      								      <th style="width:10px;">Aksi</th>
      								          <!-- ,`,dest,path,url -->
								        </tr>
								      </thead>
								      <tbody>
										<?php $nomor=1; foreach($list as $row){?>
											<tr>												
												<td><?php echo $nomor;?></td>												
                        <td><?php echo $row->name;?></td>
												<td><?php echo $row->dest;?></td>
                        <td><?php echo $row->url;?></td>                
												<td>
                          <a href="<?php echo base_url(); ?>upload/<?php echo $row->path?>" target="_blank"><img src="<?php echo base_url(); ?>upload/<?php echo $row->path?>" class="img-responsive" /></a>  
                        </td>								
												<td>
                          	<button class="btn btn-info" onclick="edit(<?php echo $row->id;?>)"><i class="glyphicon glyphicon-pencil"></i></button>
                            <br>													
													  <button class="btn btn-danger" onclick="delete_(<?php echo $row->id;?>)"><i class="glyphicon glyphicon-remove"></i></button>
                          
												</td>												
											</tr>	
										<?php $nomor++;}?>					 
								      </tbody>
								 
								      <tfoot>
								        
								      </tfoot>
								    </table>
								 
								  </div>

</div>
        <!-- END wrapper -->


    
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url(); ?>assets/jquery/jquery-3.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/datatables/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/datatables/js/dataTables.bootstrap.js"></script>

                 <!-- //// modal -->
        <script type="text/javascript">
        
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;
 
     
function add()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#photo-preview').hide();
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }

  function edit(id)
  {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string


      //Ajax Load data from ajax
      $.ajax({
          url : "<?php echo site_url('admin/informasi_get')?>/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {

              $('[name="id"]').val(data.id);
              $('[name="name"]').val(data.name);
              $('[name="dest"]').val(data.dest);
              $('[name="url"]').val(data.url);
              // $('[name="url"]').val(data.url);
              //$('[name="image"]').val(data.image['file_name']);
              $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
              $('.modal-title').text('Edit galeri'); // Set title to Bootstrap modal title
              $('#photo-preview').show();
              if(data.path)
              {
                  $('#label-photo').text('Change image'); // label photo upload
                  $('#photo-preview div').html('<img src="<?php echo base_url(); ?>upload/'+data.path+'" class="img-responsive">'); // show photo

              }
              else
              {
                  $('#label-photo').text('Upload Photo'); // label photo upload
                  $('#photo-preview div').text('(No photo)');
              }

              
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
  }

    function delete_(id)
    {
      if(confirm('Anda yakin inign menghapus data ini ?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('admin/informasi_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
 
      }
    }

    function save()
  {
      var url;

      if(save_method == 'add') {
          url = "<?php echo site_url('admin/informasi_add')?>";
      } else {
          url = "<?php echo site_url('admin/informasi_update')?>";
      }

      // ajax adding data to database

      var formData = new FormData($('#form')[0]);
      $.ajax({
          url : url,
          type: "POST",
          data: formData,
          contentType: false,
          processData: false,
          dataType: "JSON",
          success: function(data){
                      alert("Data berhasil disimpan");
                      location.reload();
        },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable 

          }
      });
  }
 
  </script>

  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Tambah data galeri</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">

        <!-- npm,nmPanggil,nmLengkap,jk,alamat,nmFb,angkatan,noTelp -->
          <input type="hidden" value="" name="id"/>
          <div class="form-body">

            <div class="form-group">
              <label class="control-label col-md-3">Judul Informasi</label>
              <div class="col-md-9">
                <input type="text" name="name" class="form-control" placeholder="Judul Informasi">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Deskripsi</label>
              <div class="col-md-9">
                <textarea class="form-control limited" rows="7" name="dest" id="dest"></textarea>
                <!-- <input type="text" name="caption" class="form-control" placeholder="Caption"> -->
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">URL</label>
              <div class="col-md-9">
                <input type="text" name="url" class="form-control" placeholder="URL">
              </div>
            </div>

            <div class="form-group" id="photo-preview">
              <label class="control-label col-md-3">Photo</label>
              <div class="col-md-9">
              (No photo)
                <span class="help-block"></span>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3">Image</label>
              <div class="col-md-9">
                <input type="file" name="path">
                <p>Maximal 1.9 MB</p>
              </div>
            </div>

            
 
          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->

  </body>
</html>