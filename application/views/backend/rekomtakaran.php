  <br><br><br>
  <div class="container">
    <h3>Data Takaran</h3>
    <button class="btn btn-success" onclick="add()"><i class="glyphicon glyphicon-plus"></i> Tambah Data</button>
    <br> <br>
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
								      <thead>
								        <tr>
      										  <th style="width:20px;">No</th>
      										  <th>Formula</th>
                            <th>Nitrogen</th>
                            <th>Posfor</th>
                            <th>Kalium</th>
                            <th>Tekstur</th>
                            <th>n Nitrogen</th>
                            <th>n Posfor</th>
                            <th>n Kalium</th>
                            <th>n Tekstur</th>        
      								      <th style="width:65px;">Aksi</th>
      								          <!-- ,`,dest,path,url -->
								        </tr>
								      </thead>
								      <tbody>
										<?php $nomor=1; foreach($list as $row){?>
											<tr>												
												<td><?php echo $nomor;?></td>												
                        <td><?php echo $row->formula ?></td>
                        <td><?php echo $row->nitrogen ?></td>
                        <td><?php echo $row->posfor ?></td>
                        <td><?php echo $row->kalium ?></td>
                        <td><?php echo $row->tekstur ?></td>
                        <td><?php echo $row->nitrogen2 ?></td>
                        <td><?php echo $row->posfor2 ?></td>
                        <td><?php echo $row->kalium2 ?></td>
                        <td><?php echo $row->tekstur2 ?></td>               																		
												<td>
                          	<button class="btn btn-info" onclick="edit_(<?php echo $row->id;?>)"><i class="glyphicon glyphicon-pencil"></i></button>
                            													
													  <button class="btn btn-danger" onclick="delete_(<?php echo $row->id;?>)"><i class="glyphicon glyphicon-remove"></i></button>
                          
												</td>												
											</tr>	
										<?php $nomor++;}?>					 
								      </tbody>
								 
								      <tfoot>
								        
								      </tfoot>
								    </table>
								 
								  </div>

</div>
        <!-- END wrapper -->


    
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url(); ?>assets/jquery/jquery-3.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/datatables/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/datatables/js/dataTables.bootstrap.js"></script>

                 <!-- //// modal -->
        <script type="text/javascript">
        
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;
 
     
    function add()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#con-close-modal').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }

    function edit_(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('admin/rekomtakaran_get/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
        	  $('[name="id"]').val(data.id);
            $('[name="formula"]').val(data.formula);
            $(".nitrogen option[value='" + data.nitrogen + "']").attr("selected","selected");
            $(".posfor option[value='" + data.posfor + "']").attr("selected","selected");
            $(".kalium option[value='" + data.kalium + "']").attr("selected","selected");
            $(".tekstur option[value='" + data.tekstur + "']").attr("selected","selected");

            $('#con-close-modal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Ubah Data Takaran'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
  
    function save()
    {
      var url;
        // <!-- ,dosis,aturan_pakai,ket -->
        var formula = document.frmOnline.formula; 
        var nitrogen = document.frmOnline.nitrogen;
        var posfor = document.frmOnline.posfor;
        var kalium = document.frmOnline.kalium;
        var tekstur = document.frmOnline.tekstur;


        if (formula.value == "") {
            alert("Data Belum Lengkap");
            formula.focus();
            return false;
        }
        if (nitrogen.value == "") {
            alert("Data Belum Lengkap");
            nitrogen.focus();
            return false;
        }
        if (posfor.value == "") {
            alert("Data Belum Lengkap");
            posfor.focus();
            return false;
        }
        if (kalium.value == "") {
            alert("Data Belum Lengkap");
            kalium.focus();
            return false;
        }
        if (tekstur.value == "") {
            alert("Data Belum Lengkap");
            tekstur.focus();
            return false;
        }
      /////
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('admin/rekomtakaran_add')?>";
      }
      else
      {
        url = "<?php echo site_url('admin/rekomtakaran_update')?>";
      }
 
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               alert('Data berhasil disimpan');
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
              
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_(id)
    {
      if(confirm('Anda Yakin ingin menghapus data ini ?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('admin/rekomtakaran_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

      }
    }
 
  </script>

  <!-- modal -->
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 class="modal-title">Masukkan Data Takaran</h4> 
            </div> 
          <form class="form-appointment ui-form" action="#" id="form" onsubmit="return Validation()" name="frmOnline">
          <div class="row">
            <div class="col-lg-10 col-md-offset-1">    
              <br>          
              <input type="hidden" value="" name="id"/>
              <div class="form-group ">
                  <div class="col-xs-12">
                    <input type="text" class="form-control" name="formula" id="formula" placeholder="Formula"/>                                    
                	</div>
              </div>  
              <br> <br>                  
              <div class="form-group ">
                  <div class="col-xs-12">
                    <select class="form-control nitrogen" name="nitrogen" id="nitrogen" placeholder="Nitrogen"/>
                        <option value="Sangat Sesuai">Sangat Sesuai</option>
                        <option value="Sesuai">Sesuai</option>
                        <option value="Cukup Sesuai">Cukup Sesuai</option>
                        <option value="Tidak Sesuai">Tidak Sesuai</option>
                        <option value="Sangat Tidak Sesuai">Sangat Tidak Sesuai</option>
                    </select>                       
                	</div>
              </div>       
              <br> <br>  
              <div class="form-group ">
                  <div class="col-xs-12">
                    <select class="form-control posfor" name="posfor" id="posfor" placeholder="Posfor"/>
                        <option value="Sangat Sesuai">Sangat Sesuai</option>
                        <option value="Sesuai">Sesuai</option>
                        <option value="Cukup Sesuai">Cukup Sesuai</option>
                        <option value="Tidak Sesuai">Tidak Sesuai</option>
                        <option value="Sangat Tidak Sesuai">Sangat Tidak Sesuai</option>
                    </select>                     
                  </div>
              </div>       
              <br> <br>    
              <div class="form-group ">
                  <div class="col-xs-12">            
                    <select class="form-control kalium" name="kalium" id="kalium" placeholder="Kalium"/>
                        <option value="Sangat Sesuai">Sangat Sesuai</option>
                        <option value="Sesuai">Sesuai</option>
                        <option value="Cukup Sesuai">Cukup Sesuai</option>
                        <option value="Tidak Sesuai">Tidak Sesuai</option>
                        <option value="Sangat Tidak Sesuai">Sangat Tidak Sesuai</option>
                    </select>                    
                  </div>
              </div>       
              <br> <br>  
              <div class="form-group ">
                  <div class="col-xs-12">            
                    <select class="form-control tekstur" name="tekstur" id="tekstur" placeholder="Tekstur"/>
                        <option value="Sangat Sesuai">Sangat Sesuai</option>
                        <option value="Sesuai">Sesuai</option>
                        <option value="Cukup Sesuai">Cukup Sesuai</option>
                        <option value="Tidak Sesuai">Tidak Sesuai</option>
                        <option value="Sangat Tidak Sesuai">Sangat Tidak Sesuai</option>
                    </select>                    
                  </div>
              </div>       
              <br> <br>                  
            </div>
          </div><!-- end row -->
            <div class="modal-footer">
          <center>
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
            </center>
          </div>
        </div> 
    </div>
</div>

<!-- end modal -->        

    </body>
</html>