<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hasil_rekomendasi extends CI_Controller {

	public function __construct() {

	 		parent::__construct();
            $this->simple_login->cek_login();
			$this->load->helper('url');
			$this->load->model('Rekomedasi_model');
	 	}

	public function index()
	{
		$data['hasil'] = $this->Rekomedasi_model->get_hasil($this->session->userdata('id'));
		$this->load->view('frontend/header');
		$this->load->view('frontend/hasil_rekomendasi',$data);
		$this->load->view('frontend/footer');
	}

	public function cetak($id)
	{
		$data['hasil'] = $this->Rekomedasi_model->get_hasil($this->session->userdata('id'));
		$this->load->view('frontend/header');
		$this->load->view('frontend/hasil_rekomendasi',$data);
		$this->load->view('frontend/footer');
	}

}
