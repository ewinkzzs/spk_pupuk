<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Informasi extends CI_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
			$this->load->model('M_informasi');
	 	}

	public function index()
	{
		$data['list'] = $this->M_informasi->get(array('active' => '1'))->result();
		$this->load->view('frontend/header');
		$this->load->view('frontend/informasi',$data);
		$this->load->view('frontend/footer');
	}

	public function read($url)
	{
		$data['list'] = $this->M_informasi->get(array('url' => $url))->result();
		$this->load->view('frontend/header');
		$this->load->view('frontend/informasi',$data);
		// $this->load->view('frontend/informasi_detail',$data);
		$this->load->view('frontend/footer');
	}
}
