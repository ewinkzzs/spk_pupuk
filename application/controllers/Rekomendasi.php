<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekomendasi extends CI_Controller {

	public function __construct() {

	 		parent::__construct();
            $this->simple_login->cek_login();
			$this->load->helper('url');
			$this->load->model('Rekomedasi_model');
			$this->load->library('form_validation');
            // $this->load->library('dompdf_gen');
	 	}

	public function index()
	{
		$data['lokasisample'] = $this->Rekomedasi_model->get_lokasisample()->result();
		$this->load->view('frontend/header');
		$this->load->view('frontend/rekomendasi',$data);
		$this->load->view('frontend/footer');
	}

	public function proses()
	{
		$this->form_validation->set_rules('nama_sample', 'Nama Sample', 'required');
		

		if ($this->form_validation->run() == TRUE) {
			$n=$this->input->post('nitrogen');
			if ($n=="< 0.20 (Rendah)") $arr_n=1;
			if ($n=="0.21-0.50 (Sedang)") $arr_n=3;
			if ($n=="> 0.50 (Tinggi)") $arr_n=5;
			
			$p=$this->input->post('posfor');
			if ($p=="< 20 (Rendah)") $arr_p=1;
			if ($p=="20-40 (Sedang)") $arr_p=3;
			if ($p=="> 40 (Tinggi)") $arr_p=5;

			$k=$this->input->post('kalium');
			if ($k=="< 10 (Rendah)") $arr_k=1;
			if ($k=="10-20 (Sedang)") $arr_k=3;
			if ($k=="> 20 (Tinggi)") $arr_k=5;

			$t=$this->input->post('tekstur');
			if ($t=="Liat") $arr_t=1;
			if ($t=="Sedang") $arr_t=3;
			if ($t=="Berpasir") $arr_t=5;


			$data['id_lokasisample']=$this->input->post('id_lokasisample',TRUE);
			$data['nama_sample']=$this->input->post('nama_sample',TRUE);
			$data['nitrogen']=$arr_n;
			$data['posfor']=$arr_p;
			$data['kalium']=$arr_k;
			$data['tekstur']=$arr_t;

			$dataujitanah = array(
	                        'nama_sample' => $this->input->post('nama_sample',TRUE),
	                        'id_lokasisample' => $this->input->post('id_lokasisample',TRUE),
	                        'id_user' => $this->session->userdata('id'),
	                        'tgl_uji' => date('Y-m-d'),
	                        'nitrogen' => $arr_n,
							'posfor' => $arr_p,
							'kalium' => $arr_k,
							'tekstur' => $arr_t,
	                        'nitrogen2' => $n,
	                        'posfor2' =>  $p,
	                        'kalium2' =>  $k,
	                        'tekstur2' =>  $t,
	                         );
	        $insert = $this->Rekomedasi_model->add_dataujitanah($dataujitanah);

			$data['ujitanah_DSC'] = $this->Rekomedasi_model->get_dataujitanah_DSC();
			$data['rekomtakaran'] = $this->Rekomedasi_model->get_rekomtakaran()->result();
			
			$this->load->view('frontend/header');
			$this->load->view('frontend/rekomendasi_proses',$data);
			$this->load->view('frontend/footer');
		} else {
			echo "<script type='text/javascript'>alert('error');</script>";
			$data['lokasisample'] = $this->Rekomedasi_model->get_lokasisample()->result();
			$this->load->view('frontend/header');
			$this->load->view('frontend/rekomendasi',$data);
			$this->load->view('frontend/footer');
		}
	}

	public function cetak($id)
	{
        $data['title'] ='Hasil Diagnosa';
        $data['poin_electre']=$this->Rekomedasi_model->get_poin($id);
        $data['poin_topsis']=$this->Rekomedasi_model->get_poin_topsis($id);
		$data['hasil'] = $this->Rekomedasi_model->get_cetak($id);
		$data['rank_one'] = $this->Rekomedasi_model->get_rank_one($id);
		$this->load->view('frontend/cetak_laporan',$data);
// $this->load->view('diagnosa_print_view',$data);
             
                    
            // $paper_size='A4';
            // //paper size
                    
            // $orientation='portrait';
            // //tipe format kertas
                    
            // $html=$this ->output->get_output();
             
                    
            // $this->dompdf->set_paper($paper_size,$orientation);
                    
            // //Convert to PDF
                    
            // $this->dompdf->load_html($html);            
            // $this->dompdf->render();   
            // $this->dompdf->stream("Hasil-diagnosa.pdf" ,
            //     array('Attachment'=>0));
	}

}
