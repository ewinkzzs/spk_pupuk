<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petunjuk extends CI_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 	}

	public function index()
	{
		$this->load->view('frontend/header');
		$this->load->view('frontend/petunjuk');
		$this->load->view('frontend/footer');
	}
}
