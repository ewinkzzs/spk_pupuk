<?php  
defined('BASEPATH') OR exit('No direct script access allowed');  

class Admin extends CI_Controller {  
        public function __construct() {
                parent::__construct();
                $this->simple_login->cek_login();
		$this->load->helper('url');
               
                $this->load->model('M_informasi');
                $this->load->model('Lokasisample_model');
                $this->load->model('Rekomtakaran_model');
                $this->load->model('Rekomedasi_model');
                $this->load->model('Rule_model');
                $this->load->model('User_model');
                
	}

        public function index() { 
                $this->load->view('backend/header');
                $this->load->view('backend/dashboard');  
                $this->load->view('backend/footer');  
        }

        public function users() {                  
                $data['users']=$this->User_model->get_all_users();
                $this->load->view('backend/header');                
		$this->load->view('backend/users',$data);				                
        }

        public function users_delete($id)
                {
                        $this->User_model->delete_by_id($id);
                        echo json_encode(array("status" => TRUE));
                }



        public function lokasi_sample() {                  
                $data['list']=$this->Lokasisample_model->get_all();
                $this->load->view('backend/header');                
                $this->load->view('backend/lokasi_sample',$data);                                         
        }

        public function lokasi_sample_add() 
                {                        
                        $data = array(                          
                                        'kab_kota' => $this->input->post('kab_kota',TRUE),
                                        'kec' => $this->input->post('kec',TRUE),
                                );
                        $insert = $this->Lokasisample_model->add($data);
                        echo json_encode(array("status" => TRUE));
                }

        public function lokasi_sample_get($id)
                {
                        $data = $this->Lokasisample_model->get_by_id($id);
                        echo json_encode($data);
                }
        public function lokasi_sample_update()
                {
                $data = array(
                                'kab_kota' => $this->input->post('kab_kota',TRUE),
                                'kec' => $this->input->post('kec',TRUE),
                        );
                $this->Lokasisample_model->update(array('id' => $this->input->post('id')), $data);
                echo json_encode(array("status" => TRUE));
                }

        public function lokasi_sample_delete($id)
                {
                        $this->Lokasisample_model->delete($id);
                        echo json_encode(array("status" => TRUE));
                }

        /////
        public function rekomtakaran() {                  
                $data['list']=$this->Rekomtakaran_model->get_all();
                $data['row'] = $this->Rekomtakaran_model->get_by_id($this->input->post('id'));

                $this->load->view('backend/header');                
                $this->load->view('backend/rekomtakaran',$data);                                         
        }

        public function rekomtakaran_add() 
                {            

                $nitrogen = $this->input->post('nitrogen');
                if ($nitrogen == 'Sangat Sesuai') $nnitrogen='5';
                if ($nitrogen == 'Sesuai') $nnitrogen='4';
                if ($nitrogen == 'Cukup Sesuai') $nnitrogen='3';
                if ($nitrogen == 'Tidak Sesuai') $nnitrogen='2';
                if ($nitrogen == 'Sangat Tidak Sesuai') $nnitrogen='1';

                $posfor = $this->input->post('posfor');
                if ($posfor == 'Sangat Sesuai') $nposfor='5';
                if ($posfor == 'Sesuai') $nposfor='4';
                if ($posfor == 'Cukup Sesuai') $nposfor='3';
                if ($posfor == 'Tidak Sesuai') $nposfor='2';
                if ($posfor == 'Sangat Tidak Sesuai') $nposfor='1';

                $kalium = $this->input->post('kalium');
                if ($kalium == 'Sangat Sesuai') $nkalium='5';
                if ($kalium == 'Sesuai') $nkalium='4';
                if ($kalium == 'Cukup Sesuai') $nkalium='3';
                if ($kalium == 'Tidak Sesuai') $nkalium='2';
                if ($kalium == 'Sangat Tidak Sesuai') $nkalium='1';

                $tekstur = $this->input->post('tekstur');
                if ($tekstur == 'Sangat Sesuai') $ntekstur='5';
                if ($tekstur == 'Sesuai') $ntekstur='4';
                if ($tekstur == 'Cukup Sesuai') $ntekstur='3';
                if ($tekstur == 'Tidak Sesuai') $ntekstur='2';
                if ($tekstur == 'Sangat Tidak Sesuai') $ntekstur='1';
                
                        $data = array(                          
                                        'formula' => $this->input->post('formula',TRUE),
                                        'nitrogen' => $this->input->post('nitrogen',TRUE),
                                        'posfor' => $this->input->post('posfor',TRUE),
                                        'kalium' => $this->input->post('kalium',TRUE),
                                        'tekstur' => $this->input->post('tekstur',TRUE),
                                        'nitrogen2' => $nnitrogen,
                                        'posfor2' => $nposfor,
                                        'kalium2' => $nkalium,
                                        'tekstur2' => $ntekstur,                           
                                );
                        $insert = $this->Rekomtakaran_model->add($data);
                        echo json_encode(array("status" => TRUE));
                }

        public function rekomtakaran_get($id)
                {
                        $data = $this->Rekomtakaran_model->get_by_id($id);
                        echo json_encode($data);
                }
        public function rekomtakaran_update()
                {
                
                $nitrogen = $this->input->post('nitrogen');
                if ($nitrogen == 'Sangat Sesuai') $nnitrogen='5';
                if ($nitrogen == 'Sesuai') $nnitrogen='4';
                if ($nitrogen == 'Cukup Sesuai') $nnitrogen='3';
                if ($nitrogen == 'Tidak Sesuai') $nnitrogen='2';
                if ($nitrogen == 'Sangat Tidak Sesuai') $nnitrogen='1';

                $posfor = $this->input->post('posfor');
                if ($posfor == 'Sangat Sesuai') $nposfor='5';
                if ($posfor == 'Sesuai') $nposfor='4';
                if ($posfor == 'Cukup Sesuai') $nposfor='3';
                if ($posfor == 'Tidak Sesuai') $nposfor='2';
                if ($posfor == 'Sangat Tidak Sesuai') $nposfor='1';

                $kalium = $this->input->post('kalium');
                if ($kalium == 'Sangat Sesuai') $nkalium='5';
                if ($kalium == 'Sesuai') $nkalium='4';
                if ($kalium == 'Cukup Sesuai') $nkalium='3';
                if ($kalium == 'Tidak Sesuai') $nkalium='2';
                if ($kalium == 'Sangat Tidak Sesuai') $nkalium='1';

                $tekstur = $this->input->post('tekstur');
                if ($tekstur == 'Sangat Sesuai') $ntekstur='5';
                if ($tekstur == 'Sesuai') $ntekstur='4';
                if ($tekstur == 'Cukup Sesuai') $ntekstur='3';
                if ($tekstur == 'Tidak Sesuai') $ntekstur='2';
                if ($tekstur == 'Sangat Tidak Sesuai') $ntekstur='1';
                
                        $data = array(                          
                                        'formula' => $this->input->post('formula',TRUE),
                                        'nitrogen' => $this->input->post('nitrogen',TRUE),
                                        'posfor' => $this->input->post('posfor',TRUE),
                                        'kalium' => $this->input->post('kalium',TRUE),
                                        'tekstur' => $this->input->post('tekstur',TRUE),
                                        'nitrogen2' => $nnitrogen,
                                        'posfor2' => $nposfor,
                                        'kalium2' => $nkalium,
                                        'tekstur2' => $ntekstur,                           
                                );
                $this->Rekomtakaran_model->update(array('id' => $this->input->post('id')), $data);
                echo json_encode(array("status" => TRUE));
                }

        public function rekomtakaran_delete($id)
                {
                        $this->Rekomtakaran_model->delete($id);
                        echo json_encode(array("status" => TRUE));
                }

        ///
        public function informasi() {                  
                $data['list']=$this->M_informasi->get(array('active' => '1'))->result();
                $this->load->view('backend/header');                
                $this->load->view('backend/informasi',$data);                                         
        }

        public function informasi_add() 
        {               
                $config['upload_path']="./upload";
                $config['allowed_types']='gif|jpg|png';
                $config['encrypt_name'] = TRUE; 
                $this->load->library('upload',$config);
                if($this->upload->do_upload("path")){
                        $data = array(
                                'name' => $this->input->post('name'),
                                'metatittle' => $this->input->post('name'),
                                'dest' => $this->input->post('dest'),
                                'dest2' => $this->input->post('dest'),
                                'path' => $this->upload->data()['file_name'],
                                'url'=> $this->input->post('url'),
                                'active'=> 1,
                                'type'=> 1,
                                'id_user'=>$this->session->userdata('id'),
                        );
                } else {
                        $data = array(
                                'name' => $this->input->post('name'),
                                'metatittle' => $this->input->post('name'),
                                'dest' => $this->input->post('dest'),
                                'dest2' => $this->input->post('dest'),
                                'url'=> $this->input->post('url')
                        );
                } 
                $this->M_informasi->add($data);
                echo json_encode(array("status" => TRUE));                                   
        }

        public function informasi_get($id)
        {
                $data = $this->M_informasi->get_by_id($id);
                echo json_encode($data);
        }

        public function informasi_update() 
                {                        
                       $config['upload_path']="./upload";
                        $config['allowed_types']='gif|jpg|png';
                        $config['encrypt_name'] = TRUE; 
                        $this->load->library('upload',$config);
                        if($this->upload->do_upload("path")){
                        $data = array(
                                'name' => $this->input->post('name'),
                                'metatittle' => $this->input->post('name'),
                                'dest' => $this->input->post('dest'),
                                'dest2' => $this->input->post('dest'),
                                'path' => $this->upload->data()['file_name'],
                                'url'=> $this->input->post('url'),
                        );
                        } else {
                        $data = array(
                                'name' => $this->input->post('name'),
                                'metatittle' => $this->input->post('name'),
                                'dest' => $this->input->post('dest'),
                                'dest2' => $this->input->post('dest'),
                                'url'=> $this->input->post('url')
                        );
                        }   
                        $this->M_informasi->update(array('id' => $this->input->post('id')), $data);
                        echo json_encode(array("status" => TRUE));
                }

        public function informasi_delete($id)
                {
                        $this->M_informasi->delete_by_id($id);
                        echo json_encode(array("status" => TRUE));
                }

        public function data_rekomendasi() {                  
                $data['list']=$this->Rekomedasi_model->get_hasil_rekomendasi_admin();
                $this->load->view('backend/header');                
                $this->load->view('backend/data_rekomendasi',$data);                                         
        }


        public function logout() {  
                $this->simple_login->logout();  
        } 


}  