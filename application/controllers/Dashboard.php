<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 	}

	public function index()
	{
		$data['list']='';
		$this->load->view('frontend/header');
		$this->load->view('frontend/dashboard',$data);
		$this->load->view('frontend/footer');
	}
}
