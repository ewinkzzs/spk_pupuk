<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekomedasi_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function get_lokasisample()
	{
		$this->db->from('lokasisample');
		$query=$this->db->get();
		return $query;
	}

	public function get_rekomtakaran()
	{
		$this->db->from('rekomtakaran');
		$query=$this->db->get();
		return $query;
	}

	public function get_poin($id)
	{
		$results = array();
		$query = $this->db->query(' SELECT
							poin.id,
							poin.id_analisa,
							CONCAT(poin.kd_formula,"-",rekomtakaran.formula) AS formula,
							poin.poin
							FROM
							poin
							INNER JOIN rekomtakaran ON poin.kd_formula = rekomtakaran.kd_formula
							where poin.id_analisa='.$id.'
							ORDER BY
							poin.poin DESC LIMIT 3');
		return $query->result();			
	}

	public function get_poin_topsis($id)
	{
		$results = array();
		$query = $this->db->query(' SELECT
							poin_topsis.id,
							poin_topsis.id_analisa,
							CONCAT(poin_topsis.kd_formula,"-",rekomtakaran.formula) AS formula,
							poin_topsis.poin
							FROM
							poin_topsis
							INNER JOIN rekomtakaran ON poin_topsis.kd_formula = rekomtakaran.kd_formula
							where poin_topsis.id_analisa='.$id.'
							ORDER BY
							poin_topsis.poin DESC LIMIT 3');
		return $query->result();			
	}

	public function get_dataujitanah_DSC()
	{
		$results = array();
		$query = $this->db->query(' SELECT *FROM dataujitanah ORDER BY id DESC');
		return $query->row();			
	}

	public function add_dataujitanah($data)
	{
		$this->db->insert('dataujitanah', $data);
		return $this->db->insert_id();
	}

	public function add_poin_electre($data)
	{
		$this->db->insert('poin', $data);
		return $this->db->insert_id();
	}

	public function add_poin_topsis($data)
	{
		$this->db->insert('poin_topsis', $data);
		return $this->db->insert_id();
	}

	public function get_hasil($id)
	{
		$results = array();
		$query = $this->db->query(' SELECT
									dataujitanah.id,
									dataujitanah.id_user,
									users.nama,
									dataujitanah.id_lokasisample,
									lokasisample.kab_kota,
									lokasisample.kec,
									dataujitanah.nama_sample,
									dataujitanah.tgl_uji,
									dataujitanah.nitrogen,
									dataujitanah.posfor,
									dataujitanah.kalium,
									dataujitanah.tekstur,
									dataujitanah.nitrogen2,
									dataujitanah.posfor2,
									dataujitanah.kalium2,
									dataujitanah.tekstur2
									FROM
									dataujitanah
									INNER JOIN lokasisample ON dataujitanah.id_lokasisample = lokasisample.id
									INNER JOIN users ON dataujitanah.id_user = users.id
									WHERE id_user='.$id.' order by dataujitanah.id ASC');
		return $query->result();			
	}

	public function get_hasil_rekomendasi_admin()
	{
		$results = array();
		$query = $this->db->query(' SELECT
									dataujitanah.id,
									dataujitanah.id_user,
									users.nama,
									dataujitanah.id_lokasisample,
									lokasisample.kab_kota,
									lokasisample.kec,
									dataujitanah.nama_sample,
									dataujitanah.tgl_uji,
									dataujitanah.nitrogen,
									dataujitanah.posfor,
									dataujitanah.kalium,
									dataujitanah.tekstur,
									dataujitanah.nitrogen2,
									dataujitanah.posfor2,
									dataujitanah.kalium2,
									dataujitanah.tekstur2
									FROM
									dataujitanah
									INNER JOIN lokasisample ON dataujitanah.id_lokasisample = lokasisample.id
									INNER JOIN users ON dataujitanah.id_user = users.id
									order by dataujitanah.id ASC');
		return $query->result();			
	}

	public function get_cetak($id)
	{
		$results = array();
		$query = $this->db->query(' SELECT
									dataujitanah.id,
									dataujitanah.id_user,
									users.nama,
									dataujitanah.id_lokasisample,
									lokasisample.kab_kota,
									lokasisample.kec,
									dataujitanah.nama_sample,
									dataujitanah.tgl_uji,
									dataujitanah.nitrogen,
									dataujitanah.posfor,
									dataujitanah.kalium,
									dataujitanah.tekstur,
									dataujitanah.nitrogen2,
									dataujitanah.posfor2,
									dataujitanah.kalium2,
									dataujitanah.tekstur2
									FROM
									dataujitanah
									INNER JOIN lokasisample ON dataujitanah.id_lokasisample = lokasisample.id
									INNER JOIN users ON dataujitanah.id_user = users.id
									WHERE dataujitanah.id='.$id.'');
		return $query->row();			
	}


	public function get_rank_one($id)
	{
		$results = array();
		$query = $this->db->query(' SELECT
									poin_topsis.id,
									poin_topsis.id_analisa,
									poin_topsis.kd_formula,
									poin_topsis.poin,
									rekomtakaran.formula
									FROM
									poin_topsis
									INNER JOIN rekomtakaran ON poin_topsis.kd_formula = rekomtakaran.kd_formula
									WHERE
									poin_topsis.id_analisa = '.$id.'
									ORDER BY
									poin_topsis.poin DESC LIMIT 1');
		return $query->row();			
	}

	

}

/* End of file remomedasi_model.php */
/* Location: ./application/models/remomedasi_model.php */