<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rule_model extends CI_Model
{
private $rule = 'rule';   //site log
private $penyakit = 'penyakit'; 
private $gejala = 'gejala'; 
 
    function __construct() {
        
    }
 
    function get_penyakit() {
        $results = array();
        $query = $this->db->query('SELECT rule.kd_gejala,rule.kd_penyakit,penyakit.kd_penyakit AS kd_penyakit2,penyakit.nama_penyakit AS penyakit FROM ' . $this->rule . ',' . $this->penyakit . ' 
				 WHERE rule.kd_penyakit=penyakit.kd_penyakit 
				 GROUP BY rule.kd_penyakit');
        if ($query->num_rows() > 0) {
                return $query->result();
 
        return $results;
    	}
    }

    function get_gejala($id) {
        $results = array();
        $query = $this->db->query('SELECT rule.id_rule,rule.id_rule,rule.kd_gejala,rule.bobot,rule.kd_penyakit,gejala.gejala AS namagejala 
								   FROM ' . $this->rule . ',' . $this->gejala . ' 
								   WHERE rule.kd_penyakit="' . $id . '" AND gejala.id_gejala=rule.kd_gejala');
        $this->db->where('id',$id);
        if ($query->num_rows() > 0) {
                return $query->result();
 
        return $results;
    	}
    }

    public function get_gejala_all()
    {
        $this->db->from('gejala');
        $query=$this->db->get();
        return $query->result();
    }

        public function get_penyakit_all()
    {
        $this->db->from('penyakit');
        $query=$this->db->get();
        return $query->result();
    }

        public function rule_add($data)
    {
        $this->db->insert('rule', $data);
        return $this->db->insert_id();
    }

    public function delete_by_id($id)
    {
        $this->db->where('id_rule', $id);
        $this->db->delete('rule');
    }


 
}