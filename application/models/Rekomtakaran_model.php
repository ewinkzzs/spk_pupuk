<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekomtakaran_model extends CI_Model
{

	var $table = 'rekomtakaran'; 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function get_all()
	{
	$this->db->from($this->table);
	$this->db->order_by('id','ASC');
	$query=$this->db->get();
	return $query->result();
	}


	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function add($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}


}