<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_informasi extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get($where)
	{
		$this->db->from('informasi');
		$this->db->where($where);
		$query = $this->db->get();
		return $query;
	}


	public function get_()
	{
		$results = array();
        $query = $this->db->query(' SELECT penanggulangan.id,penanggulangan.kdPenyakit,
									penyakit.nama_penyakit,penanggulangan.penanggulangan
									FROM penanggulangan
									INNER JOIN penyakit ON penanggulangan.kdPenyakit = penyakit.kd_penyakit
									');
   
        return $query->result();
	}


	public function get_by_id($id)
	{
		$this->db->from('informasi');
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function add($data)
	{
		$this->db->insert('informasi', $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update('informasi', $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('informasi');
	}


}