/*
 Navicat Premium Data Transfer

 Source Server         : Coding
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : 127.0.0.1:3306
 Source Schema         : spk_pupuk

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 26/08/2018 10:57:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dataujitanah
-- ----------------------------
DROP TABLE IF EXISTS `dataujitanah`;
CREATE TABLE `dataujitanah`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT ' ',
  `id_user` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_sample` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_lokasisample` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_uji` date NULL DEFAULT NULL,
  `nitrogen` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `posfor` varbinary(1) NULL DEFAULT NULL,
  `kalium` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tekstur` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nitrogen2` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `posfor2` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kalium2` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tekstur2` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 88 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dataujitanah
-- ----------------------------
INSERT INTO `dataujitanah` VALUES (32, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (31, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (30, '7', 'tes', '1', '2018-07-18', '1', 0x35, '5', '5', '< 0.20 (Rendah)', '> 40 (Tinggi)', '> 20 (Tinggi)', 'Berpasir');
INSERT INTO `dataujitanah` VALUES (33, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (34, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (35, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (36, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (37, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (38, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (39, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (40, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (41, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (42, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (43, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (44, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (45, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (46, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (47, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (48, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (49, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (50, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (51, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (52, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (53, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (54, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (55, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (56, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (57, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (58, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (59, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (60, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (61, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (62, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (63, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (64, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (65, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (66, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (67, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (68, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (69, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (70, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (71, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (72, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (73, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (74, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (75, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (76, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (77, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (78, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (79, '2', 'ddd', '1', '2018-08-11', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (80, '7', 'ddd', '2', '2018-08-13', '1', 0x31, '3', '1', '< 0.20 (Rendah)', '< 20 (Rendah)', '10-20 (Sedang)', 'Liat');
INSERT INTO `dataujitanah` VALUES (81, '7', 'ddd', '2', '2018-08-13', '1', 0x31, '3', '1', '< 0.20 (Rendah)', '< 20 (Rendah)', '10-20 (Sedang)', 'Liat');
INSERT INTO `dataujitanah` VALUES (82, '7', 'ddd', '2', '2018-08-13', '1', 0x31, '3', '1', '< 0.20 (Rendah)', '< 20 (Rendah)', '10-20 (Sedang)', 'Liat');
INSERT INTO `dataujitanah` VALUES (83, '7', 'ddd', '2', '2018-08-13', '1', 0x31, '3', '1', '< 0.20 (Rendah)', '< 20 (Rendah)', '10-20 (Sedang)', 'Liat');
INSERT INTO `dataujitanah` VALUES (84, '7', 'ddd', '2', '2018-08-13', '1', 0x31, '3', '1', '< 0.20 (Rendah)', '< 20 (Rendah)', '10-20 (Sedang)', 'Liat');
INSERT INTO `dataujitanah` VALUES (85, '7', 'ddd', '2', '2018-08-13', '1', 0x31, '3', '1', '< 0.20 (Rendah)', '< 20 (Rendah)', '10-20 (Sedang)', 'Liat');
INSERT INTO `dataujitanah` VALUES (86, '7', 'ddd', '1', '2018-08-14', '1', 0x33, '3', '3', '< 0.20 (Rendah)', '20-40 (Sedang)', '10-20 (Sedang)', 'Sedang');
INSERT INTO `dataujitanah` VALUES (87, '7', 'ddd', '1', '2018-08-14', '1', 0x31, '1', '3', '< 0.20 (Rendah)', '< 20 (Rendah)', '< 10 (Rendah)', 'Sedang');

-- ----------------------------
-- Table structure for informasi
-- ----------------------------
DROP TABLE IF EXISTS `informasi`;
CREATE TABLE `informasi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `dest` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `dest2` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type` int(11) NOT NULL,
  `path` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `url` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `metatittle` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `active` enum('1','0') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_user` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of informasi
-- ----------------------------
INSERT INTO `informasi` VALUES (1, '-', '', NULL, 0, '', '', '', '0', '1', '2018-06-06 13:53:41', '2018-06-06 13:57:38');
INSERT INTO `informasi` VALUES (16, 'Rumah Adat Atakae', '<p>Kawasan Wisata Atakae terletak di kelurahan Atakae kecamatan Tempe yang dibangunpada tahun 1995 tepatnya ditepian Danau Lumpulung sekitar 3 km dari sebelah timur kota Sengkang.di kawasan ini terdapat rumah tradisional yang berukuran besar yang disebut Saoraja La Tenri Bali dan juga terdapat 10 rumah tradisional. Saoraja La Tenri Bali memiliki 101 tiang penyangga yang memiliki 2 ton pada setiap tiang.tiangnya terbuat dari kayu Ulin Kalimantan.</p>', '<p>Kawasan Wisata Atakae terletak di kelurahan Atakae kecamatan Tempe yang dibangunpada tahun 1995 tepatnya ditepian Danau Lumpulung sekitar 3 km dari', 1, 'padi1.jpg', 'rumah-adat-atakae', 'Rumah Adat Atakae', '1', '1', '2018-06-06 13:53:41', '2018-06-08 13:52:27');
INSERT INTO `informasi` VALUES (17, 'Budidaya Murbei Dan Ulat Sutera', '<p>Budidaya ini terletak di Kec.Sambangparu,yaitu 7 km dari kota sengkang. Wisatawan yang dapat melihat tanaman merbai, ulat, kepompomg, dan proses pemintalan dari kepompong menjadi benang.</p>', '<p>Budidaya ini terletak di Kec.Sambangparu,yaitu 7 km dari kota sengkang. Wisatawan yang dapat melihat tanaman merbai, ulat, kepompomg, dan proses pe', 1, 'padi2.jpg', 'budidaya-murbei-dan-ulat-sutera', 'Budidaya Murbei Dan Ulat Sutera', '1', '1', '2018-06-06 13:53:41', '2018-06-08 13:52:32');
INSERT INTO `informasi` VALUES (18, 'Saoraja Mallangga', '<p>Saoraja Mallangga terletak dipusat kota Sengkang kec.Tempe Kabupaten Wajo. Rumah ini dibangun 1933pada masa pemerintahan Arung Betteng Pola ke 27. Didalam Saoraja Mallangga terdapat beberapa benda Pusaka yaitu Latea Kasih,Laula Balu,Cobok\'e, dan Balubu.</p>', '<p>Saoraja Mallangga terletak dipusat kota Sengkang kec.Tempe Kabupaten Wajo. Rumah ini dibangun 1933pada masa pemerintahan Arung Betteng Pola ke 27. ', 1, 'gambar-tani.png', 'saoraja-mallangga', 'Saoraja Mallangga', '1', '1', '2018-06-06 13:53:41', '2018-06-08 13:52:35');
INSERT INTO `informasi` VALUES (20, 'Belanda', 'Belanda dd Indonesia 3,5 abad. Jepang menjajah Indonesia 3,5 tahun. Demikianlah pendapat hampir selurdddduh rakyat Indonesia hingga kini. Yang sehubungan dengan masa penjajahan Belanda 3,5 abad, tidak pernah dijelaskan secara rinci, kapan dimulainya pendd', 'Belanda dd Indonesia 3,5 abad. Jepang menjajah Indonesia 3,5 tahun. Demikianlah pendapat hampir selurdddduh rakyat Indonesia hingga kini. Yang sehubun', 1, '1aac09c550b556feecb8080bb31ac1df.png', 'belna-blog', 'Belanda', '1', '7', '2018-07-19 08:22:40', '2018-07-19 08:22:40');

-- ----------------------------
-- Table structure for lokasisample
-- ----------------------------
DROP TABLE IF EXISTS `lokasisample`;
CREATE TABLE `lokasisample`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kab_kota` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kec` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lokasisample
-- ----------------------------
INSERT INTO `lokasisample` VALUES (1, 'Kab', 'kec');
INSERT INTO `lokasisample` VALUES (2, '20', '30');

-- ----------------------------
-- Table structure for poin
-- ----------------------------
DROP TABLE IF EXISTS `poin`;
CREATE TABLE `poin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_analisa` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kd_formula` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `poin` int(3) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3367 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of poin
-- ----------------------------
INSERT INTO `poin` VALUES (3095, '66', 'RRR', 5);
INSERT INTO `poin` VALUES (3096, '66', 'RRS-RRT', 8);
INSERT INTO `poin` VALUES (3097, '66', 'RSR', 8);
INSERT INTO `poin` VALUES (3098, '66', 'RSS-RST', 8);
INSERT INTO `poin` VALUES (3099, '66', 'RTR', 10);
INSERT INTO `poin` VALUES (3100, '66', 'RTS-RTT', 12);
INSERT INTO `poin` VALUES (3101, '66', 'SRR', 8);
INSERT INTO `poin` VALUES (3102, '66', 'SRS-SRT', 12);
INSERT INTO `poin` VALUES (3103, '66', 'SSR', 11);
INSERT INTO `poin` VALUES (3104, '66', 'SSS-SST', 14);
INSERT INTO `poin` VALUES (3105, '66', 'STR', 13);
INSERT INTO `poin` VALUES (3106, '66', 'TSR', 16);
INSERT INTO `poin` VALUES (3107, '66', 'STS-STT', 16);
INSERT INTO `poin` VALUES (3108, '66', 'TRS-TRT', 14);
INSERT INTO `poin` VALUES (3109, '66', 'TSS-TST', 16);
INSERT INTO `poin` VALUES (3110, '66', 'TTR', 16);
INSERT INTO `poin` VALUES (3111, '66', 'TTS-TTT', 16);
INSERT INTO `poin` VALUES (3112, '67', 'RRR', 5);
INSERT INTO `poin` VALUES (3113, '67', 'RRS-RRT', 8);
INSERT INTO `poin` VALUES (3114, '67', 'RSR', 8);
INSERT INTO `poin` VALUES (3115, '67', 'RSS-RST', 8);
INSERT INTO `poin` VALUES (3116, '67', 'RTR', 10);
INSERT INTO `poin` VALUES (3117, '67', 'RTS-RTT', 12);
INSERT INTO `poin` VALUES (3118, '67', 'SRR', 8);
INSERT INTO `poin` VALUES (3119, '67', 'SRS-SRT', 12);
INSERT INTO `poin` VALUES (3120, '67', 'SSR', 11);
INSERT INTO `poin` VALUES (3121, '67', 'SSS-SST', 14);
INSERT INTO `poin` VALUES (3122, '67', 'STR', 13);
INSERT INTO `poin` VALUES (3123, '67', 'TSR', 16);
INSERT INTO `poin` VALUES (3124, '67', 'STS-STT', 16);
INSERT INTO `poin` VALUES (3125, '67', 'TRS-TRT', 14);
INSERT INTO `poin` VALUES (3126, '67', 'TSS-TST', 16);
INSERT INTO `poin` VALUES (3127, '67', 'TTR', 16);
INSERT INTO `poin` VALUES (3128, '67', 'TTS-TTT', 16);
INSERT INTO `poin` VALUES (3129, '69', 'RRR', 1);
INSERT INTO `poin` VALUES (3130, '69', 'RRS-RRT', 3);
INSERT INTO `poin` VALUES (3131, '69', 'RSR', 3);
INSERT INTO `poin` VALUES (3132, '69', 'RSS-RST', 3);
INSERT INTO `poin` VALUES (3133, '69', 'RTR', 4);
INSERT INTO `poin` VALUES (3134, '69', 'RTS-RTT', 7);
INSERT INTO `poin` VALUES (3135, '69', 'SRR', 4);
INSERT INTO `poin` VALUES (3136, '69', 'SRS-SRT', 6);
INSERT INTO `poin` VALUES (3137, '69', 'SSR', 7);
INSERT INTO `poin` VALUES (3138, '69', 'SSS-SST', 9);
INSERT INTO `poin` VALUES (3139, '69', 'STR', 5);
INSERT INTO `poin` VALUES (3140, '69', 'TSR', 7);
INSERT INTO `poin` VALUES (3141, '69', 'STS-STT', 10);
INSERT INTO `poin` VALUES (3142, '69', 'TRS-TRT', 11);
INSERT INTO `poin` VALUES (3143, '69', 'TSS-TST', 12);
INSERT INTO `poin` VALUES (3144, '69', 'TTR', 10);
INSERT INTO `poin` VALUES (3145, '69', 'TTS-TTT', 10);
INSERT INTO `poin` VALUES (3146, '70', 'RRR', 0);
INSERT INTO `poin` VALUES (3147, '70', 'RRS-RRT', 0);
INSERT INTO `poin` VALUES (3148, '70', 'RSR', 0);
INSERT INTO `poin` VALUES (3149, '70', 'RSS-RST', 0);
INSERT INTO `poin` VALUES (3150, '70', 'RTR', 0);
INSERT INTO `poin` VALUES (3151, '70', 'RTS-RTT', 0);
INSERT INTO `poin` VALUES (3152, '70', 'SRR', 3);
INSERT INTO `poin` VALUES (3153, '70', 'SRS-SRT', 3);
INSERT INTO `poin` VALUES (3154, '70', 'SSR', 5);
INSERT INTO `poin` VALUES (3155, '70', 'SSS-SST', 5);
INSERT INTO `poin` VALUES (3156, '70', 'STR', 3);
INSERT INTO `poin` VALUES (3157, '70', 'TSR', 3);
INSERT INTO `poin` VALUES (3158, '70', 'STS-STT', 5);
INSERT INTO `poin` VALUES (3159, '70', 'TRS-TRT', 9);
INSERT INTO `poin` VALUES (3160, '70', 'TSS-TST', 9);
INSERT INTO `poin` VALUES (3161, '70', 'TTR', 10);
INSERT INTO `poin` VALUES (3162, '70', 'TTS-TTT', 10);
INSERT INTO `poin` VALUES (3163, '71', 'RRR', 1);
INSERT INTO `poin` VALUES (3164, '71', 'RRS-RRT', 3);
INSERT INTO `poin` VALUES (3165, '71', 'RSR', 3);
INSERT INTO `poin` VALUES (3166, '71', 'RSS-RST', 3);
INSERT INTO `poin` VALUES (3167, '71', 'RTR', 4);
INSERT INTO `poin` VALUES (3168, '71', 'RTS-RTT', 7);
INSERT INTO `poin` VALUES (3169, '71', 'SRR', 4);
INSERT INTO `poin` VALUES (3170, '71', 'SRS-SRT', 6);
INSERT INTO `poin` VALUES (3171, '71', 'SSR', 7);
INSERT INTO `poin` VALUES (3172, '71', 'SSS-SST', 9);
INSERT INTO `poin` VALUES (3173, '71', 'STR', 5);
INSERT INTO `poin` VALUES (3174, '71', 'TSR', 7);
INSERT INTO `poin` VALUES (3175, '71', 'STS-STT', 10);
INSERT INTO `poin` VALUES (3176, '71', 'TRS-TRT', 11);
INSERT INTO `poin` VALUES (3177, '71', 'TSS-TST', 12);
INSERT INTO `poin` VALUES (3178, '71', 'TTR', 10);
INSERT INTO `poin` VALUES (3179, '71', 'TTS-TTT', 10);
INSERT INTO `poin` VALUES (3180, '75', 'RRR', 1);
INSERT INTO `poin` VALUES (3181, '75', 'RRS-RRT', 3);
INSERT INTO `poin` VALUES (3182, '75', 'RSR', 3);
INSERT INTO `poin` VALUES (3183, '75', 'RSS-RST', 3);
INSERT INTO `poin` VALUES (3184, '75', 'RTR', 4);
INSERT INTO `poin` VALUES (3185, '75', 'RTS-RTT', 7);
INSERT INTO `poin` VALUES (3186, '75', 'SRR', 4);
INSERT INTO `poin` VALUES (3187, '75', 'SRS-SRT', 6);
INSERT INTO `poin` VALUES (3188, '75', 'SSR', 7);
INSERT INTO `poin` VALUES (3189, '75', 'SSS-SST', 9);
INSERT INTO `poin` VALUES (3190, '75', 'STR', 5);
INSERT INTO `poin` VALUES (3191, '75', 'TSR', 7);
INSERT INTO `poin` VALUES (3192, '75', 'STS-STT', 10);
INSERT INTO `poin` VALUES (3193, '75', 'TRS-TRT', 11);
INSERT INTO `poin` VALUES (3194, '75', 'TSS-TST', 12);
INSERT INTO `poin` VALUES (3195, '75', 'TTR', 10);
INSERT INTO `poin` VALUES (3196, '75', 'TTS-TTT', 10);
INSERT INTO `poin` VALUES (3197, '77', 'RRR', 1);
INSERT INTO `poin` VALUES (3198, '77', 'RRS-RRT', 3);
INSERT INTO `poin` VALUES (3199, '77', 'RSR', 3);
INSERT INTO `poin` VALUES (3200, '77', 'RSS-RST', 3);
INSERT INTO `poin` VALUES (3201, '77', 'RTR', 4);
INSERT INTO `poin` VALUES (3202, '77', 'RTS-RTT', 7);
INSERT INTO `poin` VALUES (3203, '77', 'SRR', 4);
INSERT INTO `poin` VALUES (3204, '77', 'SRS-SRT', 6);
INSERT INTO `poin` VALUES (3205, '77', 'SSR', 7);
INSERT INTO `poin` VALUES (3206, '77', 'SSS-SST', 9);
INSERT INTO `poin` VALUES (3207, '77', 'STR', 5);
INSERT INTO `poin` VALUES (3208, '77', 'TSR', 7);
INSERT INTO `poin` VALUES (3209, '77', 'STS-STT', 10);
INSERT INTO `poin` VALUES (3210, '77', 'TRS-TRT', 11);
INSERT INTO `poin` VALUES (3211, '77', 'TSS-TST', 12);
INSERT INTO `poin` VALUES (3212, '77', 'TTR', 10);
INSERT INTO `poin` VALUES (3213, '77', 'TTS-TTT', 10);
INSERT INTO `poin` VALUES (3214, '79', 'RRR', 1);
INSERT INTO `poin` VALUES (3215, '79', 'RRS-RRT', 3);
INSERT INTO `poin` VALUES (3216, '79', 'RSR', 3);
INSERT INTO `poin` VALUES (3217, '79', 'RSS-RST', 3);
INSERT INTO `poin` VALUES (3218, '79', 'RTR', 4);
INSERT INTO `poin` VALUES (3219, '79', 'RTS-RTT', 7);
INSERT INTO `poin` VALUES (3220, '79', 'SRR', 4);
INSERT INTO `poin` VALUES (3221, '79', 'SRS-SRT', 6);
INSERT INTO `poin` VALUES (3222, '79', 'SSR', 7);
INSERT INTO `poin` VALUES (3223, '79', 'SSS-SST', 9);
INSERT INTO `poin` VALUES (3224, '79', 'STR', 5);
INSERT INTO `poin` VALUES (3225, '79', 'TSR', 7);
INSERT INTO `poin` VALUES (3226, '79', 'STS-STT', 10);
INSERT INTO `poin` VALUES (3227, '79', 'TRS-TRT', 11);
INSERT INTO `poin` VALUES (3228, '79', 'TSS-TST', 12);
INSERT INTO `poin` VALUES (3229, '79', 'TTR', 10);
INSERT INTO `poin` VALUES (3230, '79', 'TTS-TTT', 10);
INSERT INTO `poin` VALUES (3231, '80', 'RRR', 1);
INSERT INTO `poin` VALUES (3232, '80', 'RRS-RRT', 3);
INSERT INTO `poin` VALUES (3233, '80', 'RSR', 3);
INSERT INTO `poin` VALUES (3234, '80', 'RSS-RST', 3);
INSERT INTO `poin` VALUES (3235, '80', 'RTR', 4);
INSERT INTO `poin` VALUES (3236, '80', 'RTS-RTT', 7);
INSERT INTO `poin` VALUES (3237, '80', 'SRR', 4);
INSERT INTO `poin` VALUES (3238, '80', 'SRS-SRT', 6);
INSERT INTO `poin` VALUES (3239, '80', 'SSR', 7);
INSERT INTO `poin` VALUES (3240, '80', 'SSS-SST', 9);
INSERT INTO `poin` VALUES (3241, '80', 'STR', 5);
INSERT INTO `poin` VALUES (3242, '80', 'TSR', 7);
INSERT INTO `poin` VALUES (3243, '80', 'STS-STT', 10);
INSERT INTO `poin` VALUES (3244, '80', 'TRS-TRT', 11);
INSERT INTO `poin` VALUES (3245, '80', 'TSS-TST', 12);
INSERT INTO `poin` VALUES (3246, '80', 'TTR', 10);
INSERT INTO `poin` VALUES (3247, '80', 'TTS-TTT', 10);
INSERT INTO `poin` VALUES (3248, '81', 'RRR', 1);
INSERT INTO `poin` VALUES (3249, '81', 'RRS-RRT', 3);
INSERT INTO `poin` VALUES (3250, '81', 'RSR', 3);
INSERT INTO `poin` VALUES (3251, '81', 'RSS-RST', 3);
INSERT INTO `poin` VALUES (3252, '81', 'RTR', 4);
INSERT INTO `poin` VALUES (3253, '81', 'RTS-RTT', 7);
INSERT INTO `poin` VALUES (3254, '81', 'SRR', 4);
INSERT INTO `poin` VALUES (3255, '81', 'SRS-SRT', 6);
INSERT INTO `poin` VALUES (3256, '81', 'SSR', 7);
INSERT INTO `poin` VALUES (3257, '81', 'SSS-SST', 9);
INSERT INTO `poin` VALUES (3258, '81', 'STR', 5);
INSERT INTO `poin` VALUES (3259, '81', 'TSR', 11);
INSERT INTO `poin` VALUES (3260, '81', 'STS-STT', 7);
INSERT INTO `poin` VALUES (3261, '81', 'TRS-TRT', 10);
INSERT INTO `poin` VALUES (3262, '81', 'TSS-TST', 12);
INSERT INTO `poin` VALUES (3263, '81', 'TTR', 10);
INSERT INTO `poin` VALUES (3264, '81', 'TTS-TTT', 10);
INSERT INTO `poin` VALUES (3265, '82', 'RRR', 1);
INSERT INTO `poin` VALUES (3266, '82', 'RRS-RRT', 3);
INSERT INTO `poin` VALUES (3267, '82', 'RSR', 3);
INSERT INTO `poin` VALUES (3268, '82', 'RSS-RST', 3);
INSERT INTO `poin` VALUES (3269, '82', 'RTR', 4);
INSERT INTO `poin` VALUES (3270, '82', 'RTS-RTT', 7);
INSERT INTO `poin` VALUES (3271, '82', 'SRR', 4);
INSERT INTO `poin` VALUES (3272, '82', 'SRS-SRT', 6);
INSERT INTO `poin` VALUES (3273, '82', 'SSR', 7);
INSERT INTO `poin` VALUES (3274, '82', 'SSS-SST', 9);
INSERT INTO `poin` VALUES (3275, '82', 'STR', 5);
INSERT INTO `poin` VALUES (3276, '82', 'TSR', 11);
INSERT INTO `poin` VALUES (3277, '82', 'STS-STT', 7);
INSERT INTO `poin` VALUES (3278, '82', 'TRS-TRT', 10);
INSERT INTO `poin` VALUES (3279, '82', 'TSS-TST', 12);
INSERT INTO `poin` VALUES (3280, '82', 'TTR', 10);
INSERT INTO `poin` VALUES (3281, '82', 'TTS-TTT', 10);
INSERT INTO `poin` VALUES (3282, '83', 'RRR', 1);
INSERT INTO `poin` VALUES (3283, '83', 'RRS-RRT', 3);
INSERT INTO `poin` VALUES (3284, '83', 'RSR', 3);
INSERT INTO `poin` VALUES (3285, '83', 'RSS-RST', 3);
INSERT INTO `poin` VALUES (3286, '83', 'RTR', 5);
INSERT INTO `poin` VALUES (3287, '83', 'RTS-RTT', 7);
INSERT INTO `poin` VALUES (3288, '83', 'SRR', 5);
INSERT INTO `poin` VALUES (3289, '83', 'SRS-SRT', 7);
INSERT INTO `poin` VALUES (3290, '83', 'SSR', 7);
INSERT INTO `poin` VALUES (3291, '83', 'SSS-SST', 9);
INSERT INTO `poin` VALUES (3292, '83', 'STR', 8);
INSERT INTO `poin` VALUES (3293, '83', 'TSR', 11);
INSERT INTO `poin` VALUES (3294, '83', 'STS-STT', 11);
INSERT INTO `poin` VALUES (3295, '83', 'TRS-TRT', 12);
INSERT INTO `poin` VALUES (3296, '83', 'TSS-TST', 12);
INSERT INTO `poin` VALUES (3297, '83', 'TTR', 12);
INSERT INTO `poin` VALUES (3298, '83', 'TTS-TTT', 12);
INSERT INTO `poin` VALUES (3299, '84', 'RRR', 1);
INSERT INTO `poin` VALUES (3300, '84', 'RRS-RRT', 3);
INSERT INTO `poin` VALUES (3301, '84', 'RSR', 3);
INSERT INTO `poin` VALUES (3302, '84', 'RSS-RST', 3);
INSERT INTO `poin` VALUES (3303, '84', 'RTR', 5);
INSERT INTO `poin` VALUES (3304, '84', 'RTS-RTT', 7);
INSERT INTO `poin` VALUES (3305, '84', 'SRR', 5);
INSERT INTO `poin` VALUES (3306, '84', 'SRS-SRT', 7);
INSERT INTO `poin` VALUES (3307, '84', 'SSR', 7);
INSERT INTO `poin` VALUES (3308, '84', 'SSS-SST', 9);
INSERT INTO `poin` VALUES (3309, '84', 'STR', 8);
INSERT INTO `poin` VALUES (3310, '84', 'TSR', 11);
INSERT INTO `poin` VALUES (3311, '84', 'STS-STT', 11);
INSERT INTO `poin` VALUES (3312, '84', 'TRS-TRT', 12);
INSERT INTO `poin` VALUES (3313, '84', 'TSS-TST', 12);
INSERT INTO `poin` VALUES (3314, '84', 'TTR', 12);
INSERT INTO `poin` VALUES (3315, '84', 'TTS-TTT', 12);
INSERT INTO `poin` VALUES (3316, '85', 'RRR', 1);
INSERT INTO `poin` VALUES (3317, '85', 'RRS-RRT', 3);
INSERT INTO `poin` VALUES (3318, '85', 'RSR', 3);
INSERT INTO `poin` VALUES (3319, '85', 'RSS-RST', 3);
INSERT INTO `poin` VALUES (3320, '85', 'RTR', 5);
INSERT INTO `poin` VALUES (3321, '85', 'RTS-RTT', 7);
INSERT INTO `poin` VALUES (3322, '85', 'SRR', 5);
INSERT INTO `poin` VALUES (3323, '85', 'SRS-SRT', 7);
INSERT INTO `poin` VALUES (3324, '85', 'SSR', 7);
INSERT INTO `poin` VALUES (3325, '85', 'SSS-SST', 9);
INSERT INTO `poin` VALUES (3326, '85', 'STR', 8);
INSERT INTO `poin` VALUES (3327, '85', 'STS-STT', 11);
INSERT INTO `poin` VALUES (3328, '85', 'TRS-TRT', 12);
INSERT INTO `poin` VALUES (3329, '85', 'TSR', 11);
INSERT INTO `poin` VALUES (3330, '85', 'TSS-TST', 12);
INSERT INTO `poin` VALUES (3331, '85', 'TTR', 12);
INSERT INTO `poin` VALUES (3332, '85', 'TTS-TTT', 12);
INSERT INTO `poin` VALUES (3333, '86', 'RRR', 1);
INSERT INTO `poin` VALUES (3334, '86', 'RRS-RRT', 3);
INSERT INTO `poin` VALUES (3335, '86', 'RSR', 3);
INSERT INTO `poin` VALUES (3336, '86', 'RSS-RST', 3);
INSERT INTO `poin` VALUES (3337, '86', 'RTR', 5);
INSERT INTO `poin` VALUES (3338, '86', 'RTS-RTT', 7);
INSERT INTO `poin` VALUES (3339, '86', 'SRR', 5);
INSERT INTO `poin` VALUES (3340, '86', 'SRS-SRT', 7);
INSERT INTO `poin` VALUES (3341, '86', 'SSR', 7);
INSERT INTO `poin` VALUES (3342, '86', 'SSS-SST', 9);
INSERT INTO `poin` VALUES (3343, '86', 'STR', 8);
INSERT INTO `poin` VALUES (3344, '86', 'STS-STT', 11);
INSERT INTO `poin` VALUES (3345, '86', 'TRS-TRT', 12);
INSERT INTO `poin` VALUES (3346, '86', 'TSR', 11);
INSERT INTO `poin` VALUES (3347, '86', 'TSS-TST', 12);
INSERT INTO `poin` VALUES (3348, '86', 'TTR', 12);
INSERT INTO `poin` VALUES (3349, '86', 'TTS-TTT', 12);
INSERT INTO `poin` VALUES (3350, '87', 'RRR', 0);
INSERT INTO `poin` VALUES (3351, '87', 'RRS-RRT', 0);
INSERT INTO `poin` VALUES (3352, '87', 'RSR', 0);
INSERT INTO `poin` VALUES (3353, '87', 'RSS-RST', 11);
INSERT INTO `poin` VALUES (3354, '87', 'RTR', 11);
INSERT INTO `poin` VALUES (3355, '87', 'RTS-RTT', 11);
INSERT INTO `poin` VALUES (3356, '87', 'SRR', 3);
INSERT INTO `poin` VALUES (3357, '87', 'SRS-SRT', 3);
INSERT INTO `poin` VALUES (3358, '87', 'SSR', 3);
INSERT INTO `poin` VALUES (3359, '87', 'SSS-SST', 3);
INSERT INTO `poin` VALUES (3360, '87', 'STR', 3);
INSERT INTO `poin` VALUES (3361, '87', 'STS-STT', 3);
INSERT INTO `poin` VALUES (3362, '87', 'TRS-TRT', 1);
INSERT INTO `poin` VALUES (3363, '87', 'TSR', 0);
INSERT INTO `poin` VALUES (3364, '87', 'TSS-TST', 2);
INSERT INTO `poin` VALUES (3365, '87', 'TTR', 1);
INSERT INTO `poin` VALUES (3366, '87', 'TTS-TTT', 3);

-- ----------------------------
-- Table structure for poin_topsis
-- ----------------------------
DROP TABLE IF EXISTS `poin_topsis`;
CREATE TABLE `poin_topsis`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_analisa` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kd_formula` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `poin` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3367 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of poin_topsis
-- ----------------------------
INSERT INTO `poin_topsis` VALUES (3095, '66', 'RRR', '0.127');
INSERT INTO `poin_topsis` VALUES (3096, '66', 'RRS-RRT', '0.255');
INSERT INTO `poin_topsis` VALUES (3097, '66', 'RSR', '0.312');
INSERT INTO `poin_topsis` VALUES (3098, '66', 'RSS-RST', '0.239');
INSERT INTO `poin_topsis` VALUES (3099, '66', 'RTR', '0.479');
INSERT INTO `poin_topsis` VALUES (3100, '66', 'RTS-RTT', '0.374');
INSERT INTO `poin_topsis` VALUES (3101, '66', 'SRR', '0.298');
INSERT INTO `poin_topsis` VALUES (3102, '66', 'SRS-SRT', '0.374');
INSERT INTO `poin_topsis` VALUES (3103, '66', 'SSR', '0.461');
INSERT INTO `poin_topsis` VALUES (3104, '66', 'SSS-SST', '0.539');
INSERT INTO `poin_topsis` VALUES (3105, '66', 'STR', '0.626');
INSERT INTO `poin_topsis` VALUES (3106, '66', 'TSR', '0.702');
INSERT INTO `poin_topsis` VALUES (3107, '66', 'STS-STT', '0.516');
INSERT INTO `poin_topsis` VALUES (3108, '66', 'TRS-TRT', '0.617');
INSERT INTO `poin_topsis` VALUES (3109, '66', 'TSS-TST', '0.688');
INSERT INTO `poin_topsis` VALUES (3110, '66', 'TTR', '0.745');
INSERT INTO `poin_topsis` VALUES (3111, '66', 'TTS-TTT', '0.873');
INSERT INTO `poin_topsis` VALUES (3112, '67', 'RRR', '0.127');
INSERT INTO `poin_topsis` VALUES (3113, '67', 'RRS-RRT', '0.255');
INSERT INTO `poin_topsis` VALUES (3114, '67', 'RSR', '0.312');
INSERT INTO `poin_topsis` VALUES (3115, '67', 'RSS-RST', '0.239');
INSERT INTO `poin_topsis` VALUES (3116, '67', 'RTR', '0.479');
INSERT INTO `poin_topsis` VALUES (3117, '67', 'RTS-RTT', '0.374');
INSERT INTO `poin_topsis` VALUES (3118, '67', 'SRR', '0.298');
INSERT INTO `poin_topsis` VALUES (3119, '67', 'SRS-SRT', '0.374');
INSERT INTO `poin_topsis` VALUES (3120, '67', 'SSR', '0.461');
INSERT INTO `poin_topsis` VALUES (3121, '67', 'SSS-SST', '0.539');
INSERT INTO `poin_topsis` VALUES (3122, '67', 'STR', '0.626');
INSERT INTO `poin_topsis` VALUES (3123, '67', 'TSR', '0.702');
INSERT INTO `poin_topsis` VALUES (3124, '67', 'STS-STT', '0.516');
INSERT INTO `poin_topsis` VALUES (3125, '67', 'TRS-TRT', '0.617');
INSERT INTO `poin_topsis` VALUES (3126, '67', 'TSS-TST', '0.688');
INSERT INTO `poin_topsis` VALUES (3127, '67', 'TTR', '0.745');
INSERT INTO `poin_topsis` VALUES (3128, '67', 'TTS-TTT', '0.873');
INSERT INTO `poin_topsis` VALUES (3129, '69', 'RRR', '0.127');
INSERT INTO `poin_topsis` VALUES (3130, '69', 'RRS-RRT', '0.255');
INSERT INTO `poin_topsis` VALUES (3131, '69', 'RSR', '0.312');
INSERT INTO `poin_topsis` VALUES (3132, '69', 'RSS-RST', '0.239');
INSERT INTO `poin_topsis` VALUES (3133, '69', 'RTR', '0.479');
INSERT INTO `poin_topsis` VALUES (3134, '69', 'RTS-RTT', '0.374');
INSERT INTO `poin_topsis` VALUES (3135, '69', 'SRR', '0.298');
INSERT INTO `poin_topsis` VALUES (3136, '69', 'SRS-SRT', '0.374');
INSERT INTO `poin_topsis` VALUES (3137, '69', 'SSR', '0.461');
INSERT INTO `poin_topsis` VALUES (3138, '69', 'SSS-SST', '0.539');
INSERT INTO `poin_topsis` VALUES (3139, '69', 'STR', '0.626');
INSERT INTO `poin_topsis` VALUES (3140, '69', 'TSR', '0.702');
INSERT INTO `poin_topsis` VALUES (3141, '69', 'STS-STT', '0.516');
INSERT INTO `poin_topsis` VALUES (3142, '69', 'TRS-TRT', '0.617');
INSERT INTO `poin_topsis` VALUES (3143, '69', 'TSS-TST', '0.688');
INSERT INTO `poin_topsis` VALUES (3144, '69', 'TTR', '0.745');
INSERT INTO `poin_topsis` VALUES (3145, '69', 'TTS-TTT', '0.873');
INSERT INTO `poin_topsis` VALUES (3146, '70', 'RRR', '0.127');
INSERT INTO `poin_topsis` VALUES (3147, '70', 'RRS-RRT', '0.255');
INSERT INTO `poin_topsis` VALUES (3148, '70', 'RSR', '0.312');
INSERT INTO `poin_topsis` VALUES (3149, '70', 'RSS-RST', '0.239');
INSERT INTO `poin_topsis` VALUES (3150, '70', 'RTR', '0.479');
INSERT INTO `poin_topsis` VALUES (3151, '70', 'RTS-RTT', '0.374');
INSERT INTO `poin_topsis` VALUES (3152, '70', 'SRR', '0.298');
INSERT INTO `poin_topsis` VALUES (3153, '70', 'SRS-SRT', '0.374');
INSERT INTO `poin_topsis` VALUES (3154, '70', 'SSR', '0.461');
INSERT INTO `poin_topsis` VALUES (3155, '70', 'SSS-SST', '0.539');
INSERT INTO `poin_topsis` VALUES (3156, '70', 'STR', '0.626');
INSERT INTO `poin_topsis` VALUES (3157, '70', 'TSR', '0.702');
INSERT INTO `poin_topsis` VALUES (3158, '70', 'STS-STT', '0.516');
INSERT INTO `poin_topsis` VALUES (3159, '70', 'TRS-TRT', '0.617');
INSERT INTO `poin_topsis` VALUES (3160, '70', 'TSS-TST', '0.688');
INSERT INTO `poin_topsis` VALUES (3161, '70', 'TTR', '0.745');
INSERT INTO `poin_topsis` VALUES (3162, '70', 'TTS-TTT', '0.873');
INSERT INTO `poin_topsis` VALUES (3163, '71', 'RRR', '0.127');
INSERT INTO `poin_topsis` VALUES (3164, '71', 'RRS-RRT', '0.255');
INSERT INTO `poin_topsis` VALUES (3165, '71', 'RSR', '0.312');
INSERT INTO `poin_topsis` VALUES (3166, '71', 'RSS-RST', '0.239');
INSERT INTO `poin_topsis` VALUES (3167, '71', 'RTR', '0.479');
INSERT INTO `poin_topsis` VALUES (3168, '71', 'RTS-RTT', '0.374');
INSERT INTO `poin_topsis` VALUES (3169, '71', 'SRR', '0.298');
INSERT INTO `poin_topsis` VALUES (3170, '71', 'SRS-SRT', '0.374');
INSERT INTO `poin_topsis` VALUES (3171, '71', 'SSR', '0.461');
INSERT INTO `poin_topsis` VALUES (3172, '71', 'SSS-SST', '0.539');
INSERT INTO `poin_topsis` VALUES (3173, '71', 'STR', '0.626');
INSERT INTO `poin_topsis` VALUES (3174, '71', 'TSR', '0.702');
INSERT INTO `poin_topsis` VALUES (3175, '71', 'STS-STT', '0.516');
INSERT INTO `poin_topsis` VALUES (3176, '71', 'TRS-TRT', '0.617');
INSERT INTO `poin_topsis` VALUES (3177, '71', 'TSS-TST', '0.688');
INSERT INTO `poin_topsis` VALUES (3178, '71', 'TTR', '0.745');
INSERT INTO `poin_topsis` VALUES (3179, '71', 'TTS-TTT', '0.873');
INSERT INTO `poin_topsis` VALUES (3180, '75', 'RRR', '0.127');
INSERT INTO `poin_topsis` VALUES (3181, '75', 'RRS-RRT', '0.255');
INSERT INTO `poin_topsis` VALUES (3182, '75', 'RSR', '0.312');
INSERT INTO `poin_topsis` VALUES (3183, '75', 'RSS-RST', '0.239');
INSERT INTO `poin_topsis` VALUES (3184, '75', 'RTR', '0.479');
INSERT INTO `poin_topsis` VALUES (3185, '75', 'RTS-RTT', '0.374');
INSERT INTO `poin_topsis` VALUES (3186, '75', 'SRR', '0.298');
INSERT INTO `poin_topsis` VALUES (3187, '75', 'SRS-SRT', '0.374');
INSERT INTO `poin_topsis` VALUES (3188, '75', 'SSR', '0.461');
INSERT INTO `poin_topsis` VALUES (3189, '75', 'SSS-SST', '0.539');
INSERT INTO `poin_topsis` VALUES (3190, '75', 'STR', '0.626');
INSERT INTO `poin_topsis` VALUES (3191, '75', 'TSR', '0.702');
INSERT INTO `poin_topsis` VALUES (3192, '75', 'STS-STT', '0.516');
INSERT INTO `poin_topsis` VALUES (3193, '75', 'TRS-TRT', '0.617');
INSERT INTO `poin_topsis` VALUES (3194, '75', 'TSS-TST', '0.688');
INSERT INTO `poin_topsis` VALUES (3195, '75', 'TTR', '0.745');
INSERT INTO `poin_topsis` VALUES (3196, '75', 'TTS-TTT', '0.873');
INSERT INTO `poin_topsis` VALUES (3197, '77', 'RRR', '0.127');
INSERT INTO `poin_topsis` VALUES (3198, '77', 'RRS-RRT', '0.255');
INSERT INTO `poin_topsis` VALUES (3199, '77', 'RSR', '0.312');
INSERT INTO `poin_topsis` VALUES (3200, '77', 'RSS-RST', '0.239');
INSERT INTO `poin_topsis` VALUES (3201, '77', 'RTR', '0.479');
INSERT INTO `poin_topsis` VALUES (3202, '77', 'RTS-RTT', '0.374');
INSERT INTO `poin_topsis` VALUES (3203, '77', 'SRR', '0.298');
INSERT INTO `poin_topsis` VALUES (3204, '77', 'SRS-SRT', '0.374');
INSERT INTO `poin_topsis` VALUES (3205, '77', 'SSR', '0.461');
INSERT INTO `poin_topsis` VALUES (3206, '77', 'SSS-SST', '0.539');
INSERT INTO `poin_topsis` VALUES (3207, '77', 'STR', '0.626');
INSERT INTO `poin_topsis` VALUES (3208, '77', 'TSR', '0.702');
INSERT INTO `poin_topsis` VALUES (3209, '77', 'STS-STT', '0.516');
INSERT INTO `poin_topsis` VALUES (3210, '77', 'TRS-TRT', '0.617');
INSERT INTO `poin_topsis` VALUES (3211, '77', 'TSS-TST', '0.688');
INSERT INTO `poin_topsis` VALUES (3212, '77', 'TTR', '0.745');
INSERT INTO `poin_topsis` VALUES (3213, '77', 'TTS-TTT', '0.873');
INSERT INTO `poin_topsis` VALUES (3214, '79', 'RRR', '0.127');
INSERT INTO `poin_topsis` VALUES (3215, '79', 'RRS-RRT', '0.255');
INSERT INTO `poin_topsis` VALUES (3216, '79', 'RSR', '0.312');
INSERT INTO `poin_topsis` VALUES (3217, '79', 'RSS-RST', '0.239');
INSERT INTO `poin_topsis` VALUES (3218, '79', 'RTR', '0.479');
INSERT INTO `poin_topsis` VALUES (3219, '79', 'RTS-RTT', '0.374');
INSERT INTO `poin_topsis` VALUES (3220, '79', 'SRR', '0.298');
INSERT INTO `poin_topsis` VALUES (3221, '79', 'SRS-SRT', '0.374');
INSERT INTO `poin_topsis` VALUES (3222, '79', 'SSR', '0.461');
INSERT INTO `poin_topsis` VALUES (3223, '79', 'SSS-SST', '0.539');
INSERT INTO `poin_topsis` VALUES (3224, '79', 'STR', '0.626');
INSERT INTO `poin_topsis` VALUES (3225, '79', 'TSR', '0.702');
INSERT INTO `poin_topsis` VALUES (3226, '79', 'STS-STT', '0.516');
INSERT INTO `poin_topsis` VALUES (3227, '79', 'TRS-TRT', '0.617');
INSERT INTO `poin_topsis` VALUES (3228, '79', 'TSS-TST', '0.688');
INSERT INTO `poin_topsis` VALUES (3229, '79', 'TTR', '0.745');
INSERT INTO `poin_topsis` VALUES (3230, '79', 'TTS-TTT', '0.873');
INSERT INTO `poin_topsis` VALUES (3231, '80', 'RRR', '0.127');
INSERT INTO `poin_topsis` VALUES (3232, '80', 'RRS-RRT', '0.255');
INSERT INTO `poin_topsis` VALUES (3233, '80', 'RSR', '0.312');
INSERT INTO `poin_topsis` VALUES (3234, '80', 'RSS-RST', '0.239');
INSERT INTO `poin_topsis` VALUES (3235, '80', 'RTR', '0.479');
INSERT INTO `poin_topsis` VALUES (3236, '80', 'RTS-RTT', '0.374');
INSERT INTO `poin_topsis` VALUES (3237, '80', 'SRR', '0.298');
INSERT INTO `poin_topsis` VALUES (3238, '80', 'SRS-SRT', '0.374');
INSERT INTO `poin_topsis` VALUES (3239, '80', 'SSR', '0.461');
INSERT INTO `poin_topsis` VALUES (3240, '80', 'SSS-SST', '0.539');
INSERT INTO `poin_topsis` VALUES (3241, '80', 'STR', '0.626');
INSERT INTO `poin_topsis` VALUES (3242, '80', 'TSR', '0.702');
INSERT INTO `poin_topsis` VALUES (3243, '80', 'STS-STT', '0.516');
INSERT INTO `poin_topsis` VALUES (3244, '80', 'TRS-TRT', '0.617');
INSERT INTO `poin_topsis` VALUES (3245, '80', 'TSS-TST', '0.688');
INSERT INTO `poin_topsis` VALUES (3246, '80', 'TTR', '0.745');
INSERT INTO `poin_topsis` VALUES (3247, '80', 'TTS-TTT', '0.873');
INSERT INTO `poin_topsis` VALUES (3248, '81', 'RRR', '0.127');
INSERT INTO `poin_topsis` VALUES (3249, '81', 'RRS-RRT', '0.255');
INSERT INTO `poin_topsis` VALUES (3250, '81', 'RSR', '0.312');
INSERT INTO `poin_topsis` VALUES (3251, '81', 'RSS-RST', '0.239');
INSERT INTO `poin_topsis` VALUES (3252, '81', 'RTR', '0.479');
INSERT INTO `poin_topsis` VALUES (3253, '81', 'RTS-RTT', '0.374');
INSERT INTO `poin_topsis` VALUES (3254, '81', 'SRR', '0.298');
INSERT INTO `poin_topsis` VALUES (3255, '81', 'SRS-SRT', '0.374');
INSERT INTO `poin_topsis` VALUES (3256, '81', 'SSR', '0.461');
INSERT INTO `poin_topsis` VALUES (3257, '81', 'SSS-SST', '0.539');
INSERT INTO `poin_topsis` VALUES (3258, '81', 'STR', '0.626');
INSERT INTO `poin_topsis` VALUES (3259, '81', 'TSR', '0.617');
INSERT INTO `poin_topsis` VALUES (3260, '81', 'STS-STT', '0.702');
INSERT INTO `poin_topsis` VALUES (3261, '81', 'TRS-TRT', '0.516');
INSERT INTO `poin_topsis` VALUES (3262, '81', 'TSS-TST', '0.688');
INSERT INTO `poin_topsis` VALUES (3263, '81', 'TTR', '0.745');
INSERT INTO `poin_topsis` VALUES (3264, '81', 'TTS-TTT', '0.873');
INSERT INTO `poin_topsis` VALUES (3265, '82', 'RRR', '0.127');
INSERT INTO `poin_topsis` VALUES (3266, '82', 'RRS-RRT', '0.255');
INSERT INTO `poin_topsis` VALUES (3267, '82', 'RSR', '0.312');
INSERT INTO `poin_topsis` VALUES (3268, '82', 'RSS-RST', '0.239');
INSERT INTO `poin_topsis` VALUES (3269, '82', 'RTR', '0.479');
INSERT INTO `poin_topsis` VALUES (3270, '82', 'RTS-RTT', '0.374');
INSERT INTO `poin_topsis` VALUES (3271, '82', 'SRR', '0.298');
INSERT INTO `poin_topsis` VALUES (3272, '82', 'SRS-SRT', '0.374');
INSERT INTO `poin_topsis` VALUES (3273, '82', 'SSR', '0.461');
INSERT INTO `poin_topsis` VALUES (3274, '82', 'SSS-SST', '0.539');
INSERT INTO `poin_topsis` VALUES (3275, '82', 'STR', '0.626');
INSERT INTO `poin_topsis` VALUES (3276, '82', 'TSR', '0.617');
INSERT INTO `poin_topsis` VALUES (3277, '82', 'STS-STT', '0.702');
INSERT INTO `poin_topsis` VALUES (3278, '82', 'TRS-TRT', '0.516');
INSERT INTO `poin_topsis` VALUES (3279, '82', 'TSS-TST', '0.688');
INSERT INTO `poin_topsis` VALUES (3280, '82', 'TTR', '0.745');
INSERT INTO `poin_topsis` VALUES (3281, '82', 'TTS-TTT', '0.873');
INSERT INTO `poin_topsis` VALUES (3282, '83', 'RRR', '0.147');
INSERT INTO `poin_topsis` VALUES (3283, '83', 'RRS-RRT', '0.293');
INSERT INTO `poin_topsis` VALUES (3284, '83', 'RSR', '0.246');
INSERT INTO `poin_topsis` VALUES (3285, '83', 'RSS-RST', '0.276');
INSERT INTO `poin_topsis` VALUES (3286, '83', 'RTR', '0.358');
INSERT INTO `poin_topsis` VALUES (3287, '83', 'RTS-RTT', '0.339');
INSERT INTO `poin_topsis` VALUES (3288, '83', 'SRR', '0.363');
INSERT INTO `poin_topsis` VALUES (3289, '83', 'SRS-SRT', '0.459');
INSERT INTO `poin_topsis` VALUES (3290, '83', 'SSR', '0.447');
INSERT INTO `poin_topsis` VALUES (3291, '83', 'SSS-SST', '0.553');
INSERT INTO `poin_topsis` VALUES (3292, '83', 'STR', '0.541');
INSERT INTO `poin_topsis` VALUES (3293, '83', 'TSR', '0.647');
INSERT INTO `poin_topsis` VALUES (3294, '83', 'STS-STT', '0.637');
INSERT INTO `poin_topsis` VALUES (3295, '83', 'TRS-TRT', '0.631');
INSERT INTO `poin_topsis` VALUES (3296, '83', 'TSS-TST', '0.754');
INSERT INTO `poin_topsis` VALUES (3297, '83', 'TTR', '0.707');
INSERT INTO `poin_topsis` VALUES (3298, '83', 'TTS-TTT', '0.853');
INSERT INTO `poin_topsis` VALUES (3299, '84', 'RRR', '0.147');
INSERT INTO `poin_topsis` VALUES (3300, '84', 'RRS-RRT', '0.293');
INSERT INTO `poin_topsis` VALUES (3301, '84', 'RSR', '0.246');
INSERT INTO `poin_topsis` VALUES (3302, '84', 'RSS-RST', '0.276');
INSERT INTO `poin_topsis` VALUES (3303, '84', 'RTR', '0.358');
INSERT INTO `poin_topsis` VALUES (3304, '84', 'RTS-RTT', '0.339');
INSERT INTO `poin_topsis` VALUES (3305, '84', 'SRR', '0.363');
INSERT INTO `poin_topsis` VALUES (3306, '84', 'SRS-SRT', '0.459');
INSERT INTO `poin_topsis` VALUES (3307, '84', 'SSR', '0.447');
INSERT INTO `poin_topsis` VALUES (3308, '84', 'SSS-SST', '0.553');
INSERT INTO `poin_topsis` VALUES (3309, '84', 'STR', '0.541');
INSERT INTO `poin_topsis` VALUES (3310, '84', 'TSR', '0.647');
INSERT INTO `poin_topsis` VALUES (3311, '84', 'STS-STT', '0.637');
INSERT INTO `poin_topsis` VALUES (3312, '84', 'TRS-TRT', '0.631');
INSERT INTO `poin_topsis` VALUES (3313, '84', 'TSS-TST', '0.754');
INSERT INTO `poin_topsis` VALUES (3314, '84', 'TTR', '0.707');
INSERT INTO `poin_topsis` VALUES (3315, '84', 'TTS-TTT', '0.853');
INSERT INTO `poin_topsis` VALUES (3316, '85', 'RRR', '0.147');
INSERT INTO `poin_topsis` VALUES (3317, '85', 'RRS-RRT', '0.293');
INSERT INTO `poin_topsis` VALUES (3318, '85', 'RSR', '0.246');
INSERT INTO `poin_topsis` VALUES (3319, '85', 'RSS-RST', '0.276');
INSERT INTO `poin_topsis` VALUES (3320, '85', 'RTR', '0.358');
INSERT INTO `poin_topsis` VALUES (3321, '85', 'RTS-RTT', '0.339');
INSERT INTO `poin_topsis` VALUES (3322, '85', 'SRR', '0.363');
INSERT INTO `poin_topsis` VALUES (3323, '85', 'SRS-SRT', '0.459');
INSERT INTO `poin_topsis` VALUES (3324, '85', 'SSR', '0.447');
INSERT INTO `poin_topsis` VALUES (3325, '85', 'SSS-SST', '0.553');
INSERT INTO `poin_topsis` VALUES (3326, '85', 'STR', '0.541');
INSERT INTO `poin_topsis` VALUES (3327, '85', 'STS-STT', '0.637');
INSERT INTO `poin_topsis` VALUES (3328, '85', 'TRS-TRT', '0.631');
INSERT INTO `poin_topsis` VALUES (3329, '85', 'TSR', '0.647');
INSERT INTO `poin_topsis` VALUES (3330, '85', 'TSS-TST', '0.754');
INSERT INTO `poin_topsis` VALUES (3331, '85', 'TTR', '0.707');
INSERT INTO `poin_topsis` VALUES (3332, '85', 'TTS-TTT', '0.853');
INSERT INTO `poin_topsis` VALUES (3333, '86', 'RRR', '0.147');
INSERT INTO `poin_topsis` VALUES (3334, '86', 'RRS-RRT', '0.293');
INSERT INTO `poin_topsis` VALUES (3335, '86', 'RSR', '0.246');
INSERT INTO `poin_topsis` VALUES (3336, '86', 'RSS-RST', '0.276');
INSERT INTO `poin_topsis` VALUES (3337, '86', 'RTR', '0.358');
INSERT INTO `poin_topsis` VALUES (3338, '86', 'RTS-RTT', '0.339');
INSERT INTO `poin_topsis` VALUES (3339, '86', 'SRR', '0.363');
INSERT INTO `poin_topsis` VALUES (3340, '86', 'SRS-SRT', '0.459');
INSERT INTO `poin_topsis` VALUES (3341, '86', 'SSR', '0.447');
INSERT INTO `poin_topsis` VALUES (3342, '86', 'SSS-SST', '0.553');
INSERT INTO `poin_topsis` VALUES (3343, '86', 'STR', '0.541');
INSERT INTO `poin_topsis` VALUES (3344, '86', 'STS-STT', '0.637');
INSERT INTO `poin_topsis` VALUES (3345, '86', 'TRS-TRT', '0.631');
INSERT INTO `poin_topsis` VALUES (3346, '86', 'TSR', '0.647');
INSERT INTO `poin_topsis` VALUES (3347, '86', 'TSS-TST', '0.754');
INSERT INTO `poin_topsis` VALUES (3348, '86', 'TTR', '0.707');
INSERT INTO `poin_topsis` VALUES (3349, '86', 'TTS-TTT', '0.853');
INSERT INTO `poin_topsis` VALUES (3350, '87', 'RRR', '0.669');
INSERT INTO `poin_topsis` VALUES (3351, '87', 'RRS-RRT', '0.699');
INSERT INTO `poin_topsis` VALUES (3352, '87', 'RSR', '0.710');
INSERT INTO `poin_topsis` VALUES (3353, '87', 'RSS-RST', '0.450');
INSERT INTO `poin_topsis` VALUES (3354, '87', 'RTR', '0.482');
INSERT INTO `poin_topsis` VALUES (3355, '87', 'RTS-RTT', '0.482');
INSERT INTO `poin_topsis` VALUES (3356, '87', 'SRR', '0.445');
INSERT INTO `poin_topsis` VALUES (3357, '87', 'SRS-SRT', '0.482');
INSERT INTO `poin_topsis` VALUES (3358, '87', 'SSR', '0.480');
INSERT INTO `poin_topsis` VALUES (3359, '87', 'SSS-SST', '0.518');
INSERT INTO `poin_topsis` VALUES (3360, '87', 'STR', '0.515');
INSERT INTO `poin_topsis` VALUES (3361, '87', 'STS-STT', '0.552');
INSERT INTO `poin_topsis` VALUES (3362, '87', 'TRS-TRT', '0.268');
INSERT INTO `poin_topsis` VALUES (3363, '87', 'TSR', '0.251');
INSERT INTO `poin_topsis` VALUES (3364, '87', 'TSS-TST', '0.290');
INSERT INTO `poin_topsis` VALUES (3365, '87', 'TTR', '0.301');
INSERT INTO `poin_topsis` VALUES (3366, '87', 'TTS-TTT', '0.331');

-- ----------------------------
-- Table structure for rekomtakaran
-- ----------------------------
DROP TABLE IF EXISTS `rekomtakaran`;
CREATE TABLE `rekomtakaran`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT ' ',
  `kd_formula` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `formula` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nitrogen` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `posfor` varbinary(20) NULL DEFAULT NULL,
  `kalium` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tekstur` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nitrogen2` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `posfor2` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kalium2` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tekstur2` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 18 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rekomtakaran
-- ----------------------------
INSERT INTO `rekomtakaran` VALUES (1, 'RRR', '( 290 Kg Urea + 100 Kg SP36 + 100 Kg KCl)', 'Tidak Sesuai', 0x546964616B20536573756169, 'Tidak Sesuai', 'Sesuai', '1', '1', '1', '5');
INSERT INTO `rekomtakaran` VALUES (2, 'RRS-RRT', '( 290 Kg Urea + 100 Kg SP36 + 50 Kg KCl)', 'Cukup Sesuai', 0x536573756169, 'Cukup Sesuai', 'Cukup Sesuai', '1', '1', '3', '5');
INSERT INTO `rekomtakaran` VALUES (3, 'RSR', '( 290 Kg Urea + 75 Kg SP36 + 100 Kg KCl)', 'Tidak Sesuai', 0x43756B757020536573756169, 'Tidak Sesuai', 'Sesuai', '1', '3', '1', '5');
INSERT INTO `rekomtakaran` VALUES (4, 'RSS-RST', '( 290 Kg Urea + 75 Kg SP36 + 50 Kg KCl)', 'Tidak Sesuai', 0x546964616B20536573756169, 'Cukup Sesuai', 'Cukup Sesuai', '1', '1', '3', '3');
INSERT INTO `rekomtakaran` VALUES (5, 'RTR', '( 290 Kg Urea + 50 Kg SP36 + 100 Kg KCl)', 'Tidak Sesuai', 0x536573756169, 'Tidak Sesuai', 'Cukup Sesuai', '1', '5', '1', '3');
INSERT INTO `rekomtakaran` VALUES (6, 'RTS-RTT', '( 290 Kg Urea + 50 Kg SP36 + 50 Kg KCl)', 'Tidak Sesuai', 0x43756B757020536573756169, 'Cukup Sesuai', 'Cukup Sesuai', '1', '3', '3', '3');
INSERT INTO `rekomtakaran` VALUES (7, 'SRR', '( 250 Kg Urea + 100 Kg SP36 + 100 Kg KCl)', 'Cukup Sesuai', 0x546964616B20536573756169, 'Tidak Sesuai', 'Cukup Sesuai', '3', '1', '1', '3');
INSERT INTO `rekomtakaran` VALUES (8, 'SRS-SRT', '( 250 Kg Urea + 100 Kg SP36 + 50 Kg KCl)', 'Cukup Sesuai', 0x546964616B20536573756169, 'Cukup Sesuai', 'Cukup Sesuai', '3', '1', '3', '3');
INSERT INTO `rekomtakaran` VALUES (9, 'SSR', '( 250 Kg Urea + 75 Kg SP36 + 100 Kg KCl)', 'Cukup Sesuai', 0x43756B757020536573756169, 'Tidak Sesuai', 'Cukup Sesuai', '3', '3', '1', '3');
INSERT INTO `rekomtakaran` VALUES (10, 'SSS-SST', '( 250 Kg Urea + 75 Kg SP36 + 50 Kg KCl)', 'Cukup Sesuai', 0x43756B757020536573756169, 'Cukup Sesuai', 'Cukup Sesuai', '3', '3', '3', '3');
INSERT INTO `rekomtakaran` VALUES (11, 'STR', '( 250 Kg Urea + 50 Kg SP36 + 100 Kg KCl)', 'Cukup Sesuai', 0x536573756169, 'Tidak Sesuai', 'Cukup Sesuai', '3', '5', '1', '3');
INSERT INTO `rekomtakaran` VALUES (12, 'STS-STT', '( 250 Kg Urea + 50 Kg SP36 + 50 Kg KCl)', 'Cukup Sesuai', 0x536573756169, 'Cukup Sesuai', 'Cukup Sesuai', '3', '5', '3', '3');
INSERT INTO `rekomtakaran` VALUES (13, 'TRS-TRT', '( 200 Kg Urea + 100 Kg SP36 + 50 Kg KCl)', 'Sesuai', 0x546964616B20536573756169, 'Cukup Sesuai', 'Tidak Sesuai', '5', '1', '3', '1');
INSERT INTO `rekomtakaran` VALUES (14, 'TSR', '( 200 Kg Urea + 75 Kg SP36 + 100 Kg KCl)', 'Sesuai', 0x43756B757020536573756169, 'Tidak Sesuai', 'Tidak Sesuai', '5', '3', '1', '1');
INSERT INTO `rekomtakaran` VALUES (15, 'TSS-TST', '( 200 Kg Urea + 75 Kg SP36 + 50 Kg KCl)', 'Sesuai', 0x43756B757020536573756169, 'Cukup Sesuai', 'Tidak Sesuai', '5', '3', '3', '1');
INSERT INTO `rekomtakaran` VALUES (16, 'TTR', '( 200 Kg Urea + 50 Kg SP36 + 100 Kg KCl)', 'Sesuai', 0x536573756169, 'Cukup Sesuai', 'Tidak Sesuai', '5', '5', '1', '1');
INSERT INTO `rekomtakaran` VALUES (17, 'TTS-TTT', '( 200 Kg Urea + 50 Kg SP36 + 50 Kg KCl)', 'Sesuai', 0x536573756169, 'Cukup Sesuai', 'Tidak Sesuai', '5', '5', '3', '1');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jk` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_telp` varchar(13) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pekerjaan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `username` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `level` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `aktif` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `level`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (6, NULL, 'admin', 'Laki-laki', '-', '-', '-', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', '0');
INSERT INTO `users` VALUES (7, NULL, '1', 'Perempuan', '1', '1', '1', '1', 'c4ca4238a0b923820dcc509a6f75849b', 'User', '1');

SET FOREIGN_KEY_CHECKS = 1;
